/*****************/
/* WEBSITE PART */
/***************/
function bindWebisteEvents()
{
    $("#filters-btn").on("click", function(event) {
        $("#sidebar-filters").addClass("active-sidebar");
    });
    $("#filters-btn-close").on("click", function(event) {
        $("#sidebar-filters").removeClass("active-sidebar");
    });
}

bindWebisteEvents();

/*****************/
/*   MAP PART   */
/***************/
var config = {
        tileSize: 256,
        imgWidth: 4864,
        imgHeight: 4864,
        imgMarginLeft: 5,
        imgMarginTop: 0,
        imgCoordinatesBoxWidth: 40,
        imgCoordinatesBoxHeight: 23,
        mapWidth: 152,
        mapHeight: 152,
        mapOverlayBottom: 0,
        maxZoom: 5,
        minZoom: 2,
        bounds: [[0, 0], [-152, 152]], /* [Top left : y / x] [Bottom right : y / x] */
        debug: false
    },
    map,
    mapXRatio,
    mapYRatio,
    currentMapPos,
    currentMapMousePos,
    mapObjects = {
        coordinatesBox: null,
        cellResources: {},
        markerCluster: L.markerClusterGroup({
            showCoverageOnHover: false,
            spiderfyOnMaxZoom: false,
            disableClusteringAtZoom: 5
        })
    };

/**
 * Init map.
 */
function initMap() {
    let center = getMapCenter();
    defineMapRatio();
    map = L.map('map', {
        crs: L.CRS.Simple,
        zoom: L.Browser.mobile ? config.minZoom : config.maxZoom,
        maxZoom: config.maxZoom,
        minZoom: config.minZoom,
        center: center,
        attributionControl: false
    });
    addMapTileLayer();
    if (config.debug) {
        addMapGridCoordsLayer();
    }
    initMapTileCoordinatesBox();
    bindMapEvents();
}

/**
 * Get center coordinates according to bounds.
 * @returns {number[]}
 */
function getMapCenter()
{
    let y = Math.floor(config.bounds[1][0] / 2),
        x = Math.floor(config.bounds[1][1] / 2);

    return [y, x];
}

/**
 *
 */
function defineMapRatio()
{
    mapYRatio = config.imgCoordinatesBoxHeight / ((config.imgHeight - config.imgMarginTop) / config.mapHeight);
    mapXRatio = config.imgCoordinatesBoxWidth / ((config.imgWidth - config.imgMarginLeft)  / config.mapWidth);
}

/**
 * Add tiles to map.
 */
function addMapTileLayer()
{
    L.tileLayer("//" + window.location.hostname + "/tiles/0/{z}/{x}/{y}.png", {
        bounds: config.bounds
    }).addTo(map);
}

/**
 * Add grid coordinates to debug map.
 */
function addMapGridCoordsLayer()
{
    let grid = L.gridLayer({
        attribution: 'Grid Layer',
        bounds: config.bounds
    });

    grid.createTile = function (coords) {
        let tile = L.DomUtil.create('div', 'tile-coords');
        tile.innerHTML = [coords.z, coords.x, coords.y].join(', ');
        tile.style.color = 'red';
        tile.style.fontSize = '32px';
        tile.style.position = 'absolute';
        tile.style.top = 0;
        tile.style.left = 0;
        return tile;
    };

    map.addLayer(grid);

    L.marker(getMapCenter()).addTo(map);
}

/**
 * Bind map events.
 */
function bindMapEvents()
{
    $('body').on('shown.bs.collapse', '.accordion .card .collapse', function() {
        map.invalidateSize({pan: false});
    });
    map.on('mousemove', mapEventMouseOver);
    map.on('zoomend', mapEventZoom);
}

/**
 *
 */
function initMapTileCoordinatesBox()
{
    mapObjects.coordinatesBox = L.marker([0,0], {
        interactive: false,
        icon: L.divIcon({
            className: 'coordinatesBox',
            iconAnchor: [0, 0],
            iconSize: getCoordinatesBoxDimensions()
        })
    }).addTo(map);
}

/**
 *
 * @param event
 */
function mapEventMouseOver(event)
{
    updateCoordinatesBoxContent(event, false);
}

function updateCoordinatesBoxContent(position, forceUpdate)
{
    // coords = y / x sur 4864px.
    let coords = map.project(position.latlng, config.maxZoom).subtract([config.imgMarginLeft, config.imgMarginTop]),
        y = Math.floor(coords.y / config.imgCoordinatesBoxHeight),
        x = Math.floor(coords.x / config.imgCoordinatesBoxWidth),
        yMap = y - 139,
        xMap = x - 76,
        xyMapPos = xMap + ',' + yMap,
        xyMapTxt = '<span class="coordinates-cell">[ ' + xyMapPos + ' ]</span>',
        divCoordinates = document.getElementById('coordinates');
    if (currentMapPos !== xyMapPos ||
        forceUpdate) {
        currentMapPos = xyMapPos;
        currentMapMousePos = position;
        if (mapObjects.cellResources[xyMapPos]) {
            $.each(mapObjects.cellResources[xyMapPos].resources, function (key, resource) {
                xyMapTxt += '<br />' + resource.txt;
            });
        }
        divCoordinates.innerHTML = xyMapTxt;
        divCoordinates.style.top  = position.containerPoint.y + 6 + 'px';
        divCoordinates.style.left = position.containerPoint.x + 6 + 'px';
        divCoordinates.style.display = 'block';
        mapObjects.coordinatesBox.setLatLng(map.unproject([
                (x * config.imgCoordinatesBoxWidth) + config.imgMarginLeft,
                (y * config.imgCoordinatesBoxHeight) + config.imgMarginTop
            ],
            config.maxZoom)
        );
    }
}

/**
 *
 * @param event
 */
function mapEventZoom(event)
{
    updateCoordinatesBoxDimensions();
}

/**
 *
 * @param y number
 * @param x number
 * @returns {number[]}
 */
function convertYXImgToMap(y, x)
{
    y = -y * mapYRatio;
    x =  x * mapXRatio;

    return [y, x];
}

/**
 *
 * @param y
 * @param x
 * @returns {number[]}
 */
function convertYXMapToImg(y, x)
{
    y = -y / mapYRatio;
    x =  x / mapXRatio;

    return [y, x];
}

/**
 *
 * @returns {number[]}
 */
function getCoordinatesBoxDimensions()
{
    let zoom = map.getZoom();

    return [
        config.imgCoordinatesBoxWidth / Math.pow(2, config.maxZoom - zoom),
        config.imgCoordinatesBoxHeight / Math.pow(2, config.maxZoom - zoom)
    ];
}

/**
 *
 * @returns {number[]}
 */
function getResourceBoxDimensions()
{
    let zoom = map.getZoom();

    return [
        18 / Math.pow(2, config.maxZoom - zoom),
        19 / Math.pow(2, config.maxZoom - zoom)
    ];
}

function getResourceBoxAnchor()
{
    let zoom = map.getZoom();

    return [
        -11 / Math.pow(2, config.maxZoom - zoom),
        -2 / Math.pow(2, config.maxZoom - zoom)
    ];
}

/**
 *
 */
function updateCoordinatesBoxDimensions()
{
    let boxDimensions = getCoordinatesBoxDimensions(),
        resourceBoxDimensions = getResourceBoxDimensions(),
        coordinatesBoxIcon = mapObjects.coordinatesBox.options.icon;
    coordinatesBoxIcon.options.iconSize = boxDimensions;
    mapObjects.coordinatesBox.setIcon(coordinatesBoxIcon);

    if (Object.keys(mapObjects.cellResources).length) {
        $.each(mapObjects.cellResources, function(index, cellResource) {
            coordinatesBoxIcon = cellResource.marker.options.icon;
            coordinatesBoxIcon.options.iconSize = resourceBoxDimensions;
            coordinatesBoxIcon.options.iconAnchor = getResourceBoxAnchor();
            cellResource.marker.setIcon(coordinatesBoxIcon);
        });
    }
}

// FILTERS

function bindCheckboxFilterEvents()
{
    $('.custom-control-input').change(function() {
        let jobId = $(this).data('jobId'),
            resourceId = $(this).data('resourceId');
        if ($(this).prop("checked")) {
            if (resourceId === undefined &&
                $(this).hasClass('custom-switch-all')) {
                    displayAllResourceOnMap(jobId);
            } else {
                displayResourceOnMap(jobId, resourceId);
            }
        } else {
            if (resourceId === undefined &&
                $(this).hasClass("custom-switch-all")) {
                hideAllResourceOnMap(jobId);
            } else {
                hideResourceOnMap(jobId, resourceId);
            }
        }
        if (currentMapMousePos) {
            updateCoordinatesBoxContent(currentMapMousePos, true);
        }
    });
}

/**
 * Check all resource type to display them on map.
 * @param jobId
 */
function displayAllResourceOnMap(jobId)
{
    $(".custom-switch-resource[data-job-id='" + jobId + "']")
        .prop("checked", true)
        .trigger("change");
}

/**
 * Uncheck all resource type to display them on map.
 * @param jobId
 */
function hideAllResourceOnMap(jobId)
{
    $(".custom-switch-resource[data-job-id='" + jobId + "']")
        .prop("checked", false)
        .trigger("change");
}

/**
 * Display resource on map.
 * @param jobId
 * @param resourceId
 */
function displayResourceOnMap(jobId, resourceId)
{
    $.each(jobs[jobId].resources[resourceId].maps, function (index, mapCoord) {
        let pos = mapCoord.pos.split(','),
            y = parseInt(pos[1]) + 139,
            x = parseInt(pos[0]) + 76,
            mapPos = map.unproject([(x * config.imgCoordinatesBoxWidth) + config.imgMarginLeft, (y * config.imgCoordinatesBoxHeight)], config.maxZoom);

        if (mapObjects.cellResources[mapCoord.pos] === undefined) {
            mapObjects.cellResources[mapCoord.pos] = {};
            mapObjects.cellResources[mapCoord.pos].marker = L.marker(mapPos, {
                interactive: true,
                icon: L.divIcon({
                    className: "cellResource",
                    iconAnchor: getResourceBoxAnchor(),
                    iconSize: getResourceBoxDimensions(),
                })
            }).addTo(map);
            mapObjects.markerCluster.addLayer(mapObjects.cellResources[mapCoord.pos].marker);
        }
        if (mapObjects.cellResources[mapCoord.pos].resources === undefined) {
            mapObjects.cellResources[mapCoord.pos].resources = {};
        }
        if (mapObjects.cellResources[mapCoord.pos].resources[resourceId] === undefined) {
            mapObjects.cellResources[mapCoord.pos].resources[resourceId] = {
                txt: '<span class="coordinates-resource coordinates-resource-' + jobId + '">' + mapCoord.qty + ' ' + jobs[jobId].resources[resourceId].name[currentLang] +'</span>'
            };
        }
    });
    map.addLayer(mapObjects.markerCluster);
}

/**
 * Hide resource on map.
 * @param jobId
 * @param resourceId
 */
function hideResourceOnMap(jobId, resourceId)
{
    $.each(jobs[jobId].resources[resourceId].maps, function (index, mapCoord) {
        if (mapObjects.cellResources[mapCoord.pos] !== undefined &&
            mapObjects.cellResources[mapCoord.pos].resources[resourceId] !== undefined) {
            delete mapObjects.cellResources[mapCoord.pos].resources[resourceId];
        }
        if (!Object.keys(mapObjects.cellResources[mapCoord.pos].resources).length) {
            map.removeLayer(mapObjects.cellResources[mapCoord.pos].marker);
            delete mapObjects.cellResources[mapCoord.pos];
        }
    });
}

initMap();
bindCheckboxFilterEvents();