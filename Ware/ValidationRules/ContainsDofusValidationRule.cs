﻿using System.Globalization;
using System.Windows.Controls;

namespace Ware.ValidationRules
{
    public class ContainsDofusValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (((string)value).Contains("Dofus.exe") && ((string)value).Contains(@"\Local\Ankama\zaap\retro\resources"))
            {
                return ValidationResult.ValidResult;
            }
            else
            {
                //return new ValidationResult(false, null);
                return new ValidationResult(false, "Chemin incorrect");
            }
        }
    }
}