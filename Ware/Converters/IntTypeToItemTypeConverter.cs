﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Ware.Converters
{
	public class IntTypeToItemTypeConverter : IValueConverter
	{

			public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
			{
				switch (value.ToString())
				{
					case "24":
					return "Equipement";
					case "1":
					return "Equipement";
	
				}
				return "Divers";
			}

			public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
			{
				
				return "Divers";
			}
		}
	
}
