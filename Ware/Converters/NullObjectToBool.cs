﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Ware.Converters
{
    public class NullObjectToBool : IValueConverter
    {
        public object Convert(object value, Type targetType,object parameter, System.Globalization.CultureInfo culture)
        {
            if (value.ToString() == "" || value.ToString() == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public object ConvertBack(object value, Type targetType,object parameter, System.Globalization.CultureInfo culture)
        {
            // I don't think you'll need this
            throw new Exception("Can't convert back");
        }
    }

}
