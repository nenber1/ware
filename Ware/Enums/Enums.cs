﻿using System.ComponentModel;

namespace Ware.Enums
{
    public enum LogType
    {
        General,
        Prive,
        Guilde,
        Alignement,
        Recrutement,
        Commerce,
        Alert,
        Info,
        Ware,
        Equipe,
        Groupe
    }

    internal enum AnimationType
    {
        Mount,
        Running,
        Walking,
        Ghost
    }

    public enum MovementResult
    {
        Success,
        SameCell,
        FALLO,
        PathfindingError,
        CellRangeError,
        CharacterBusyOrFull,
        CellNotWalkable,
        CellIsTypeOfInteractiveObject,
        MonsterOnSun
    }

    public enum InventoryObjectType
    {
        Equipement,
        Resource,
        Miscellaneous,
        QuestItem,
        Unknown
    }

    public enum EquipementType
    {
        Unequipped = -1,
        Necklace = 1,
        
        LeftRing = 2,
        Wand = 3,
        Stick = 4,
        //Belt = 3,
        //RightRing = 4,
        Dagger = 5,
        Boots = 5,
        Sword = 6,
       // Cap = 6,
       
        Hammer = 7,
       // Mantle = 7,
        //Pet = 8,
        Shovel = 8,
        Dofus1 = 9,
        Dofus2 = 10,
        Dofus3 = 11,
        Dofus4 = 12,
        Dofus5 = 13,
        Dofus6 = 14,
        Shield = 15,
        Axe = 19,
        Scythe = 22,
       SoulStone = 83
    }

    public enum SocketState
    {
        Closed,
        Closing,
        Connected,
        Connecting,
        Listening,
    }

    public enum MapDirection
    {
        NONE,
        TOP,
        BOTTOM,
        LEFT,
        RIGHT,
    }

    public enum DebugType
    {
        Server,
        Client,
        NULL
    }

    public enum BotType
    {
        Harvest,
        Fight,
        Group,
        Information
    }

    public enum WareType
    {
        MITM, Socket
    }

    public enum Server
    {
        Algathe = 605,
        Arty = 604,
        Ayuto = 608,
        Bilby = 609,
        Clustus = 610,
        Droupik = 607,
        Eratz = 601,
        Henual = 602,
        Hogmeiser = 606,
        Issering = 611,
        Nabur = 603,
    }

    public enum InstanceState
    {
        [Description("Déconnecté")]
        Disconnected = 0,

        [Description("Connexion au serveur d'authentification...")]
        AuthenticationConnection = 1,

        [Description("Connecté au serveur")]
        ConnectionIdle = 2,

        [Description("Connexion au serveur de jeu...")]
        ServerConnection = 3,

        [Description("Sélection du personnage")]
        CharacterSelection = 4,

        [Description("En attente")]
        Idle = 5,

        [Description("En combat")]
        Fight = 6,

        [Description("En mouvement")]
        Moving = 7,
    }
}