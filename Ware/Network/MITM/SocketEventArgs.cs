﻿using System;
using System.Net.Sockets;

namespace Ware.Network.MITM
{
    public class SocketConnectionEventArgs : EventArgs
    {
        private bool success;
        private Socket pSocket;

        public SocketConnectionEventArgs(Socket socket, bool succeed)
        {
            Success = succeed;
            PSocket = socket;
        }

        public bool Success { get => success; set => success = value; }
        public Socket PSocket { get => pSocket; set => pSocket = value; }
    }

    public class SocketDisconnectionEventArgs : EventArgs
    {
        private bool success;
        private Socket pSocket;

        public SocketDisconnectionEventArgs(Socket socket)
        {
            PSocket = socket;
        }

        public Socket PSocket { get => pSocket; set => pSocket = value; }
    }

    public class SocketReceiveDataEventArgs : EventArgs
    {
        private string msg;
        private Socket pSocket;
        private byte[] data;

        public SocketReceiveDataEventArgs(Socket socket, string msg, byte[] data)
        {
            Msg = msg;
            PSocket = socket;
            Data = data;
        }

        public string Msg { get => msg; set => msg = value; }
        public Socket PSocket { get => pSocket; set => pSocket = value; }
        public byte[] Data { get => data; set => data = value; }
    }

    public class SocketSendDataEventArgs : EventArgs
    {
        private string msg;
        private Socket pSocket;
        private byte[] data;

        public SocketSendDataEventArgs(Socket socket, string msg, byte[] data)
        {
            Msg = msg;
            PSocket = socket;
            Data = data;
        }

        public string Msg { get => msg; set => msg = value; }
        public Socket PSocket { get => pSocket; set => pSocket = value; }
        public byte[] Data { get => data; set => data = value; }
    }

    public class SocketErrorEventArgs : EventArgs
    {
        private string msg;
        private Socket pSocket;

        public SocketErrorEventArgs(Socket socket, string msg)
        {
            Msg = msg;
            PSocket = socket;
        }

        public string Msg { get => msg; set => msg = value; }
        public Socket PSocket { get => pSocket; set => pSocket = value; }
    }
}