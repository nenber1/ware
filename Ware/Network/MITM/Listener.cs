﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ware.Common.Utils;
using Ware.Core.MDI;

namespace Ware.Network.MITM
{
    public static class Listener
    {
        private static Socket socket;
        private static IPEndPoint _localEP;
        private static bool started = false;

        public delegate void onConnectionCatchedEventHandler(Socket catchedConnection, int pid);

        public static event onConnectionCatchedEventHandler OnCatchedConnection;

        private static List<TcpProcessRecord> tcpProcessRecords;
        private static List<int> registeredSocketManagers;
        private static Socket listener;
        private static Socket handler;

        public static void Subscribe(int id)
        {
            if (started)
            {
                registeredSocketManagers.Add(id);
                try
                {
                    listener.Listen(100);
                    listener.BeginAccept(
                        new AsyncCallback(AcceptCallback),
                        listener);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString());
                }
            }
        }

        public static void StartListening()
        {
            started = true;
            if (listener != null)
            {
                listener.Close();
                listener = null;
            }
            registeredSocketManagers = new List<int>();

            // Establish the local endpoint for the socket.
            // The DNS name of the computer
            // running the listener is "host.contoso.com".
            IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Loopback, 443);

            // Create a TCP/IP socket.
            listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            // Bind the socket to the local endpoint and listen for incoming connections.
            //listener.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Linger, new LingerOption(false, 1));
            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(100);
                listener.BeginAccept(
                    new AsyncCallback(AcceptCallback),
                    listener);
            }
            catch (Exception e)
            {
                MessageBox.Show("Probleme de communication avec dofus", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private async static void AcceptCallback(IAsyncResult ar)
        {
            // Signal the main thread to continue.

            // Get the socket that handles the client request.
            if (started == true)
            {
                Socket listener = (Socket)ar.AsyncState;
                if (listener != null && registeredSocketManagers.Count > 0)
                {
                    Socket handler = listener.EndAccept(ar);

                    int pid = await Task<int>.Factory.StartNew(() =>
                    {
                        tcpProcessRecords = new List<TcpProcessRecord>();
                        tcpProcessRecords = NetState.GetAllTcpConnections();

                        foreach (TcpProcessRecord tcp in tcpProcessRecords)
                        {
                            //dofus.dll
                            if (tcp.RemoteAddress.ToString() == "127.0.0.1" && tcp.RemotePort.ToString() == "443" && tcp.ProcessName == "Dofus" && registeredSocketManagers.Contains(tcp.ProcessId))
                            {
                                //MessageBox.Show(tcp.ProcessId.ToString());
                                return tcp.ProcessId;
                            }
                        }
                        return 0;
                    });
                    if (pid == 0)
                    {
                        //throw new Exception();
                        Functions.ShowToastMessage("Impossible de reconnaitre le processus associé");
                    }

                    if (OnCatchedConnection != null && registeredSocketManagers.Contains(pid) && handler != null)
                    {
                        registeredSocketManagers.Remove(pid);
                        OnCatchedConnection(handler, pid);
                    }

                    if (registeredSocketManagers.Count > 0)
                    {
                        listener.BeginAccept(
                    new AsyncCallback(AcceptCallback),
                    listener);
                    }
                }
            }
        }

        public static void StopListening()
        {
            started = false;
            try
            {
                //listener.Close();
            }
            catch (Exception)
            {
            }
            try
            {
                //listener.Dispose();
            }
            catch (Exception)
            {
            }
            try
            {
                //registeredSocketManagers.Clear();
            }
            catch (Exception)
            {
            }
        }
    }
}