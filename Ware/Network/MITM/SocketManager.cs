﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Ware.Core.MDI;
using Ware.Engine.Providers;
using Ware.Network.Protocol;
using Ware.Network.TcpEngine;
using static Ware.Network.TcpEngine.SimpleClient;

namespace Ware.Network.MITM
{
    public class SocketManager
    {
        public string Ip { get; set; }
        public string Port { get; set; }
        private ProtocolManager ProtocolManager;
        private int Pid { get; set; }
        private SimpleClient Client;
        private SimpleClient Server;
        private List<TcpProcessRecord> tcpProcessRecords;


        public delegate void onDisconnectedEventHandler(object sender, SocketDisconnectionEventArgs Socket);

        public event onDisconnectedEventHandler OnDisconnection;

        /// <summary>
        ///
        /// </summary>
        /// <param name="ip">L'ip du serveur d'authentification Dofus</param>
        /// <param name="port">Le port du serveur d'authentification Dofus</param>
        //public SocketManager(string ip,string port,Character character)
        public SocketManager(string ip, string port)
        {
            Ip = ip;
            Port = port;
        }

        /// <summary>
        /// Fonction lancer lors de la premiere connexion
        /// </summary>
        /// <param name="protocolManager"></param>
        /// <param name="pid"></param>
        public async void Start(ProtocolManager protocolManager, int pid)
        {
            ProtocolManager = protocolManager;
            Pid = pid;
            Listener.Subscribe(Pid);
            Listener.OnCatchedConnection += (s, p) => 
            {
                if(p == Pid)
                {
                    Server = new SimpleClient(s);
                    Server.DataReceived += Server_DataReceived;
                    Server.Disconnected += Server_Disconnected;

                    Client = new SimpleClient(Ip, short.Parse(Port));
                    Client.Connected += Client_Connected;
                    Client.DataReceived += Client_DataReceived;
                    Client.Disconnected += Client_Disconnected;
                    Listener.OnCatchedConnection -= (aa,bb) => { };
                }
            };

        }

        public void Disconnect()
        {
            try
            {
                //Client.Dispose();
                Server.Dispose();
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        private void Client_Disconnected(object sender, DisconnectedEventArgs e)
        {
            LogProvider.Log(new Models.Character.LogMessage(Enums.LogType.Ware, "Server Dofus Déconnecté"));
        }

        private void Client_DataReceived(object sender, DataReceivedEventArgs e)
        {
            //LogProvider.Debug(new Models.Character.DebugMessage(Enums.DebugType.Server, Encoding.UTF8.GetString(e.Data)));
            //Server.Send(e.Data);
            ProtocolManager.HandleMessage(e.Data);
        }

        private void Client_Connected(object sender, ConnectedEventArgs e)
        {
            LogProvider.Log(new Models.Character.LogMessage(Enums.LogType.Ware, "Connecté au serveur " + e.EndPoint));
        }

        private void Server_Disconnected(object sender, SimpleClient.DisconnectedEventArgs e)
        {
            LogProvider.Log(new Models.Character.LogMessage(Enums.LogType.Ware, "Client Dofus Déconnecté"));
        }

        private void Server_DataReceived(object sender, SimpleClient.DataReceivedEventArgs e)
        {
            //LogProvider.Debug(new Models.Character.DebugMessage(Enums.DebugType.Client, Encoding.UTF8.GetString(e.Data)));
            Client.Send(e.Data);
            ProtocolManager.HandleClientMessage(Encoding.UTF8.GetString(e.Data));
        }

        public void ClientGameSend(string msg)
        {
            if(Server != null)
            {
                LogProvider.Debug(new Models.Character.DebugMessage(Enums.DebugType.Server, msg));
                msg += "\n\x00";
                byte[] data = Encoding.UTF8.GetBytes(msg);
                Server.Send(data);
            }
            
        }
        public void ServerGameSend(string msg)
        {
            if(Client != null)
            {
                LogProvider.Debug(new Models.Character.DebugMessage(Enums.DebugType.Client, msg));
                msg += "\n\x00";
                byte[] data = Encoding.UTF8.GetBytes(msg);
                Client.Send(data);
            }
        }

        protected virtual void RaiseDisconnection(SocketDisconnectionEventArgs e)
        {
            // Event will be null if there are no subscribers
            if (OnDisconnection != null)
            {
                OnDisconnection?.Invoke(this, e);
            }
        }

        #region old

        //public async Task Run()
        //{
        //    IPEndPoint endpoint = new IPEndPoint(IPAddress.Parse(Ip), int.Parse(Port));
        //    Client = new BaseClient(endpoint);
        //    while (true)
        //    {
        //            var result = await Client.ConnectAsync();
        //            if (result.SocketError == SocketError.Success)
        //                break;
        //    }
        //    await ClientReceive();
        //}

        ////DOFUS SERV TO WARE
        //private async Task ClientReceive()
        //{
        //    while (true)
        //    {
        //        var result = await Client.ReceiveAsync(Client.ConnectSocket);

        //        if (!result.ReceiveSuccess)
        //        {
        //            break;
        //        }
        //        else if(result.ReceiveSuccess)
        //        {
        //            Debug.WriteLine("Dofus serveur : " + Encoding.ASCII.GetString(result.Data) + "\n");
        //            ProtocolManager.HandleMessage(result.Data);
        //        }
        //    }
        //    var res = await Server.DisconnectAsync(Server.ConnectSocket);

        //}

        ////DOFUS CLIENT TO WARE
        //private async Task ServerReceive()
        //{
        //    while (true)
        //    {
        //        var result = await Server.ReceiveAsync(Server.ConnectSocket);

        //        if (!result.ReceiveSuccess)
        //        {
        //            break;
        //        }
        //        else if (result.ReceiveSuccess)
        //        {
        //            Debug.WriteLine("Dofus client : " + Encoding.ASCII.GetString(result.Data) + "\n");
        //            ProtocolManager.HandleClientMessage(result.Data);
        //        }
        //    }
        //    if (Process.GetProcessById(Pid) != null)
        //    {
        //        Listener.Subscribe(Pid);
        //        Listener.OnCatchedConnection += Listener_OnCatchedConnection;
        //    }
        //    var r = await Client.DisconnectAsync(Client.ConnectSocket);
        //    if (OnDisconnection != null)
        //    {
        //        OnDisconnection(this, r);
        //    }
        //}

        ////WARE TO DOFUS SERVEUR
        //private async Task ClientSend(byte[] data)
        //{
        //    var result = await Client.SendAsync(Client.ConnectSocket,data);
        //    Debug.WriteLine("Ware to dofus server   : " + Encoding.UTF8.GetString(result.Data) + "\n ");
        //}

        ////WARE TO DOFUS CLIENT
        //private async Task ServerSend(byte[] data)
        //{
        //    var result = await Server.SendAsync(Server.ConnectSocket, data);
        //    Debug.WriteLine("Ware to dofus client  : " + Encoding.UTF8.GetString(result.Data) + "\n ");
        //}

        ///// <summary>
        ///// Send packet to dofus client
        ///// </summary>
        ///// <param name="msg"></param>
        //public async void SendBelow(string msg)
        //{
        //    msg += "\x00";
        //    byte[] data = Encoding.UTF8.GetBytes(msg);
        //    ServerSend(data);
        //}

        ///// <summary>
        ///// Send packet to dofus client
        ///// </summary>
        ///// <param name="msg"></param>
        //public async void SendBelow(byte[] msg)
        //{
        //    ServerSend(msg);
        //}

        ///// <summary>
        ///// Send packet to dofus server
        ///// </summary>
        ///// <param name="msg"></param>
        //public async void SendAbove(string msg)
        //{
        //    msg += "\x00";
        //    byte[] data = Encoding.UTF8.GetBytes(msg);
        //    ProtocolManager.HandleClientMessage(data);
        //    //ClientSend(data);
        //}

        ///// <summary>
        ///// Send packet to dofus server
        ///// </summary>
        ///// <param name="msg"></param>
        //public async void SendAbove(byte[] msg)
        //{
        //    ClientSend(msg);
        //}

        //public void Disconnect()
        //{
        //    if (Server != null)
        //    {
        //        if (Server.ConnectSocket != null)
        //        {
        //            if (Server.ConnectSocket.Connected == true)
        //            {
        //                try
        //                {
        //                    Server.DisconnectAsync(Server.ConnectSocket);
        //                }
        //                catch (Exception e)
        //                {
        //                    MessageBox.Show(e.ToString());
        //                }
        //            }
        //            Server.CloseClientSocket(Server.ConnectSocket);
        //            Server.Dispose();
        //        }
        //    }
        //    //MessageBox.Show(Client.ConnectSocket.Connected.ToString());
        //    if (Client != null)
        //    {
        //        if (Client.ConnectSocket != null)
        //        {
        //            if (Client.ConnectSocket.Connected == true)
        //            {
        //                try
        //                {
        //                    Client.DisconnectAsync(Client.ConnectSocket);
        //                }
        //                catch (Exception e)
        //                {
        //                    MessageBox.Show(e.ToString());
        //                }
        //            }
        //            Client.CloseClientSocket(Client.ConnectSocket);
        //            Client.Dispose();
        //        }
        //    }
        //}

        #endregion old
    }
    //public class SyncStoppedEventArgs : EventArgs
    //{
    //    public S Client;

    //    public SyncStoppedEventArgs(SyncClient client)
    //    {
    //        Client = client;
    //    }
    //}

    //public class MessageReceivedEventArgs : EventArgs
    //{
    //    public NetworkMessage Message;

    //    public MessagePart Data;

    //    public MessageReceivedEventArgs(NetworkMessage msg, MessagePart data)
    //    {
    //        Message = msg;
    //        Data = data;
    //    }
    //}

    //public class MessageSentEventArgs : EventArgs
    //{
    //    public NetworkMessage Message;

    //    public MessagePart Data;

    //    public MessageSentEventArgs(NetworkMessage msg, MessagePart data)
    //    {
    //        Message = msg;
    //        Data = data;
    //    }
    //}
}