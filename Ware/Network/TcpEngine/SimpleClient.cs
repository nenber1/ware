﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Ware.Network.TcpEngine
{
	public class SimpleClient
	{
		public class ConnectedEventArgs : EventArgs
		{
            public ConnectedEventArgs(EndPoint endPoint)
            {
				EndPoint = endPoint;
            }
			public EndPoint EndPoint
			{
				get;
				private set;
			}
		}

		public class DisconnectedEventArgs : EventArgs
		{
			public Socket Socket
			{
				get;
				private set;
			}

			public DisconnectedEventArgs(Socket socket)
			{
				Socket = socket;
			}
		}

		public class DataSendedEventArgs : EventArgs
		{
		}

		public class DataReceivedEventArgs : EventArgs
		{
			public byte[] Data
			{
				get;
				private set;
			}

			public DataReceivedEventArgs(byte[] data)
			{
				Data = data;
			}
		}

		public class ErrorEventArgs : EventArgs
		{
			public Exception Ex
			{
				get;
				private set;
			}

			public ErrorEventArgs(Exception ex)
			{
				Ex = ex;
			}
		}

		public Socket Socket;

		private byte[] sendBuffer;

		private byte[] receiveBuffer;

		private const int bufferLength = 8192;


		public bool Runing
		{
			get;
			private set;
		}

		public event EventHandler<ConnectedEventArgs> Connected;

		public event EventHandler<DisconnectedEventArgs> Disconnected;

		public event EventHandler<DataReceivedEventArgs> DataReceived;

		public event EventHandler<DataSendedEventArgs> DataSended;

		public event EventHandler<ErrorEventArgs> Error;

		public SimpleClient(string ip, short port)
		{
			Init();
			Start(ip, port);
		}

		public SimpleClient(Socket socket)
		{
			Init();
			Start(socket);
		}

		public SimpleClient()
		{
			Init();
		}

		public void Start(Socket socket)
		{
			try
			{
				Runing = true;
				Socket = socket;
				Socket.BeginReceive(receiveBuffer, 0, 8192, SocketFlags.None, ReceiveCallBack, Socket);
			}
			catch (Exception ex)
			{
				OnError(new ErrorEventArgs(ex));
			}
		}

		public void Start(string ip, short port)
		{
			try
			{
				Socket.BeginConnect(new IPEndPoint(IPAddress.Parse(ip), port), ConnectionCallBack, Socket);
			}
			catch (Exception ex)
			{
				OnError(new ErrorEventArgs(ex));
			}
		}

		public void Start(IPEndPoint endPoint)
		{
			try
			{
				Socket.BeginConnect(endPoint, ConnectionCallBack, Socket);
			}
			catch (Exception ex)
			{
				OnError(new ErrorEventArgs(ex));
			}
		}

		public void Stop()
		{
			try
			{
				if (Socket.Connected)
				{
					Socket.BeginDisconnect(reuseSocket: false, DisconectedCallBack, Socket);
				}
			}
			catch (Exception ex)
			{
				OnError(new ErrorEventArgs(ex));
			}
		}

		public void Send(byte[] data)
		{
			try
			{
				if (!Socket.Connected)
				{
					Runing = false;
				}
				if (Runing)
				{
					if (data.Length != 0)
					{
						sendBuffer = data;
						Socket.BeginSend(sendBuffer, 0, sendBuffer.Length, SocketFlags.None, SendCallBack, Socket);
					}
				}
				else
				{
					Console.WriteLine("Send " + data.Length.ToString() + " bytes but not runing");
				}
			}
			catch (Exception ex)
			{
				OnError(new ErrorEventArgs(ex));
			}
		}

		public void Dispose()
		{
			if (Socket != null)
			{
				Socket.Dispose();
			}
			//if (buffer != null)
			//{
			//	buffer.Dispose();
			//}
			Socket = null;
			sendBuffer = null;
			receiveBuffer = null;
			//buffer = null;
			//currentMessage = null;
		}

		private void Init()
		{
			try
			{
				//buffer = new BigEndianReader();
				receiveBuffer = new byte[8192];
				Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			}
			catch (Exception ex)
			{
				OnError(new ErrorEventArgs(ex));
			}
		}

		//private void ThreatBuffer()
		//{
		//	if (currentMessage == null)
		//	{
		//		currentMessage = new MessagePart();
		//	}
		//	long pos = buffer.Position;
		//	string ip = ((IPEndPoint)Socket.RemoteEndPoint).Address.ToString();
		//	bool isClient = ip.StartsWith("127.0.0.1");
		//	if (currentMessage.Build(buffer, isClient))
		//	{
		//		OnDataReceived(new DataReceivedEventArgs(currentMessage));
		//		currentMessage = null;
		//		ThreatBuffer();
		//	}
		//}

		private void ConnectionCallBack(IAsyncResult asyncResult)
		{
			try
			{
				Runing = true;
				Socket client = (Socket)asyncResult.AsyncState;
				client.EndConnect(asyncResult);
				client.BeginReceive(receiveBuffer, 0, 8192, SocketFlags.None, ReceiveCallBack, client);
				OnConnected(new ConnectedEventArgs(client.RemoteEndPoint));
			}
			catch (Exception ex)
			{
				OnError(new ErrorEventArgs(ex));
			}
		}

		private void DisconectedCallBack(IAsyncResult asyncResult)
		{
			try
			{
				Runing = false;
				Socket client = (Socket)asyncResult.AsyncState;
				client.EndDisconnect(asyncResult);
				OnDisconnected(new DisconnectedEventArgs(Socket));
			}
			catch (Exception ex)
			{
				OnError(new ErrorEventArgs(ex));
			}
		}

		private void ReceiveCallBack(IAsyncResult asyncResult)
		{
			Socket client = (Socket)asyncResult.AsyncState;
			if (!client.Connected)
			{
				Runing = false;
			}
			else if (Runing)
			{
				int bytesRead = 0;
				try
				{
					bytesRead = client.EndReceive(asyncResult);
				}
				catch (Exception ex2)
				{
					OnError(new ErrorEventArgs(ex2));
				}
				if (bytesRead == 0)
				{
					Runing = false;
					OnDisconnected(new DisconnectedEventArgs(Socket));
					return;
				}
				byte[] data = new byte[bytesRead];
				Array.Copy(receiveBuffer, data, bytesRead);
				//buffer.Add(data, 0, data.Length);
				//ThreatBuffer();
				OnDataReceived(new DataReceivedEventArgs(data));
				try
				{
					client.BeginReceive(receiveBuffer, 0, 8192, SocketFlags.None, ReceiveCallBack, client);
				}
				catch (Exception ex)
				{
					OnError(new ErrorEventArgs(ex));
				}
			}
			else
			{
				Console.WriteLine("Receive data but not running");
			}
		}

		private void SendCallBack(IAsyncResult asyncResult)
		{
			try
			{
				if (Runing)
				{
					Socket client = (Socket)asyncResult.AsyncState;
					client.EndSend(asyncResult);
					OnDataSended(new DataSendedEventArgs());
				}
				else
				{
					Console.WriteLine("Send data but not runing !");
				}
			}
			catch (Exception ex)
			{
				OnError(new ErrorEventArgs(ex));
			}
		}

		private void OnConnected(ConnectedEventArgs e)
		{
			if (this.Connected != null)
			{
				this.Connected(this, e);
			}
		}

		private void OnDisconnected(DisconnectedEventArgs e)
		{
			if (this.Disconnected != null)
			{
				this.Disconnected(this, e);
			}
		}

		private void OnDataReceived(DataReceivedEventArgs e)
		{
			if (this.DataReceived != null)
			{
				this.DataReceived(this, e);
			}
		}

		private void OnDataSended(DataSendedEventArgs e)
		{
			if (this.DataSended != null)
			{
				this.DataSended(this, e);
			}
		}

		private void OnError(ErrorEventArgs e)
		{
			if (this.Error != null)
			{
				this.Error(this, e);
			}
		}
	}
}
