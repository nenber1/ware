﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Ware.Common.Attributes;
using Ware.Common.Frame;
using Ware.Engine.Providers;
using Ware.Models.Character;

namespace Ware.Network.Protocol
{
    public class ProtocolManager
    {
        private readonly List<NetMessage> Retriever = new List<NetMessage>();
        private bool IsInit = false;
        private Instance Instance;

        public ProtocolManager(Instance inst)
        {
            Instance = inst;
            if (IsInit == false)
            {
                IsInit = true;
                Assembly asm = typeof(Frame).GetTypeInfo().Assembly;

                foreach (MethodInfo methodInfo in asm.GetTypes().SelectMany(x => x.GetMethods()).Where(m => m.GetCustomAttributes(typeof(MessageHandlerAttribute), false).Length > 0))
                {
                    MessageHandlerAttribute attribut = methodInfo.GetCustomAttributes(typeof(MessageHandlerAttribute), true)[0] as MessageHandlerAttribute;
                    Type methode = Type.GetType(methodInfo.DeclaringType.FullName);

                    object instance = Activator.CreateInstance(methode, new object[1] { Instance });
                    Retriever.Add(new NetMessage(attribut.Message, methodInfo, instance));
                }
            }
        }

        public void HandleMessage(byte[] msg)
        {

            string data = "";
            foreach (string packet in Encoding.UTF8.GetString(msg).Replace("\x0a", string.Empty).Split('\0').Where(x => x != string.Empty))
            {
                NetMessage message = Retriever.Find(m => packet.StartsWith(m.Id));

                if (message != null)
                {
                    data += message.MethodInfo.Invoke(message.Instance, new object[1] { packet });
                    data += '\0';
                }
                else
                {
                    data += packet;
                    data += '\0';
                }
                //LogProvider.Debug(new DebugMessage(Ware.Enums.DebugType.Server, data));
            }
            Instance.SocketManager.ClientGameSend(data);
            //LogProvider.Debug(new DebugMessage(Ware.Enums.DebugType.Server, "(FULL PACKET) => " + data));
            
        }

        public void HandleClientMessage(string msg)
        {
            foreach (string packet in msg.Replace("\x0a", string.Empty).Split('\0').Where(x => x != string.Empty))
            {

                switch(packet)
                {
                    case "GKK0":
                        Instance.InstanceState = Enums.InstanceState.Idle;
                        break;
                }
                LogProvider.Debug(new DebugMessage(Ware.Enums.DebugType.Client, packet));
                //LogProvider.Debug(new DebugMessage(Ware.Enums.DebugType.Server, data));
            }
            //if(packet.Contains("GA001"))
            //{
            //    Instance.InstanceState = Enums.InstanceState.Moving;
            //}
            //string data = "";
            //int i = 0;
            //i = msg.Replace("\x0a", string.Empty).Split('\0').Where(x => x != string.Empty).Count();

            //foreach (string packet in msg.Replace("\x0a", string.Empty).Split('\0').Where(x => x != string.Empty))
            //{
            //    data += packet;
            //    data += "\0";

            //}
            

            // Instance.SocketManager.ForwardToServer(msg);
        }
    }
}