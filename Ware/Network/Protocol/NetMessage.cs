﻿using System.Reflection;

namespace Ware.Network.Protocol
{
    public class NetMessage
    {
        public string Id { get; }
        public MethodInfo MethodInfo { get; }
        public object Instance { get; }

        public NetMessage(string id, MethodInfo methodInfo, object instance)
        {
            Id = id;
            MethodInfo = methodInfo;
            Instance = instance;
        }
    }
}