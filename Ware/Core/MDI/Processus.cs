﻿using EasyHook;
using No.Ankama._1._29;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Management;
using System.Runtime.InteropServices;
using System.Runtime.Remoting;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms.Integration;
using Ware.View.MITM;
using Ware.ViewModels;

namespace Ware.Core.MDI
{
    internal static class ProcessExtensions
    {
        public static IEnumerable<Process> GetChildProcesses(this Process process)
        {
            List<Process> children = new List<Process>();
            ManagementObjectSearcher mos = new ManagementObjectSearcher(String.Format("Select * From Win32_Process Where ParentProcessID={0}", process.Id));

            foreach (ManagementObject mo in mos.Get())
            {
                children.Add(Process.GetProcessById(Convert.ToInt32(mo["ProcessID"])));
            }

            return children;
        }
    }

    public class Processus
    {
        #region Methods/Consts for Embedding a Window

        [DllImport("user32.dll", EntryPoint = "GetWindowThreadProcessId", SetLastError = true,
           CharSet = CharSet.Unicode, ExactSpelling = true,
           CallingConvention = CallingConvention.StdCall)]
        private static extern long GetWindowThreadProcessId(long hWnd, long lpdwProcessId);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern long SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

        [DllImport("user32.dll", EntryPoint = "GetWindowLongA", SetLastError = true)]
        private static extern long GetWindowLong(IntPtr hwnd, int nIndex);

        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern long SetWindowPos(IntPtr hwnd, long hWndInsertAfter, long x, long y, long cx, long cy, long wFlags);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool MoveWindow(IntPtr hwnd, int x, int y, int cx, int cy, bool repaint);

        [DllImport("user32.dll", EntryPoint = "PostMessageA", SetLastError = true)]
        private static extern bool PostMessage(IntPtr hwnd, uint Msg, int wParam, int lParam);

        [DllImport("ntdll.dll")]
        private static extern int NtQueryInformationProcess(IntPtr ProcessHandle, int ProcessInformationClass, out PROCESS_BASIC_INFORMATION ProcessInformation, int ProcessInformationLength, out int ReturnLength);

        [DllImport("user32.dll")]
        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll", EntryPoint = "FindWindow", SetLastError = true)]
        private static extern IntPtr FindWindowByCaption(IntPtr ZeroOnly, string lpWindowName);

        [DllImport("user32.dll")]
        private static extern IntPtr GetMenu(IntPtr hWnd);

        [DllImport("user32.dll")]
        private static extern int GetMenuItemCount(IntPtr hMenu);

        [DllImport("user32.dll")]
        private static extern bool DrawMenuBar(IntPtr hWnd);

        [DllImport("user32.dll")]
        private static extern bool RemoveMenu(IntPtr hMenu, uint uPosition, uint uFlags);

        private static string injectionLibrary = Environment.CurrentDirectory + @"\No.Ankama.1.29.dll";
        private const int SW_MAXIMIZE = 3;
        private const int SWP_NOOWNERZORDER = 0x200;
        private const int SWP_NOREDRAW = 0x8;
        private const int SWP_NOZORDER = 0x4;
        private const int SWP_SHOWWINDOW = 0x0040;
        private const int WS_EX_MDICHILD = 0x40;
        private const int SWP_FRAMECHANGED = 0x20;
        private const int SWP_NOACTIVATE = 0x10;
        private const int SWP_ASYNCWINDOWPOS = 0x4000;
        private const int SWP_NOMOVE = 0x2;
        private const int SWP_NOSIZE = 0x1;
        private const int GWL_STYLE = (-16);
        private const int WS_VISIBLE = 0x10000000;
        private const int WM_CLOSE = 0x10;
        private const int WS_CHILD = 0x40000000;
        private const int WS_MAXIMIZE = 0x01000000;
        public static uint MF_BYPOSITION = 0x400;
        public static uint MF_REMOVE = 0x1000;
        public static int WS_BORDER = 0x00800000; //window with border
        public static int WS_DLGFRAME = 0x00400000; //window with double border but no title
        public static int WS_CAPTION = WS_BORDER | WS_DLGFRAME; //window with a title bar
        public static int WS_SYSMENU = 0x00080000; //window menu

        #endregion Methods/Consts for Embedding a Window

        [StructLayout(LayoutKind.Sequential)]
        private struct PROCESS_BASIC_INFORMATION
        {
            public int ExitStatus;
            public int PebBaseAddress;
            public int AffinityMask;
            public int BasePriority;
            public int UniqueProcessId;
            public int InheritedFromUniqueProcessId;
        }

        [DllImport("user32.dll")]
        private static extern bool SetLayeredWindowAttributes(IntPtr hwnd, uint crKey, byte bAlpha, uint dwFlags);

        public const int GWL_EXSTYLE = -20;
        public const int WS_EX_LAYERED = 0x80000;
        public const int LWA_ALPHA = 0x2;
        public const int LWA_COLORKEY = 0x1;

        private DashBoardMITM Parent { get; set; }
        private System.Windows.Forms.Panel Panel { get; set; }
        private WindowsFormsHost WindowsFormsHost { get; set; }
        public DashboardViewModel DashboardViewModel { get; set; }
        public Process Proc { get; set; }
        private string AppPath = "";
        private IntPtr mainHandle;
        private ProcessStartInfo processStartInfo;
        private string channelName = null;

        public Processus()
        {
        }

        public async Task Start(System.Windows.Forms.Panel panel, DashBoardMITM parent)
        {
            Parent = parent;
            Panel = panel;
            Parent.onResizingProcessEventHandler += Parent_onResizingProcessEventHandler;

            await Task.Factory.StartNew(() =>
            {
                VerifPathEnum vpe = VerifPath();
                switch (vpe)
                {
                    case VerifPathEnum.Success:
                        processStartInfo = new ProcessStartInfo(AppPath);
                        processStartInfo.WindowStyle = ProcessWindowStyle.Maximized;
                        Proc = Process.Start(processStartInfo);

                        Proc.WaitForInputIdle();
                        SetParentApp(panel);
                        break;

                    case VerifPathEnum.NoPathFile:
                        break;

                    case VerifPathEnum.PathUnknown:
                        break;
                }
            });
        }

        public VerifPathEnum VerifPath()
        {
            if (Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Ware"))
            {
            }
            else
            {
                Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Ware");
            }
            if (File.Exists((Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)) + @"\Ware\AppPath.txt"))
            {
                if (File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Ware\AppPath.txt") != "")
                {
                    AppPath = File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Ware\AppPath.txt");
                    if (AppPath.Contains("Dofus.exe"))
                    {
                        return VerifPathEnum.Success;
                    }
                    else
                    {
                        return VerifPathEnum.PathUnknown;
                    }
                }
                else
                {
                    MessageBox.Show("Le chemin d'accés à Dofus 1.29 est vide, impossible de continuer", "Chemin d'accés vide", MessageBoxButton.OK, MessageBoxImage.Error);
                    return VerifPathEnum.PathUnknown;
                }
            }
            else
            {
                File.Create((Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)) + @"\Ware\AppPath.txt");
                MessageBox.Show("Le chemin d'accés à Dofus 1.29 est vide, impossible de continuer", "Chemin d'accés vide", MessageBoxButton.OK, MessageBoxImage.Error);
                return VerifPathEnum.NoPathFile;
            }
        }

        private void SetParentApp(System.Windows.Forms.Panel panel)
        {
            if (Proc != null)
            {
                //IEnumerable<Process> list = ProcessExtensions.GetChildProcesses(Proc);
                //Proc = list.Where(p => p.ProcessName == "dofus.dll").First();

                IntPtr pFoundWindow = Proc.MainWindowHandle;
                int style = (int)GetWindowLong(pFoundWindow, GWL_STYLE);
                //get menu
                IntPtr HMENU = GetMenu(Proc.MainWindowHandle);
                //get item count
                int count = GetMenuItemCount(HMENU);
                //loop & remove
                for (int i = 0; i < count; i++)
                    RemoveMenu(HMENU, 0, (MF_BYPOSITION | MF_REMOVE));
                mainHandle = pFoundWindow;

                Application.Current.Dispatcher.Invoke(() =>
                {
                    //force a redraw
                    DrawMenuBar(Proc.MainWindowHandle);
                    Debug.WriteLine("set parent hanle " + Proc.MainWindowHandle.ToString());
                    SetWindowLong(pFoundWindow, GWL_STYLE, (style & ~WS_SYSMENU));
                    SetWindowLong(pFoundWindow, GWL_STYLE, (style & ~WS_CAPTION));

                    SetParent(Proc.MainWindowHandle, panel.Handle);
                    SetWindowLong(Proc.MainWindowHandle, GWL_STYLE, WS_VISIBLE + WS_MAXIMIZE);
                    MoveWindow(Proc.MainWindowHandle, 0, 0, panel.Width, panel.Height, true);
                });

                Proc.Refresh();
                var injectResult = Inject();
            }
            else
            {
                MessageBox.Show("Une erreur s'est produit (SetParentApp)", "Ware Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Parent_onResizingProcessEventHandler(object sender, EventArgs e)
        {
            System.Windows.Forms.Panel p = sender as System.Windows.Forms.Panel;
            if (!MoveWindow(mainHandle, 0, 0, p.Width, p.Height, true))
            {
                Debug.WriteLine("handle  " + Proc.MainWindowHandle.ToString());
            }
        }

        private InjectionResultEnum Inject()
        {
            if (File.Exists(Environment.CurrentDirectory + @"\No.Ankama.1.29.dll"))
            {
                try
                {
                    RemoteHooking.IpcCreateServer<RemoteInterface>(ref channelName, WellKnownObjectMode.Singleton);
                    RemoteHooking.Inject(
                            Proc.Id,
                            injectionLibrary,
                            injectionLibrary,
                            channelName
                        );

                    return InjectionResultEnum.Success;
                }
                catch (Exception e)
                {
                    return InjectionResultEnum.Failed;
                }
            }
            else
            {
                return InjectionResultEnum.FileNotFound;
            }
        }

        ~Processus()  // finalizer
        {
        }
    }
}