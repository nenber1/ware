﻿using System;

namespace Ware.Common.Attributes
{
    public class MessageHandlerAttribute : Attribute
    {
        public string Message { get; private set; }

        public MessageHandlerAttribute(string message)
        {
            Message = message;
        }
    }
}