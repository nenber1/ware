﻿using MaterialDesignThemes.Wpf;
using SwfDotNet.IO;
using SwfDotNet.IO.ByteCode;
using SwfDotNet.IO.ByteCode.Actions;
using SwfDotNet.IO.Tags;
using System;
using System.Collections;
using System.Diagnostics;
using System.Text;
using System.Windows;

namespace Ware.Common.Utils
{
    public static class Functions
    {
        public static Snackbar MainSnackBar { get; set; }

        public static void ShowToastMessage(string message)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                MainSnackBar.MessageQueue.Enqueue(message);
            }));
        }

        public static char[] caracteres_array = new char[]
         {
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
            'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F',
            'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
            'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '_'
         };

        public static string DecryptIP(string packet)
        {
            StringBuilder ip = new StringBuilder();

            for (int i = 0; i < 8; i += 2)
            {
                int ascii1 = packet[i] - 48;
                int ascii2 = packet[i + 1] - 48;

                if (i != 0)
                    ip.Append('.');
                Debug.WriteLine("ascii 1 : " + ascii1 + "ascii 2 : " + ascii2);
                Debug.WriteLine("\n");
                Debug.WriteLine("binary : " + (((ascii1 & 15) << 4) | (ascii2 & 15)).ToString());

                ip.Append(((ascii1 & 15) << 4) | (ascii2 & 15));
            }
            return ip.ToString();
        }

        public static string EncryptIp(string ip)
        {
            string[] bytes = ip.Split('.');
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.Length; i++)
            {
                int b = int.Parse(bytes[i]);
                sb.Append((char)((b >> 4) + 48)).Append((char)((b & 15) + 48));
            }
            return sb.ToString();
        }

        //port = nombre total
        //caracteres_array.length() = 64
        //a = 4096 * 0 = 0            car a = 0
        //g = 64 * 6 = 384            car g = 6
        //7 = 59                      car 7 = 59
        public static int DecryptPort(char[] chars)
        {
            if (chars.Length != 3)
                throw new ArgumentOutOfRangeException("The port must be 3-chars coded.");

            int port = 0;
            for (int i = 0; i < 2; i++)
            {
                port += (int)(Math.Pow(64, 2 - i) * get_Hash(chars[i]));
                //Debug.WriteLine((Math.Pow(64, 2 - i)) + " * " + get_Hash(chars[i]) + " = " + port);
            }

            port += get_Hash(chars[2]);
            return port;
        }

        public static string EncryptPort(int port)
        {
            int cur = port;
            StringBuilder sb = new StringBuilder();

            for (int i = 2; i > 0; i--)
            {
                sb.Append(caracteres_array[cur >> (6 * i)]);
                cur &= (int)(Math.Pow(64, i) - 1);
            }
            sb.Append(caracteres_array[cur]);
            return sb.ToString();
        }

        public static short get_Hash(char ch)
        {
            for (short i = 0; i < caracteres_array.Length; i++)
                if (caracteres_array[i] == ch)
                {
                    return i;
                }

            throw new IndexOutOfRangeException(ch + " is not in the hash table.");
        }

        public static string Get_Cell_Char(short cellID) => caracteres_array[cellID / 64] + "" + caracteres_array[cellID % 64];


        public static void ParseMapsPos(string path)
        {
            SwfDotNet.IO.SwfReader swfReader = new SwfDotNet.IO.SwfReader(path);
            Swf swf = swfReader.ReadSwf();
            Decompiler decompiler = new Decompiler(swf.Version);
            IEnumerator enumerator = swf.Tags.GetEnumerator();
            while (enumerator.MoveNext())
            {
                BaseTag current = (BaseTag)enumerator.Current;
                if (current.ActionRecCount != 0)
                {
                    IEnumerator currentenumerator = current.GetEnumerator();
                    while (currentenumerator.MoveNext())
                    {
                        foreach (BaseAction action in decompiler.Decompile((byte[])currentenumerator.Current))
                        {
                            //if(action.Code == (int)ActionCode.InitObject)
                            //{
                            //}
                            Debug.WriteLine(action.ToString());
                        }
                    }
                }
            }
        }
    }
}