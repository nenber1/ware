﻿using Ware.Common.Attributes;
using Ware.Models.Character;

namespace Ware.Common.Frame.MITM.Chat
{
    public class Chat : Frame
    {
        public Chat(Instance instance) : base(instance)
        {
        }

        [MessageHandler("cMK|")]
        public string HandleGeneral(string data)
        {
            Instance.Log(new LogMessage(Ware.Enums.LogType.General, data.Split('|')[3], data.Split('|')[2]));
            return data;
        }

        [MessageHandler("cMK:")]
        public string HandleCommerce(string data)
        {
            Instance.Log(new LogMessage(Ware.Enums.LogType.Commerce, data.Split('|')[3], data.Split('|')[2]));
            return data;
        }

        [MessageHandler("cMK?")]
        public string HandleRecrutement(string data)
        {
            Instance.Log(new LogMessage(Ware.Enums.LogType.Recrutement, data.Split('|')[3], data.Split('|')[2]));
            return data;
        }

        [MessageHandler("cMK^")]
        public string HandleIncarnam(string data)
        {
            Instance.Log(new LogMessage(Ware.Enums.LogType.General, data.Split('|')[3], data.Split('|')[2]));
            return data;
        }

        [MessageHandler("cMKi")]
        public string HandleInformation(string data)
        {
            Instance.Log(new LogMessage(Ware.Enums.LogType.Info, data.Split('|')[3], data.Split('|')[2]));
            return data;
        }

        [MessageHandler("cMK#")]
        public string HandleEquipe(string data)
        {
            Instance.Log(new LogMessage(Ware.Enums.LogType.Equipe, data.Split('|')[3], data.Split('|')[2]));
            return data;
        }

        [MessageHandler("cMK$")]
        public string HandleGroupe(string data)
        {
            Instance.Log(new LogMessage(Ware.Enums.LogType.Groupe, data.Split('|')[3], data.Split('|')[2]));
            return data;
        }

        [MessageHandler("cMK%")]
        public string HandleGuilde(string data)
        {
            Instance.Log(new LogMessage(Ware.Enums.LogType.Guilde, data.Split('|')[3], data.Split('|')[2]));
            return data;
        }

        [MessageHandler("cMKF")]
        public string HandleSendPrive(string data)
        {
            Instance.Log(new LogMessage(Ware.Enums.LogType.Prive, data.Split('|')[3], data.Split('|')[2]));
            return data;
        }

        [MessageHandler("cMKT")]
        public string HandleReceivePrive(string data)
        {
            Instance.Log(new LogMessage(Ware.Enums.LogType.Prive, data.Split('|')[3], data.Split('|')[2]));
            return data;
        }
    }
}