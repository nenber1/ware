﻿using Ware.Common.Attributes;
using Ware.Models.Character;

namespace Ware.Common.Frame.MITM.Chat
{
    public class GameMessages : Frame
    {
        public GameMessages(Instance instance) : base(instance)
        {
        }

        [MessageHandler("Im189")]
        public string HandleWelcomeGameMessage(string data)
        {
            Instance.InstanceState = Ware.Enums.InstanceState.Idle;
            Instance.Log(new LogMessage(Ware.Enums.LogType.Ware, "Connecté au personnage \"" + Instance.Name + "\""));
            Instance.Log(new LogMessage(Ware.Enums.LogType.Alert, "Bienvenue sur DOFUS, dans le Monde des Douze ! Rappel: prenez garde, il est interdit de transmettre votre identifiant de connexion ainsi que votre mot de passe."));
            return data;
        }

        [MessageHandler("Im039")]
        public string HandleSpectatorDisabled(string data)
        {
            Instance.Log(new LogMessage(Ware.Enums.LogType.Info, "Mode spectateur désactivé"));
            return data;
        }

        [MessageHandler("Im040")]
        public string HandleSpectatorEnabled(string data)
        {
            Instance.Log(new LogMessage(Ware.Enums.LogType.Info, "Mode spectateur activé"));
            return data;
        }

        [MessageHandler("Im0152")]
        public string HandleLastConnection(string data)
        {
            Instance.Log(new LogMessage(Ware.Enums.LogType.Info, "Précédente connexion sur votre compte effectuée le " + data.Substring(7).Split('~')[0] + "/" + data.Substring(5).Split('~')[1] + "/" + data.Substring(5).Split('~')[2] + " à " + data.Substring(5).Split('~')[3] + ":" + data.Substring(5).Split('~')[4] + " via l'adresse IP " + data.Substring(5).Split('~')[5]));
            return data;
        }

        [MessageHandler("Im020")]
        public string HandleBankMessage(string data)
        {
            Instance.Log(new LogMessage(Ware.Enums.LogType.Info, "Vous avez dû donner " + data.Split(';')[1] + " kamas pour accéder à ce coffre."));
            return data;
        }

        [MessageHandler("Im025")]
        public string HandlePetsMessage(string data)
        {
            Instance.Log(new LogMessage(Ware.Enums.LogType.Info, "Votre animal est si heureux de vous revoir !"));
            return data;
        }

        [MessageHandler("Im0157")]
        public string HandleSubscriptionChannelRestrictMessage(string data)
        {
            Instance.Log(new LogMessage(Ware.Enums.LogType.Info, "Ce canal est seulement disponible aux abonnés de niveau " + data.Split(';')[1]));
            return data;
        }

        [MessageHandler("Im037")]
        public string HandleMissingMessage(string data)
        {
            Instance.Log(new LogMessage(Ware.Enums.LogType.Info, "Vous maintenant considéré comme absent."));
            return data;
        }

        [MessageHandler("Im112")]
        public string HandleWeightMessage(string data)
        {
            Instance.Log(new LogMessage(Ware.Enums.LogType.Info, "Tu es trop chargé. Jetez quelques objets pour pouvoir bouger."));
            return data;
        }
    }
}