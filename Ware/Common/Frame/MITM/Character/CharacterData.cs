﻿using Ware.Common.Attributes;
using Ware.Models.Character;

namespace Ware.Common.Frame.MITM.Character
{
    public class CharacterData : Frame
    {
        public CharacterData(Instance instance) : base(instance)
        {
        }

        [MessageHandler("GCK")]
        public string HandleCharacterConnectionValidation(string data)
        {
            Instance.Name = data.Split('|')[2];

            Instance.InstanceState = Ware.Enums.InstanceState.Idle;
            return data;
        }

        [MessageHandler("As")]
        public string HandleCharacterCharacteristics(string data)
        {
            Instance.CharacterManager.TreatCaracteristics(data);
            return data;
        }

        [MessageHandler("Ow")]
        public string HandleCharacterPods(string data)
        {
            Instance.DashboardViewModel.Pods = data.Substring(2).Split('|')[0];
            Instance.DashboardViewModel.MaxPods = data.Substring(2).Split('|')[1];
            Instance.DashboardViewModel.PercentPods = ((((int.Parse(data.Substring(2).Split('|')[0])) * 100)) / (int.Parse(data.Substring(2).Split('|')[1]))).ToString();

            return data;
        }

        [MessageHandler("ASK")]
        public string HandleChoseCharacter(string data)
        {
            Instance.IdCharacter = data.Split('|')[1];
            Instance.Name = data.Split('|')[2];
            Instance.CharacterManager.TreatInventory(data);
            //data.Split('|')[10]
            return data;
        }

        [MessageHandler("SL")]
        public string HandleSpellsData(string data)
        {
            if(data.Substring(2,1) !="o")
            {
                Instance.CharacterManager.TreatSpells(data);
            }
            
            return data;
        }
        


    }
}