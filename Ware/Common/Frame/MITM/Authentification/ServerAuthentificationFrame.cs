﻿using System;
using System.Net;
using Ware.Common.Attributes;
using Ware.Common.Utils;
using Ware.Models.Character;
using Ware.Network.MITM;

namespace Ware.Common.Frame.MITM.Authentification
{
    public class ServerAuthentificationFrame : Frame
    {
        public ServerAuthentificationFrame(Instance instance) : base(instance)
        {
        }

        [MessageHandler("HC")]
        public string Connection(string data)
        {
            Instance.InstanceState = Ware.Enums.InstanceState.AuthenticationConnection;
            Instance.Log(new LogMessage(Ware.Enums.LogType.Ware, "Connexion au serveur d'authentification..."));
            return data;
        }

        [MessageHandler("Ad")]
        public string Credential(string data)
        {
            Instance.Name = data.Substring(2);
            return data;
        }

        [MessageHandler("AlEf")]
        public string HandleBadCredentials(string data)
        {
            Instance.Log(new LogMessage(Ware.Enums.LogType.Ware, Instance.Name + "Mauvais nom de compte ou mauvais mot de passe" + "  " + Instance.InstanceState.ToString()));
            return data;
        }

        [MessageHandler("AlEb")]
        public string HandleBanned(string data)
        {
            Instance.Log(new LogMessage(Ware.Enums.LogType.Ware, "Compte banni =("));
            return data;
        }

        [MessageHandler("AlEn")]
        public string HandleTimedOut(string data)
        {
            Instance.Log(new LogMessage(Ware.Enums.LogType.Ware, "Connexion interompue avec le serveur"));
            return data;
        }

        [MessageHandler("AlEa")]
        public string HandleAlreadyInConnection(string data)
        {
            Instance.Log(new LogMessage(Ware.Enums.LogType.Ware, "Compte déjà en cours de connexion"));
            return data;
        }

        [MessageHandler("AlEc")]
        public string HandleAlreadyInGameConnection(string data)
        {
            Instance.Log(new LogMessage(Ware.Enums.LogType.Ware, "Compte déjà en cours de connexion"));
            return data;
        }

        [MessageHandler("AlEv")]
        public string HandleNewVersion(string data)
        {
            Instance.Log(new LogMessage(Ware.Enums.LogType.Ware, "Nouvelle version de Dofus disponible"));
            Instance.InstanceState = Ware.Enums.InstanceState.Disconnected;
            return data;
        }

        [MessageHandler("AlEp")]
        public string HandleBadAccount(string data)
        {
            Instance.Log(new LogMessage(Ware.Enums.LogType.Ware, "Compte non valide"));
            return data;
        }

        [MessageHandler("AlEd")]
        public string HandleAccountDisconnectedt(string data)
        {
            Instance.Log(new LogMessage(Ware.Enums.LogType.Ware, "Le compte vient d'être déconnecté"));
            return data;
        }

        [MessageHandler("AlEk")]
        public string HandleInvalidAccount(string data)
        {
            Instance.Log(new LogMessage(Ware.Enums.LogType.Ware, "Compte invalide pendant " + data.Substring(4).Split('|')[0] + " jours, " + data.Substring(4).Split('|')[1] + " heurs et " + data.Substring(4).Split('|')[2] + " minutes"));
            return data;
        }

        //SERVER AVEC AXK

        [MessageHandler("AXK")]
        public string HandleGameServerAuthentificationX(string data)
        {
            Instance.InstanceState = Ware.Enums.InstanceState.ServerConnection;
            Instance.Log(new LogMessage(Ware.Enums.LogType.Ware, "Connexion au serveur de jeu..."));
            SocketManager.Ip = Functions.DecryptIP(data.Substring(3, 8));
            SocketManager.Port = Functions.DecryptPort(data.Substring(11, 3).ToCharArray()).ToString();
            data = data.Substring(0, 3) + Functions.EncryptIp("127.0.0.1") + Functions.EncryptPort(443) + data.Substring(14);
            //BaseSocket ic = SocketManager.ResetServer();

            return data;
        }

        //SERVER AVEC AYK
        [MessageHandler("AYK")]
        public string HandleGameServerAuthentificationY(string data)
        {
            Instance.InstanceState = Ware.Enums.InstanceState.ServerConnection;
            Instance.Log(new LogMessage(Ware.Enums.LogType.Ware, "Connexion au serveur de jeu..."));
            SocketManager.Ip = data.Split(';')[0].Substring(3);//data.Substring(3, 12);
            IPAddress ip;
            if (!IPAddress.TryParse(SocketManager.Ip, out ip))
            {
                SocketManager.Ip = Dns.GetHostAddresses(data.Split(';')[0].Substring(3))[0].ToString();
            }

            SocketManager.Port = "443";
            //data.Replace("eratz.ankama-games.com", "127.0.0.1");
            data = data.Substring(0, 3) + "127.0.0.1;" + data.Split(';')[1];
            return data;
        }

        [MessageHandler("AxK")]
        public string HandleServerChoice(string data)
        {
            Instance.InstanceState = Ware.Enums.InstanceState.ConnectionIdle;
            Instance.Log(new LogMessage(Ware.Enums.LogType.Ware, "Connecté au serveur d'authentification"));
            var date = (new DateTime(1970, 1, 1)).AddMilliseconds(double.Parse(data.Substring(3).Split('|')[0]));

            //var posixTime = DateTime.SpecifyKind(new DateTime(1970, 1, 1), DateTimeKind.Utc);
            //var time = posixTime.AddMilliseconds(double.Parse(data.Substring(3).Split('|')[0]));
            Instance.Subscription = date.Year + " An, " +  date.Month + " Mois, " + date.Day + " Jours, " + date.Hour + " Heures et " + date.Minute + " minutes";
            return data;
        }

        [MessageHandler("HG")]
        public string HandleGameServerAuthenticated(string data)
        {
            Instance.InstanceState = Ware.Enums.InstanceState.ConnectionIdle;
            return data;
        }

        [MessageHandler("ALK")]
        public string HandleCharacterSelection(string data)
        {
            Instance.InstanceState = Ware.Enums.InstanceState.ConnectionIdle;
            Ware.Enums.Server myEnum = (Ware.Enums.Server)int.Parse(data.Substring(3).Split('|')[2].Split(';')[9]);
            Instance.Log(new LogMessage(Ware.Enums.LogType.Ware, "Connecté au serveur de jeu " + myEnum.ToString()));

            return data;
        }
        [MessageHandler("M01")]
        public string HandleDisconnection(string data)
        {
            Instance.Disconnect();
            Instance.Log(new LogMessage(Ware.Enums.LogType.Ware, "Déconnecté pour inactivité "));

            return data;
        }
    }
}