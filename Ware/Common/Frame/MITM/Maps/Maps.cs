﻿using System.Windows;
using Ware.Common.Attributes;
using Ware.Models.Character;

namespace Ware.Common.Frame.MITM.Maps
{
    public class Maps : Frame
    {
        public Maps(Instance instance) : base(instance)
        {
        }

        [MessageHandler("GA0")]
        public string HandleMoving(string data)
        {
            if (data.Split(';')[2] == Instance.IdCharacter)
            {
                Instance.InstanceState = Ware.Enums.InstanceState.Moving;
            }

            return data;
        }

        [MessageHandler("GDM")]
        public string HandleMapData(string data)
        {
            Instance.MapManager.LoadMap(data);
            return data;
        }


        [MessageHandler("GKK0")]
        public string HandleEndMovement(string data)
        {
            Instance.InstanceState = Ware.Enums.InstanceState.Idle;
            return data;
        }

        [MessageHandler("GM")]
        public string HandleEntity(string data)
        {
            string msg = data.Substring(3);
            foreach (string entity in data.Split('|'))
            {
                //New Entity
                if(entity.Split(';')[0].Substring(0,1) == "+")
                {
                    if(entity.Split(';')[3] == Instance.IdCharacter)
                    {
                        Instance.MapManager.CurrentCell = Instance.MapManager.GetCellFromId(int.Parse(entity.Split(';')[0].Substring(1)));
                    }
                }
            }
            return data;
        }


    }
}