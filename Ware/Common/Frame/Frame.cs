﻿using System;
using System.Collections.Generic;
using System.Text;
using Ware.Models.Character;
using Ware.Network.MITM;

namespace Ware.Common.Frame
{
    public class Frame
    {
        protected Instance Instance;
        protected SocketManager SocketManager;
        public Frame(Instance instance)
        {
            Instance = instance;
            SocketManager = instance.SocketManager;
        }
    }
}
