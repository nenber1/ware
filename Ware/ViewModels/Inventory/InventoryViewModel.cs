﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ware.Models.Character;
using Ware.Models.LocalDatabase;

namespace Ware.ViewModels.Inventory
{
    public class InventoryViewModel : INotifyPropertyChanged
    {
        public InventoryViewModel(DashboardViewModel dvm)
        {
            DashboardViewModel = dvm;
            Items = new ObservableCollection<InventoryObject>();
        }
        #region Members

        private DashboardViewModel DashboardViewModel;
        private ObservableCollection<InventoryObject> items;
        #endregion Members
        #region Properties

        public ObservableCollection<InventoryObject> Items { get { return items; } set { items = value; RaisePropertyChanged("Items"); } }
        #endregion
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion INotifyPropertyChanged Members

        #region Methods
        private void RaisePropertyChanged(string propertyName)
        {
            // take a copy to prevent thread issues
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Methods
    }
}
