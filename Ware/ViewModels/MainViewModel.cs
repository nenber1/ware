﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using Ware.Enums;
using Ware.Models;
using Ware.View;
using Ware.View.MITM;
using Ware.View.Socket;
using Ware.ViewModels.MITM;
using Ware.ViewModels.Socket;

namespace Ware.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        #region Construction

        /// <summary>
        /// Constructs the default instance of a SongViewModel
        /// </summary>
        public MainViewModel()
        {
            _main = new Main
            {
                SnackMessage = "",
                Utilisateur = "Inconnu",
                DialogHostContent = new Finding(),
                DialogHostIsOpen = false
            };
            MainMenuItems = new ObservableCollection<MainMenuItem>();
            startPage = new StartPage();
            startPage.OnSessionBegin += StartPage_OnSessionBegin;
            MainMenuItems.Add(new MainMenuItem("General", startPage, new PackIcon { Kind = PackIconKind.Home }));
            MainMenuItems.Add(new MainMenuItem("Gestion de comptes", null, new PackIcon { Kind = PackIconKind.FolderAccount }));
            MainMenuItems.Add(new MainMenuItem("Parametres", null, new PackIcon { Kind = PackIconKind.Settings }));
        }

        /// <summary>
        /// User clik on on let's go on the start page
        /// </summary>
        /// <param name="sender"></param>
        private void StartPage_OnSessionBegin(object sender, WareType wareType)
        {
            if (wareType == WareType.MITM)
            {
                WareType = WareType.MITM;
                InstanceContainerViewModelMITM instanceContainerViewModelMITM = new InstanceContainerViewModelMITM(this);
                ChangeMenuItemContent(startPage, new InstanceContainerMITM(instanceContainerViewModelMITM));
            }
            else
            {
                WareType = WareType.Socket;
                InstanceContainerViewModelSocket instanceContainerViewModelSocket = new InstanceContainerViewModelSocket(this);
                ChangeMenuItemContent(startPage, new InstanceContainerSocket(instanceContainerViewModelSocket));
            }
        }

        #endregion Construction

        #region Members

        private Main _main;
        private StartPage startPage;
        private ObservableCollection<MainMenuItem> mainMenuItems;

        #endregion Members

        #region Properties

        public WareType WareType { get; set; }

        public Main Main
        {
            get
            {
                return _main;
            }
            set
            {
                _main = value;
            }
        }

        public string SnackMessage
        {
            get { return Main.SnackMessage; }
            set
            {
                if (Main.SnackMessage != value)
                {
                    Main.SnackMessage = value;
                    RaisePropertyChanged("SnackMessage");
                }
            }
        }

        public string Utilisateur
        {
            get { return Main.Utilisateur; }
            set
            {
                Main.Utilisateur = value;
                RaisePropertyChanged("Utilisateur");
            }
        }

        public object DialogHostContent
        {
            get { return Main.DialogHostContent; }
            set
            {
                Main.DialogHostContent = value;
                RaisePropertyChanged("DialogHostContent");
            }
        }

        public bool DialogHostIsOpen
        {
            get { return Main.DialogHostIsOpen; }
            set
            {
                Main.DialogHostContent = value;
                RaisePropertyChanged("DialogHostIsOpen");
            }
        }

        public ObservableCollection<MainMenuItem> MainMenuItems
        {
            get { return mainMenuItems; }
            set { mainMenuItems = value; RaisePropertyChanged("MainMenuItems"); }
        }

        public void ChangeMenuItemContent(object oldContent, UserControl newContent)
        {
            try
            {
                foreach (MainMenuItem menuItem in MainMenuItems)
                {
                    if (menuItem.Content == oldContent)
                    {
                        menuItem.Content = newContent;
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            if (newContent.GetType() == typeof(StartPage))
            {
                startPage = (StartPage)newContent;
                startPage.OnSessionBegin += StartPage_OnSessionBegin;
            }
        }

        public void ChangeMenuItemContent(string name, UserControl newContent)
        {
            try
            {
                foreach (MainMenuItem menuItem in MainMenuItems)
                {
                    if (menuItem.Name == name)
                    {
                        menuItem.Content = newContent;
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            if (newContent.GetType() == typeof(StartPage))
            {
                startPage = (StartPage)newContent;
                startPage.OnSessionBegin += StartPage_OnSessionBegin;
            }
        }

        public void ChangeMenuItemContent(int index, UserControl newContent)
        {
            try
            {
                MainMenuItems[index].Content = newContent;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            if (newContent.GetType() == typeof(StartPage))
            {
                startPage = (StartPage)newContent;
                startPage.OnSessionBegin += StartPage_OnSessionBegin;
            }
        }

        #endregion Properties

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion INotifyPropertyChanged Members

        #region Methods

        private void RaisePropertyChanged(string propertyName)
        {
            // take a copy to prevent thread issues
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Methods
    }

    public class MainMenuItem : INotifyPropertyChanged
    {
        private UserControl _content;
        private string _name;
        private PackIcon _packIcon;

        public MainMenuItem(string name, UserControl content, PackIcon packIcon)
        {
            Name = name;
            Content = content;
            PackIcon = packIcon;
        }

        public string Name
        {
            get { return _name; }
            set { this._name = value; this.RaisePropertyChanged("Name"); }
        }

        public UserControl Content
        {
            get { return _content; }
            set { this._content = value; this.RaisePropertyChanged("Content"); }
        }

        public PackIcon PackIcon
        {
            get { return _packIcon; }
            set { _packIcon = value; this.RaisePropertyChanged("PackIcon"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            // take a copy to prevent thread issues
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}