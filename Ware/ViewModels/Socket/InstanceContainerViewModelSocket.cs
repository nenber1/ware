﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using Ware.Models.Character;

namespace Ware.ViewModels.Socket
{
    public class InstanceContainerViewModelSocket : INotifyPropertyChanged
    {
        public ObservableCollection<Instance> Items { get; }

        public AnotherCommandImplementation AddCommand { get; }

        public AnotherCommandImplementation RemoveSelectedItemCommand { get; }

        public BaseInstance SelectedItem { get; set; }

        private MainViewModel MainViewModel;

        public InstanceContainerViewModelSocket(MainViewModel mainViewModel)
        {
            MainViewModel = mainViewModel;
            //Ceation de fausses instances
            Items = new ObservableCollection<Instance>
            {
                new Instance("Personnage 1",Enums.WareType.Socket),
            };

            AddCommand = new AnotherCommandImplementation(
               _ =>
               {
                   Items.Add(new Instance("Personnage ", Enums.WareType.Socket));
               });
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion INotifyPropertyChanged Members

        #region Methods

        private void RaisePropertyChanged(string propertyName)
        {
            // take a copy to prevent thread issues
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Methods
    }
}