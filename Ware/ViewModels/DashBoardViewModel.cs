﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Ware.Models;
using Ware.Models.Character;
using Ware.Models.Map;
using Ware.View.Personnage;
using Ware.View.Inventory;
using Ware.ViewModels.Inventory;
using Ware.ViewModels.Personnage;

namespace Ware.ViewModels
{
    public class DashboardViewModel : INotifyPropertyChanged
    {
        #region Construction

        /// <summary>
        /// Constructs the default instance of a DashBoarsViewModel
        /// </summary>
        public DashboardViewModel()
        {
            _user = new DashBoardModel { Life = "0", Power = "0", Pods = " 0" };
            LogMessages = new ObservableCollection<LogMessage>();
            LogMessages.CollectionChanged += LogMessages_CollectionChanged;
            LogMaxLenght = 100;
            DebugMessages = new ObservableCollection<DebugMessage>();
            BotMessages = new ObservableCollection<BotMessage>();
            BotMessages.CollectionChanged += BotMessages_CollectionChanged;
            spellsViewModel = new SpellsViewModel(this);
            caracteristicsViewModel = new CaracteristicsViewModel(this);
            inventoryViewModel = new InventoryViewModel(this);
            
            SpellCaracteristicContent = new Spell();
            ((Spell)SpellCaracteristicContent).DataContext = spellsViewModel;
            InventoryContent = new View.Inventory.Inventory();
            ((View.Inventory.Inventory)InventoryContent).DataContext = inventoryViewModel;

            //COMMANDS
            SpellCaracteristicCommand = new AnotherCommandImplementation(arg =>
            {
                if((string)arg == "SpellTab")
                {
                    SpellCaracteristicContent = new Spell();
                    ((Spell)SpellCaracteristicContent).DataContext = spellsViewModel;
                }
                else if ((string)arg == "CaracteristicTab")
                {
                    SpellCaracteristicContent = new View.Personnage.Caracteristic();
                    ((View.Personnage.Caracteristic)SpellCaracteristicContent).DataContext = caracteristicsViewModel;
                }
            });
            CopyDebugMessages = new AnotherCommandImplementation(debugs =>
            {
                string s = "";
                foreach (DebugMessage item in DebugMessages.Where(x => x.Message != null))
                {
                    s += item.Message + "\n";
                }
                Clipboard.SetText(s);
                Clipboard.Flush();
                
            });
            SendMessageCommand = new AnotherCommandImplementation(a =>{
                Engine.Managers.CommandManager.ParseCommand(Enums.LogType.Ware, a.ToString());
                CommandText = "";
            });
        }

        private void BotMessages_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (BotMessages.Count > 478)
            {
                new System.Threading.Thread(t =>
                {
                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        BotMessages.RemoveAt(0);
                        RaisePropertyChanged(propertyName: "BotMessages");
                    });
                }).Start();
            }
        }

        private void LogMessages_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                if (LogMessages.Count > LogMaxLenght)
                {
                    new System.Threading.Thread(t =>
                    {
                        Application.Current.Dispatcher.Invoke((Action)delegate
                        {
                            LogMessages.RemoveAt(0);
                            RaisePropertyChanged(propertyName: "LogMessages");
                        });
                    }).Start();
                }
                IsLogsChangedPropertyInViewModel = true;
            }
        }

        #endregion Construction

        #region Members

        private DashBoardModel _user;
        private string kamas = "0";
        private string life = "0";
        private string maxLife = "0";
        private string percentLife = "0";
        private string power = "0";
        private string maxPower = "0";
        private string percentPower = "0";
        private string pods = "0";
        private string maxPods = "0";
        private string percentPods = "0";
        private string commandText = "";
        private bool isLogsChangedPropertyInViewModel = true;
        private SpellsViewModel spellsViewModel;
        private CaracteristicsViewModel caracteristicsViewModel;
        private InventoryViewModel inventoryViewModel;
        private object spellCaracteristicContent;
        private object inventoryContent;

        #endregion Members

        #region Properties

        public AnotherCommandImplementation CopyDebugMessages { get; }
        public AnotherCommandImplementation SendMessageCommand { get; }
        public AnotherCommandImplementation SpellCaracteristicCommand { get; }
        public object SpellCaracteristicContent { get { return spellCaracteristicContent; } set { spellCaracteristicContent = value; RaisePropertyChanged("SpellCaracteristicContent"); } }
        public object InventoryContent { get { return inventoryContent; } set { inventoryContent = value; RaisePropertyChanged("InventoryContent"); } }
        private ObservableCollection<LogMessage> logMessages;
        public ObservableCollection<LogMessage> LogMessages { get { return logMessages; } set { logMessages = value; RaisePropertyChanged("LogMessages"); } }
        private ObservableCollection<DebugMessage> debugMessages;
        public ObservableCollection<DebugMessage> DebugMessages { get { return debugMessages; } set { debugMessages = value; RaisePropertyChanged("DebugMessages"); } }
        private ObservableCollection<BotMessage> botMessages;
        public ObservableCollection<BotMessage> BotMessages { get { return botMessages; } set { botMessages = value; RaisePropertyChanged("BotMessages"); } }
        private ObservableCollection<Cell> cells = new ObservableCollection<Cell>();
        public ObservableCollection<Cell> Cells { get { return cells; } set { cells = value; RaisePropertyChanged("Cells"); MessageBox.Show(cells.Count.ToString()); } }

        public int LogMaxLenght { get; set; }

        public DashBoardModel User
        {
            get
            {
                return _user;
            }
            set
            {
                _user = value;
            }
        }
        public SpellsViewModel SpellsViewModel
        {
            get { return spellsViewModel; }
            set
            {
                if (spellsViewModel != value)
                {
                    spellsViewModel = value;
                    RaisePropertyChanged("SpellsViewModel");
                }
            }
        }
        public CaracteristicsViewModel CaracteristicsViewModel
        {
            get { return caracteristicsViewModel; }
            set
            {
                if (caracteristicsViewModel != value)
                {
                    caracteristicsViewModel = value;
                    RaisePropertyChanged("CaracteristicsViewModel");
                }
            }
        }
        public InventoryViewModel InventoryViewModel
        {
            get { return inventoryViewModel; }
            set
            {
                if (inventoryViewModel != value)
                {
                    inventoryViewModel = value;
                    RaisePropertyChanged("InventoryViewModel");
                }
            }
        }
        public bool IsLogsChangedPropertyInViewModel
        {
            get { return isLogsChangedPropertyInViewModel; }
            set
            {
                if (isLogsChangedPropertyInViewModel != value)
                {
                    isLogsChangedPropertyInViewModel = value;
                    RaisePropertyChanged("isLogsChangedPropertyInViewModel");
                }
            }
        }

        public string CommandText
        {
            get { return commandText; }
            set
            {
                if (commandText != value)
                {
                    commandText = value;
                    RaisePropertyChanged("CommandText");
                }
            }
        }
        public string Life
        {
            get { return life; }
            set
            {
                if (life != value)
                {
                    life = value;
                    RaisePropertyChanged("Life");
                }
            }
        }

        public string MaxLife
        {
            get { return maxLife; }
            set
            {
                if (maxLife != value)
                {
                    maxLife = value;
                    RaisePropertyChanged("MaxLife");
                }
            }
        }

        public string PercentLife
        {
            get
            {
                return percentLife;
            }
            set
            {
                percentLife = value;
                RaisePropertyChanged("PercentLife");
            }
        }

        public string Power
        {
            get { return power; }
            set
            {
                if (power != value)
                {
                    power = value;
                    RaisePropertyChanged("Power");
                }
            }
        }

        public string MaxPower
        {
            get { return maxPower; }
            set
            {
                if (maxPower != value)
                {
                    maxPower = value;
                    RaisePropertyChanged("MaxPower");
                }
            }
        }

        public string PercentPower
        {
            get { return percentPower; }
            set
            {
                if (percentPower != value)
                {
                    percentPower = value;
                    RaisePropertyChanged("PercentPower");
                }
            }
        }

        public string Pods
        {
            get { return pods; }
            set
            {
                if (pods != value)
                {
                    pods = value;
                    RaisePropertyChanged("Pods");
                }
            }
        }

        public string MaxPods
        {
            get { return maxPods; }
            set
            {
                if (maxPods != value)
                {
                    maxPods = value;
                    RaisePropertyChanged("MaxPods");
                }
            }
        }

        public string PercentPods
        {
            get { return percentPods; }
            set
            {
                if (percentPods != value)
                {
                    percentPods = value;
                    RaisePropertyChanged("PercentPods");
                }
            }
        }

        public string Kamas
        {
            get { return kamas; }
            set
            {
                if (kamas != value)
                {
                    kamas = value;
                    RaisePropertyChanged("Kamas");
                }
            }
        }

        public string Abonnement
        {
            get { return User.Abonnement; }
            set
            {
                if (User.Abonnement != value)
                {
                    User.Abonnement = value;
                    RaisePropertyChanged("Abonnement");
                }
            }
        }

        #endregion Properties

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion INotifyPropertyChanged Members

        #region Methods

        

        private void RaisePropertyChanged(string propertyName)
        {
            // take a copy to prevent thread issues
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Methods
    }
}