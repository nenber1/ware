﻿using LiteDB;
using MaterialDesignThemes.Wpf;
using Microsoft.Win32;
using Parse;
using SwfDotNet.IO;
using SwfDotNet.IO.ByteCode;
using SwfDotNet.IO.ByteCode.Actions;
using SwfDotNet.IO.Tags;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Ware.Models.LocalDatabase;
using Ware.View;
using static Parse.ParseException;

namespace Ware.ViewModels
{
    public class LoginViewModel : INotifyPropertyChanged
    {
        private UserControl SignIn;
        private UserControl SignUp;
        private string _dbPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Ware\config.db3";

        #region Constructor

        public LoginViewModel()
        {
            SignIn = new SignIn();
            SignUp = new SignUp();
            Content = SignIn;
            Application.Current.Exit += Current_Exit;

            if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Ware"))
            {
                Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData));
            }
            if (!File.Exists(_dbPath))
            {
                FileStream fs = File.Create(_dbPath);
                fs.Close();
            }
            using (var db = new ConfigContext())
            {
                db.Database.EnsureCreated();
                try
                {
                    var conf = db.Configs.Single(b => b.ConfigId == 1);
                    Path = conf.GamePath;
                    if (conf.RememberMe == true)
                    {
                        Login = conf.Login;
                        Password = conf.Password;
                        RememberMe = true;
                        if (Login != string.Empty && Password != string.Empty && Path != string.Empty)
                        {
                            ConnectionAuthorized = true;
                        }
                        else
                        {
                            ConnectionAuthorized = false;
                        }
                    }
                }
                catch (Exception e)
                {
                }
            }
            ParseClient.Initialize(new ParseClient.Configuration
            {
                ApplicationId = "iOLg93KHLQ4gEa9dkpxEDG6P2vhSqsmY87TJuhpH",
                WindowsKey = "P1UqbxOP0LXHrxYZqU8yp5oAs90FmPcDFID9o7D8",
                Server = "https://parseapi.back4app.com/"
            });

            #region ChangeContent

            ChangeContent = new AnotherCommandImplementation(
            button =>
            {
                ErrorMsg = "";
                if ((string)button == "ConnectionTab")
                {
                    Content = SignIn;
                }
                else if ((string)button == "SubscriptionTab")
                {
                    Content = SignUp;
                }
            });

            #endregion ChangeContent

            #region RememberButton

            RememberCommand = new AnotherCommandImplementation(
               arg =>
               {
                   if ((bool)arg == true)
                   {
                       Task.Factory.StartNew(() =>
                       {
                           if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Ware"))
                           {
                               Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData));
                           }
                           if (!File.Exists(_dbPath))
                           {
                               File.Create(_dbPath);
                           }
                           using (var db = new ConfigContext())
                           {
                               db.Database.EnsureCreated();
                               if (db.Configs.Count() < 1)
                               {
                                   db.AddAsync(new Config { GamePath = (string)path });
                               }
                               else
                               {
                                   try
                                   {
                                       var conf = db.Configs.Single(b => b.ConfigId == 1);
                                       conf.GamePath = (string)path;
                                       conf.RememberMe = true;
                                       conf.Login = Login;
                                       conf.Password = Password;
                                       db.Update(conf);
                                   }
                                   catch (Exception)
                                   {
                                   }
                               }
                               db.SaveChanges();
                           }
                       });
                   }
               });

            #endregion RememberButton

            #region ConnectionButton

            ConnectionCommand = new AnotherCommandImplementation(async connectionButton =>
            {
                ((Button)connectionButton).IsEnabled = false;
                MaterialDesignThemes.Wpf.ButtonProgressAssist.SetIsIndicatorVisible(((Button)connectionButton), true);
                if (RememberMe == true)
                {
                    await Task.Factory.StartNew(() =>
                    {
                        if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Ware"))
                        {
                            Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData));
                        }
                        if (!File.Exists(_dbPath))
                        {
                            File.Create(_dbPath);
                        }
                        using (var db = new ConfigContext())
                        {
                            db.Database.EnsureCreated();
                            if (db.Configs.Count() < 1)
                            {
                                _ = db.AddAsync(new Config { GamePath = (string)path });
                            }
                            else
                            {
                                try
                                {
                                    var conf = db.Configs.Single(b => b.ConfigId == 1);
                                    conf.GamePath = (string)path;
                                    conf.RememberMe = true;
                                    conf.Login = Login;
                                    conf.Password = Password;
                                    db.Update(conf);
                                }
                                catch (Exception)
                                {
                                }
                            }
                            db.SaveChanges();
                        }
                    });
                }
                ParseUser user = new ParseUser();
                try
                {
                    user = await ParseUser.LogInAsync(Login, Password);
                    // Login was successful.
                    if (user != null)
                    {
                        //TopMenuVisible = Visibility.Collapsed;
                        //Content = new Finding();
                        // await ExtractGameData(Path);
                    }
                }
                catch (ParseException e)
                {
                    HandleError(e);
                }
                ((Button)connectionButton).IsEnabled = true;
                MaterialDesignThemes.Wpf.ButtonProgressAssist.SetIsIndicatorVisible(((Button)connectionButton), false);
            });

            #endregion ConnectionButton

            #region BrowsePathButton

            BrowsePathCommand = new AnotherCommandImplementation(browseButton =>
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Title = "Chemin d'accés à Dofus Retro";
                openFileDialog.DefaultExt = ".exe";
                openFileDialog.Filter = "Application (.exe)|*.exe";

                // Display OpenFileDialog by calling ShowDialog method
                Nullable<bool> result = openFileDialog.ShowDialog();

                // Get the selected file name and display in a TextBox
                if (result == true)
                {
                    // Open document
                    //if(((ValidationRule)browseButton).ValidationStep == ValidationStep.)
                    string filename = openFileDialog.FileName;
                    Path = filename;
                    MessageBox.Show(Path);
                    Task.Factory.StartNew(() =>
                    {
                        if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Ware"))
                        {
                            Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData));
                        }
                        if (!File.Exists(_dbPath))
                        {
                            File.Create(_dbPath);
                        }
                        using (var db = new ConfigContext())
                        {
                            db.Database.EnsureCreated();
                            if (db.Configs.Count() < 1)
                            {
                                db.AddAsync(new Config { GamePath = (string)path });
                            }
                            else
                            {
                                try
                                {
                                    var conf = db.Configs.Single(b => b.ConfigId == 1);
                                    conf.GamePath = (string)path;
                                    db.Update(conf);
                                }
                                catch (Exception)
                                {
                                }
                            }
                            db.SaveChanges();
                        }
                    });
                }
            });

            #endregion BrowsePathButton

            #region SubscriptionButton

            SubscriptionCommand = new AnotherCommandImplementation(async subscriptionButton =>
            {
                ((Button)subscriptionButton).IsEnabled = false;
                MaterialDesignThemes.Wpf.ButtonProgressAssist.SetIsIndicatorVisible(((Button)subscriptionButton), true);
                if (Cgu == true && LoginS != "" && PasswordS != "" && ConfirmPasswordS != "" && MailS != "")
                {
                    try
                    {
                        ParseUser newuser = new ParseUser();
                        newuser.Username = LoginS;
                        newuser.Password = PasswordS;
                        newuser.Email = MailS;
                        await newuser.SignUpAsync();
                        ErrorMsg = "";
                        if (newuser.IsNew)
                        {
                            var message = "Bienvenue à toi " + LoginS + " =)";
                            //await Task.Factory.StartNew(() =>{Thread.Sleep(1000);}).ContinueWith(t =>{ LoginSnackbar.MessageQueue.Enqueue(message); }, TaskScheduler.FromCurrentSynchronizationContext());
                            LoginS = "";
                            PasswordS = "";
                            ConfirmPasswordS = "";
                            MailS = "";
                            Cgu = false;
                        }
                    }
                    catch (ParseException e)
                    {
                        HandleError(e);
                    }
                }

                ((Button)subscriptionButton).IsEnabled = true;
                MaterialDesignThemes.Wpf.ButtonProgressAssist.SetIsIndicatorVisible(((Button)subscriptionButton), false);
            });

            #endregion SubscriptionButton

            this.PropertyChanged += LoginViewModel_PropertyChanged;
            Init();
        }

        #endregion Constructor

        private async void Init()
        {
            using (var db = new ConfigContext())
            {
                db.Database.EnsureCreated();
                if (db.Areas.Count() < 45 || db.SubAreas.Count() < 370 || db.Maps.Count() < 7768)
                {
                    TopMenuVisible = Visibility.Collapsed;
                    Content = new ExtractGameData { DataContext = this };
                    bool extract = await DownloadGameDataAsync();
                    if (extract == true)
                    {
                        await Task.Factory.StartNew(() =>
                        {
                            Content = SignIn;
                            TopMenuVisible = Visibility.Visible;
                            Title = "Bienvenue sur Ware !";
                        });
                    }
                }
            }
        }

        private async Task<bool> DownloadGameDataAsync()
        {
            Title = "Préparation des données nécessaires";
            LibelleExtract = "Veuillez patienter...";
            try
            {
                //AREA
                ParseQuery<ParseObject> query = ParseObject.GetQuery("Area");
                MaxAmountDataExtract = await query.CountAsync();
                query = query.Limit(MaxAmountDataExtract);
                LibelleExtract = "Téléchargement des données... (1/3)";
                IEnumerable<ParseObject> results = await query.FindAsync();
                
                    //.ContinueWith(t =>
                    //{
                    //    Debug.WriteLine(t.Status.ToString());
                    //    return t.Result;
                    //});
                ExtractUnderterminate = false;
                LibelleExtract = "Extraction des données (1/3)";
                Area area;
                using (var db = new ConfigContext())
                {
                    if (db.Areas.Count() < 45)
                    {
                        await Task.Factory.StartNew(() =>
                        {
                            foreach (ParseObject m in results)
                            {
                                int Id = m.Get<int>("Id");
                                string Name = m.Get<string>("Name");
                                AmountDataExtract++;
                                PercentExtract = (100 * AmountDataExtract) / MaxAmountDataExtract;
                                area = new Area() { Id = Id, Name = Name };
                                db.Areas.Add(area);
                            }
                            MaxAmountDataExtract = 0;
                            AmountDataExtract = 0;
                            PercentExtract = 0;
                            db.SaveChanges();
                        });
                      
                    }
                }

                //SUBAREA
                LibelleExtract = "Téléchargement des données... (2/3)";
                query = ParseObject.GetQuery("SubArea");
                MaxAmountDataExtract = await query.CountAsync();
                query = query.Limit(MaxAmountDataExtract);
                results = await query.FindAsync();
                LibelleExtract = "Extraction des données (2/3)";
                SubArea subArea;
                using (var db = new ConfigContext())
                {
                    if (db.SubAreas.Count() < 370)
                    {
                        await Task.Factory.StartNew(() =>
                        {
                            foreach (ParseObject m in results)
                            {
                                int Id = m.Get<int>("Id");
                                string Name = m.Get<string>("Name");
                                int Area = m.Get<int>("Area");
                                AmountDataExtract++;
                                PercentExtract = (100 * AmountDataExtract) / MaxAmountDataExtract;
                                subArea = new SubArea() { Id = Id, Name = Name, Area = Area };
                                db.SubAreas.Add(subArea);
                            }
                            MaxAmountDataExtract = 0;
                            AmountDataExtract = 0;
                            PercentExtract = 0;
                            db.SaveChanges();
                        });
                       
                    }
                }

                //MAPS
                LibelleExtract = "Téléchargement des données... (3/3)";
                query = ParseObject.GetQuery("Map");
                MaxAmountDataExtract = await query.CountAsync();
                query = query.Limit(MaxAmountDataExtract);

                results = await query.FindAsync();
                LibelleExtract = "Extraction des données (3/3)";
                Map map;
                using (var db = new ConfigContext())
                {
                    if (db.Maps.Count() < 7768)
                    {
                        await Task.Factory.StartNew(() =>
                        {
                            foreach (ParseObject m in results)
                            {
                                int Id = m.Get<int>("Id");
                                int Width = m.Get<int>("Width");
                                int Height = m.Get<int>("Height");
                                int X = m.Get<int>("X");
                                int Y = m.Get<int>("Y");
                                string Data = m.Get<string>("Data");
                                string Teleports = m.Get<string>("Teleports") ?? "";
                                int SubArea = -1;
                                bool temp;
                                if (m.TryGetValue("SubArea", out temp) == true)
                                {
                                    SubArea = m.Get<int>("SubArea");
                                }
                                AmountDataExtract++;
                                PercentExtract = (100 * AmountDataExtract) / MaxAmountDataExtract;
                                map = new Map() { Id = Id, Width = Width, Height = Height, X = X, Y = Y, Data = Data, Teleports = Teleports, SubArea = SubArea };
                                db.Maps.Add(map);
                            }
                            db.SaveChanges();
                        });
                        
                        
                    }
                }
                LibelleExtract = "Extraction terminée";
                await Task.Delay(3000);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            return true;

            //Task.Factory.StartNew(() =>
            //{
            //    using (var webClient = new WebClient())
            //    {
            //        if (!Directory.Exists(Directory.GetCurrentDirectory() + "/temp"))
            //        {
            //            Directory.CreateDirectory(Directory.GetCurrentDirectory() + "/temp");
            //        }
            //        LibelleExtract = "Téléchargement des données nécessaires...";
            //        webClient.DownloadFile("http://dofusretro.cdn.ankama.com/lang/swf/maps_fr_366.swf", "temp/maps.swf");
            //    }
            //    LibelleExtract = "Traitement des données...";
            //    Fonctions.ParseMapsPos("temp/maps.swf");
            //});
        }

        private async Task ExtractGameData(string path)
        {
            string[] maps = Directory.GetFiles(path.Substring(0, path.Length - 9) + @"data\maps");
            int totalMaps = maps.Length;
            AmountDataExtract = totalMaps;
            //CurrentAmountDataExtract = "0 / " + AmountDataExtract;
            try
            {
                using (var db = new ConfigContext())
                {
                    SwfReader swfReader = new SwfReader(maps[0]);

                    Swf swf = swfReader.ReadSwf();

                    IEnumerator tagsEnu = swf.Tags.GetEnumerator();

                    while (tagsEnu.MoveNext())
                    {
                        BaseTag tag = (BaseTag)tagsEnu.Current;

                        if (tag.ActionRecCount != 0)
                        {
                            string sb = "";
                            IEnumerator enum2 = tag.GetEnumerator();

                            while (enum2.MoveNext())
                            {
                                Decompiler dc = new Decompiler(swf.Version);
                                ArrayList actions = dc.Decompile((byte[])enum2.Current);

                                int i = 0;
                                foreach (BaseAction obj in actions)
                                {
                                    //CurrentAmountDataExtract = i.ToString() + " / " + AmountDataExtract;
                                    //Debug.WriteLine(obj.Code.ToString());
                                    try
                                    {
                                        sb += obj.ToString() + "\n";
                                        Debug.WriteLine(obj.Code.ToString());
                                    }
                                    catch (Exception)
                                    {
                                        throw;
                                    }
                                    i++;
                                }
                            }

                            string map_data = sb.ToString().Split('\'')[29];
                            string map_id = sb.ToString().Split(new string[] { "push" }, StringSplitOptions.None)[8].Split(new string[] { " " }, StringSplitOptions.None)[1];
                            string map_width = sb.ToString().Split(new string[] { "push" }, StringSplitOptions.None)[10].Split(new string[] { " " }, StringSplitOptions.None)[1];
                            string map_height = sb.ToString().Split(new string[] { "push" }, StringSplitOptions.None)[12].Split(new string[] { " " }, StringSplitOptions.None)[1];

                            //db.Maps.Add(new Map { MapData = map_data, Id = int.Parse(map_id), Height = int.Parse(map_height) ,Width = int.Parse(map_width)}) ;

                            //Cell[] cells = Fonctions.DecompressMap(map_data);
                            //foreach (Cell cell in cells)
                            //{
                            //    if (cell.Type != TypeCell.PATH)
                            //        Debug.WriteLine("maps : " + map_id + "\n" + cell.ToString());
                            //}
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void Current_Exit(object sender, ExitEventArgs e)
        {
            if (RememberMe == true)
            {
                Task.Factory.StartNew(() =>
                {
                    if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Ware"))
                    {
                        Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData));
                    }
                    if (!File.Exists(_dbPath))
                    {
                        File.Create(_dbPath);
                    }
                    using (var db = new ConfigContext())
                    {
                        db.Database.EnsureCreated();
                        if (db.Configs.Count() < 1)
                        {
                            db.AddAsync(new Config { GamePath = (string)path });
                        }
                        else
                        {
                            try
                            {
                                var conf = db.Configs.Single(b => b.ConfigId == 1);
                                conf.GamePath = (string)path;
                                conf.RememberMe = true;
                                conf.Login = Login;
                                conf.Password = Password;
                                db.Update(conf);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        db.SaveChanges();
                    }
                });
            }
        }

        private void LoginViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (Content == SignUp)
            {
                if (Login != string.Empty && Password != string.Empty && Path != string.Empty)
                {
                    ConnectionAuthorized = true;
                }
                else
                {
                    ConnectionAuthorized = false;
                }
                if (LoginS != "" && PasswordS != "" && ConfirmPasswordS != "" && MailS != "" && Cgu != false && (PasswordS == ConfirmPasswordS))
                {
                    SubscriptionAuthorized = true;
                }
                else if (PasswordS != ConfirmPasswordS)
                {
                    ErrorMsg = "Les deux mots de pass sont différents";
                    SubscriptionAuthorized = false;
                }
                else
                {
                    ErrorMsg = "";
                    SubscriptionAuthorized = false;
                }
            }
        }

        private void HandleError(ParseException e)
        {
            Debug.WriteLine(e.Code + " : " + e.Message);

            switch (e.Code)
            {
                case ErrorCode.UsernameMissing:

                    break;

                case ErrorCode.PasswordMissing:
                    break;

                case ErrorCode.UsernameTaken:
                    ErrorMsg = "Le nom de compte \"" + LoginS + "\" est déjà pris";
                    break;

                case ErrorCode.EmailTaken:
                    break;

                case ErrorCode.EmailMissing:
                    break;

                case ErrorCode.EmailNotFound:
                    break;

                case ErrorCode.SessionMissing:
                    break;

                case ErrorCode.MustCreateUserThroughSignup:
                    break;

                case ErrorCode.AccountAlreadyLinked:
                    break;

                case ErrorCode.InvalidSessionToken:
                    break;

                case ErrorCode.ObjectNotFound:
                    ErrorMsg = "Nom de compte ou mot de passe incorrect";
                    break;

                case ErrorCode.InvalidEmailAddress:
                    ErrorMsg = "L'email n'est pas valide";
                    break;

                default:
                    ErrorMsg = "Une erreur est survenue";
                    break;
            }
        }

        #region Properties

        public AnotherCommandImplementation ChangeContent { get; }
        public AnotherCommandImplementation ConnectionCommand { get; }
        public AnotherCommandImplementation SubscriptionCommand { get; }
        public AnotherCommandImplementation BrowsePathCommand { get; }
        public AnotherCommandImplementation RememberCommand { get; }

        private Visibility topMenuVisible = Visibility.Visible;

        public Visibility TopMenuVisible
        {
            get { return topMenuVisible; }
            set
            {
                this.MutateVerbose(ref topMenuVisible, value, RaisePropertyChanged());
            }
        }

        private bool extractUnderterminate = true;

        public bool ExtractUnderterminate
        {
            get { return extractUnderterminate; }
            set
            {
                this.MutateVerbose(ref extractUnderterminate, value, RaisePropertyChanged());
            }
        }

        private int percentExtract = 0;

        public int PercentExtract
        {
            get { return percentExtract; }
            set
            {
                this.MutateVerbose(ref percentExtract, value, RaisePropertyChanged());
            }
        }

        private int amountDataExtract = 0;

        public int AmountDataExtract
        {
            get { return amountDataExtract; }
            set
            {
                this.MutateVerbose(ref amountDataExtract, value, RaisePropertyChanged());
            }
        }

        private int maxAmountDataExtract = 0;

        public int MaxAmountDataExtract
        {
            get { return maxAmountDataExtract; }
            set
            {
                this.MutateVerbose(ref maxAmountDataExtract, value, RaisePropertyChanged());
            }
        }

        private string title = "Bienvenue sur Ware !";

        public string Title
        {
            get { return title; }
            set
            {
                this.MutateVerbose(ref title, value, RaisePropertyChanged());
            }
        }

        private string libelleExtract = "";

        public string LibelleExtract
        {
            get { return libelleExtract; }
            set
            {
                this.MutateVerbose(ref libelleExtract, value, RaisePropertyChanged());
            }
        }

        private Snackbar loginSnackbar;

        public Snackbar LoginSnackbar
        {
            get { return loginSnackbar; }
            set
            {
                this.MutateVerbose(ref loginSnackbar, value, RaisePropertyChanged());
            }
        }

        private bool connectionAuthorized = false;

        public bool ConnectionAuthorized
        {
            get { return connectionAuthorized; }
            set
            {
                this.MutateVerbose(ref connectionAuthorized, value, RaisePropertyChanged());
            }
        }

        private bool subscriptionAuthorized = false;

        public bool SubscriptionAuthorized
        {
            get { return subscriptionAuthorized; }
            set
            {
                this.MutateVerbose(ref subscriptionAuthorized, value, RaisePropertyChanged());
            }
        }

        private bool rememberMe;

        public bool RememberMe
        {
            get { return rememberMe; }
            set
            {
                this.MutateVerbose(ref rememberMe, value, RaisePropertyChanged());
            }
        }

        private string path = "";

        public string Path
        {
            get { return path; }
            set
            {
                if (((string)value).Contains("Dofus.exe") && ((string)value).Contains(@"\retro\resources\app\retroclient\"))
                {
                    this.MutateVerbose(ref path, value, RaisePropertyChanged());
                }
                else
                {
                    this.MutateVerbose(ref path, "", RaisePropertyChanged());
                }
            }
        }

        private string login = "";

        public string Login
        {
            get { return login; }
            set
            {
                this.MutateVerbose(ref login, value, RaisePropertyChanged());
            }
        }

        private string password = "";

        public string Password
        {
            get { return password; }
            set
            {
                this.MutateVerbose(ref password, value, RaisePropertyChanged());
            }
        }

        private string loginS = "";

        public string LoginS
        {
            get { return loginS; }
            set
            {
                this.MutateVerbose(ref loginS, value, RaisePropertyChanged());
            }
        }

        private string passwordS = "";

        public string PasswordS
        {
            get { return passwordS; }
            set
            {
                this.MutateVerbose(ref passwordS, value, RaisePropertyChanged());
            }
        }

        private string confirmPasswordS = "";

        public string ConfirmPasswordS
        {
            get { return confirmPasswordS; }
            set
            {
                this.MutateVerbose(ref confirmPasswordS, value, RaisePropertyChanged());
            }
        }

        private string mailS = "";

        public string MailS
        {
            get { return mailS; }
            set
            {
                this.MutateVerbose(ref mailS, value, RaisePropertyChanged());
            }
        }

        private bool cgu = false;

        public bool Cgu
        {
            get { return cgu; }
            set
            {
                this.MutateVerbose(ref cgu, value, RaisePropertyChanged());
            }
        }

        private UserControl _content;

        public UserControl Content
        {
            get { return _content; }
            set
            {
                this.MutateVerbose(ref _content, value, RaisePropertyChanged());
            }
        }

        private string errorMsg = "";

        public string ErrorMsg
        {
            get { return errorMsg; }
            set
            {
                this.MutateVerbose(ref errorMsg, value, RaisePropertyChanged());
            }
        }

        #endregion Properties

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion INotifyPropertyChanged Members

        #region Methods

        //private async void ExecuteRunDialog(object o)
        //{
        //    //let's set up a little MVVM, cos that's what the cool kids are doing:
        //    var view = new LoadingDataDialog
        //    {
        //        DataContext = this
        //    };

        //    //show the dialog
        //    var result = await DialogHost.Show(view, "RootDialog", ClosingEventHandler);

        //    //check the result...
        //    Console.WriteLine("Dialog was closed, the CommandParameter used to close it was: " + (result ?? "NULL"));
        //}

        //private void ClosingEventHandler(object sender, DialogClosingEventArgs eventArgs)
        //{
        //    Console.WriteLine("You can intercept the closing event, and cancel here.");
        //}

        private Action<PropertyChangedEventArgs> RaisePropertyChanged()
        {
            return args => PropertyChanged?.Invoke(this, args);
        }

        #endregion Methods
    }
}