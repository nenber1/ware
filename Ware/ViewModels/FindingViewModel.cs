﻿using System.ComponentModel;

namespace Ware.ViewModels
{
    public class FindingViewModel : INotifyPropertyChanged
    {
        #region Construction

        /// <summary>
        /// Constructs the default instance of a SongViewModel
        /// </summary>
        public FindingViewModel()
        {
            Text = "Chargement des données nécessaires...";
        }

        public FindingViewModel(string text)
        {
            Text = text;
        }

        #endregion Construction

        #region Members

        private string text;

        #endregion Members

        #region Properties

        public string Text
        {
            get { return text; }
            set
            {
                if (text != value)
                {
                    text = value;
                    RaisePropertyChanged("Text");
                }
            }
        }

        #endregion Properties

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion INotifyPropertyChanged Members

        #region Methods

        private void RaisePropertyChanged(string propertyName)
        {
            // take a copy to prevent thread issues
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Methods
    }
}