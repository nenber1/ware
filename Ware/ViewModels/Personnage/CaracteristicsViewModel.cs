﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ware.Models.Character;

namespace Ware.ViewModels.Personnage
{
    public class CaracteristicsViewModel : INotifyPropertyChanged
    {
        public CaracteristicsViewModel(DashboardViewModel dvm)
        {
            DashboardViewModel = dvm;
            Caracteristics = new ObservableCollection<Caracteristic>();
        }
        #region Members

        private int caracteristicsPointsAmount = 0;
        private DashboardViewModel DashboardViewModel;
        private ObservableCollection<Caracteristic> caracteristics;
        #endregion Members
        #region Properties
        public int CaracteristicsPointsAmount
        {
            get { return caracteristicsPointsAmount; }
            set
            {
                if (caracteristicsPointsAmount != value)
                {
                    caracteristicsPointsAmount = value;
                    RaisePropertyChanged("CaracteristicsPointsAmount");
                }
            }
        }
        public ObservableCollection<Caracteristic> Caracteristics { get { return caracteristics; } set { caracteristics = value; RaisePropertyChanged("Caracteristics"); } }
        #endregion
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion INotifyPropertyChanged Members

        #region Methods
        private void RaisePropertyChanged(string propertyName)
        {
            // take a copy to prevent thread issues
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Methods
    }
}
