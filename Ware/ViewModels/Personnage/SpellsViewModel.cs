﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ware.Models.LocalDatabase;

namespace Ware.ViewModels.Personnage
{
    public class SpellsViewModel : INotifyPropertyChanged
    {
        public SpellsViewModel(DashboardViewModel dvm)
        {
            DashboardViewModel = dvm;
            Spells = new ObservableCollection<Spell>();
        }
        #region Members

        private int spellPointsAmount = 0;
        private DashboardViewModel DashboardViewModel;
        private ObservableCollection<Spell> spells;
        #endregion Members
        #region Properties
        public int SpellPointsAmount
        {
            get { return spellPointsAmount; }
            set
            {
                if (spellPointsAmount != value)
                {
                    spellPointsAmount = value;
                    RaisePropertyChanged("SpellPointsAmount");
                }
            }
        }
        public ObservableCollection<Spell> Spells { get { return spells; } set { spells = value; RaisePropertyChanged("Spells"); } }
        #endregion
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion INotifyPropertyChanged Members

        #region Methods
        private void RaisePropertyChanged(string propertyName)
        {
            // take a copy to prevent thread issues
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Methods
    }
}
