﻿using Firebase.Database;
using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Ware.Enums;
using Ware.Models.Character;
using Ware.Network.MITM;
using Ware.View;

namespace Ware.ViewModels.MITM
{
    public class InstanceContainerViewModelMITM : INotifyPropertyChanged
    {
        private ObservableCollection<INotifyPropertyChanged> items;

        public ObservableCollection<INotifyPropertyChanged> Items
        {
            get { return items; }
            set
            {
                RaisePropertyChanged("Items");
                items = value;
            }
        }

        public AnotherCommandImplementation test { get; }
        public AnotherCommandImplementation AddCommand { get; }

        public AnotherCommandImplementation RemoveSelectedItemCommand { get; }

        public BaseInstance SelectedItem { get; set; }

        private MainViewModel MainViewModel;
        public InstanceGroup InstanceGroup;

        public InstanceContainerViewModelMITM(MainViewModel mainViewModel)
        {
            Items = new ObservableCollection<INotifyPropertyChanged>();
            Items.CollectionChanged += Items_CollectionChanged;

            MainViewModel = mainViewModel;

            //Ceation de fausses instances
            InstanceGroup = new InstanceGroup("farm");

            Instance instance = new Instance("Nouveau Bot", Enums.WareType.MITM);
            instance.InstanceGroup = null;
            Items.Add(instance);

            AddCommand = new AnotherCommandImplementation(
            listInstance =>
            {
                instance = new Instance("Nouveau Bot", Enums.WareType.MITM);
                Items.Add(instance);
                ((ListBox)listInstance).Items.MoveCurrentToLast();
            });

            RemoveSelectedItemCommand = new AnotherCommandImplementation(item =>
            {
                if (item.GetType() == typeof(Instance))
                {
                    ((Instance)item).Dispose();
                    Items.Remove((Instance)item);
                }

                GC.Collect();
            });
            test = new AnotherCommandImplementation(inst =>
            {
                //287 => 302
                Instance i = inst as Instance;

                MovementResult a = i.MapManager.MoveToCell(i.MapManager.GetCellFromId(309), i.MapManager.GetCellFromId(324), i.MapManager.celdas_ocupadas());
                i.MapManager.ChangeMap(MapDirection.LEFT);

            });

            
        }

        private void Items_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Remove || e.Action == NotifyCollectionChangedAction.Replace)
            {
                if (Items.Count == 0)
                {
                    Listener.StopListening();
                    MainViewModel.ChangeMenuItemContent(0, new StartPage());
                }
            }
        }

        private void ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //This will get called when the property of an object inside the collection changes
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion INotifyPropertyChanged Members

        #region Methods

        private void RaisePropertyChanged(string propertyName)
        {
            // take a copy to prevent thread issues
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Methods
    }
}