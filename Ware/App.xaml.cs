﻿using System.Windows;
using Ware.View;

namespace Ware
{
    /// <summary>
    /// Logique d'interaction pour App.xaml
    /// </summary>
    public partial class App : Application
    {
        private async void App_Startup(object sender, StartupEventArgs e)
        {
            MainWindow l = new View.MainWindow();
            l.Show();
        }
    }
}