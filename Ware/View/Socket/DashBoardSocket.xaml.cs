﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Threading;
using Ware.Common.Enums;
using Ware.Core.MDI;
using Ware.Network.MITM;
using Ware.View.Interfaces;
using Ware.ViewModels;

namespace Ware.View.Socket
{
    /// <summary>
    /// Logique d'interaction pour Character.xaml
    /// </summary>
    public partial class DashBoardSocket : UserControl, IDashBoard
    {
        public DashboardViewModel _viewModel;
        public delegate void OnClose(object sender, EventArgs e);

        #region Constructor - Destructor
        public DashBoardSocket()
        {
            InitializeComponent();
            this.DataContext = new DashboardViewModel();
        }
        #endregion
    }
}
