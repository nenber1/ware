﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Ware.View
{
    /// <summary>
    /// Logique d'interaction pour PaletteSelector.xaml
    /// </summary>
    public partial class PaletteSelector : UserControl
    {
        public PaletteSelector()
        {
            InitializeComponent();
        }

        private void ItemsControl_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            MessageBox.Show(((ItemsControl)sender).Items.ToString());
        }
    }
}
