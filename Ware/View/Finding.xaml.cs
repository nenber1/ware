﻿using System.Windows.Controls;
using Ware.ViewModels;

namespace Ware.View
{
    /// <summary>
    /// Logique d'interaction pour Finding.xaml
    /// </summary>
    public partial class Finding : UserControl
    {
        FindingViewModel _viewModel;

        public Finding()
        {
            InitializeComponent();

            _viewModel = (FindingViewModel)base.DataContext;
        }
    }
}
