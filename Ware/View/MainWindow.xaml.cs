﻿using MahApps.Metro.Controls;
using MaterialDesignThemes.Wpf;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Ware.Common.Utils;
using Ware.Services;
using Ware.ViewModels;

namespace Ware.View
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private MainViewModel _viewModel;

        public MainWindow()
        {
            InitializeComponent();
            _viewModel = (MainViewModel)base.DataContext;

            var dictionary = new ResourceDictionary();
            dictionary.Source = new Uri("pack://application:,,,/MaterialDesignThemes.MahApps;component/Themes/MaterialDesignTheme.MahApps.Dialogs.xaml");

            Task.Factory.StartNew(() =>
            {
                Thread.Sleep(1000);
            }).ContinueWith(t =>
            {
                //note you can use the message queue from any thread, but just for the demo here we
                //need to get the message queue from the snackbar, so need to be on the dispatcher
                MainSnackbar.MessageQueue.Enqueue("Bienvenue nenber !");
            }, TaskScheduler.FromCurrentSynchronizationContext());

            //DataContext = new MainWindowViewModel(MainSnackbar.MessageQueue);

            Functions.MainSnackBar = MainSnackbar;

            _viewModel.PropertyChanged += _viewModel_PropertyChanged;
  
        }

        private void _viewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SnackMessage")
            {
                SendSnackBar(_viewModel.SnackMessage);
            }
            else if (e.PropertyName == "")
            {
            }
        }

        private void MetroWindow_Closed(object sender, EventArgs e)
        {
            //Environment.Exit(0);
        }

        public void SendSnackBar(string msg)
        {
            Dispatcher.Invoke(new Action<string>(sendSnackBar), msg);
        }

        private void sendSnackBar(string message)
        {
            //use the message queue to send a message.
            var messageQueue = MainSnackbar.MessageQueue;

            //the message queue can be called from any thread
            Task.Factory.StartNew(() => messageQueue.Enqueue(message));
        }

        private void MenuToggleButton_Click(object sender, RoutedEventArgs e)
        {
            if (DrawerHost.IsLeftDrawerOpen == true)
            {
            }
        }

        private void ToggleButton_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
        }

        private void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}