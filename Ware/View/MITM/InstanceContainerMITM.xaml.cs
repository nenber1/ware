﻿using System.Windows;
using System.Windows.Controls;
using Ware.Network.MITM;
using Ware.ViewModels.MITM;

namespace Ware.View.MITM
{
    /// <summary>
    /// Logique d'interaction pour InstanceContainer.xaml
    /// </summary>
    public partial class InstanceContainerMITM : UserControl
    {
        public InstanceContainerMITM(InstanceContainerViewModelMITM instanceContainerViewModelMITM)
        {
            InitializeComponent();
            this.DataContext = instanceContainerViewModelMITM;
            Listener.StartListening();
        }

        private void listInstance_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (((ListBox)sender).SelectedIndex != -1)
            {
                toolBar.Visibility = Visibility.Visible;
                Close.Visibility = Visibility.Visible;
            }
            else
            {
                toolBar.Visibility = Visibility.Hidden;
                Close.Visibility = Visibility.Hidden;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
        }

        private void Expander_GotFocus(object sender, RoutedEventArgs e)
        {
        }
    }
}