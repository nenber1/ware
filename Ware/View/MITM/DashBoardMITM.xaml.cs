﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using Ware.Core.MDI;
using Ware.View.Interfaces;
using Ware.ViewModels;

namespace Ware.View.MITM
{
    /// <summary>
    /// Logique d'interaction pour Character.xaml
    /// </summary>
    public partial class DashBoardMITM : UserControl, IDisposable, IDashBoard
    {
        public DashboardViewModel _viewModel { get; set; }
        public Processus Process { get; private set; }

        public delegate void onResizingProcess(object sender, EventArgs e);

        public event onResizingProcess onResizingProcessEventHandler;

        public delegate void OnClose(object sender, EventArgs e);

        public event OnClose onCloseEventHandler;

        public delegate void OnProcessReady(object sender, Processus e);

        public event OnProcessReady onProcessReady;

        private bool panelVisible;

        #region Constructor - Destructor

        public DashBoardMITM(DashboardViewModel dashboardViewModel)
        {
            InitializeComponent();
            Process = new Processus();
            this._viewModel = dashboardViewModel;
            this.DataContext = dashboardViewModel;
            this.Loaded += DashBoard_Loaded;
            panelVisible = true;
        }

        private async void DashBoard_Loaded(object sender, RoutedEventArgs e)
        {
            await Process.Start(DofusHost, this);
            if (onProcessReady != null)
            {
                onProcessReady(this, Process);
            }
            this.Loaded -= DashBoard_Loaded;
        }

        ~DashBoardMITM()
        {
            try
            {
                Process.Proc.Close();
                Process.Proc.Dispose();
                this.Dispose();
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.ToString());
            }
        }

        #endregion Constructor - Destructor

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            //EndSession();
        }

        private void DofusHost_Resize(object sender, EventArgs e)
        {
            OnRaiseResizingProcess(sender, e);
        }

        protected virtual void OnRaiseResizingProcess(object sender, EventArgs e)
        {
            // Event will be null if there are no subscribers
            if (onResizingProcessEventHandler != null)
            {
                onResizingProcessEventHandler(sender, e);
            }
        }

        protected virtual void OnRaiseClose(object sender, EventArgs e)
        {
            // Event will be null if there are no subscribers
            if (onCloseEventHandler != null)
            {
                onCloseEventHandler(sender, e);
            }
        }

        public void SwitchPanelVisibility()
        {
            if (panelVisible == true)
            {
                WindowsFormHost.Visibility = Visibility.Hidden;
            }
            else
            {
                DispatcherTimer dispatcherTimer = new DispatcherTimer();
                dispatcherTimer.Tick += DispatcherTimer_Tick;
                dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 250);
                dispatcherTimer.Start();
            }
            panelVisible = !panelVisible;
        }

        private void DispatcherTimer_Tick(object sender, EventArgs e)
        {
            WindowsFormHost.Visibility = Visibility.Visible;
            ((DispatcherTimer)sender).Stop();
        }

        public void Dispose()
        {
            try
            {
                Process.Proc.Kill();
                OnRaiseClose(this, new EventArgs());
            }
            catch (Exception)
            {
            }
        }

        private void TextBox_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {

        }
    }
}