﻿using Microsoft.EntityFrameworkCore;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Xml;
using Ware.Enums;
using Ware.Models.Character;
using Ware.Models.LocalDatabase;
using Ware.View.Interfaces;
//using Ware.View.Socket;
using Ware.View.MITM;
using Ware.View.Socket;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace Ware.View
{
    /// <summary>
    /// Logique d'interaction pour StartPage.xaml
    /// </summary>
    public partial class StartPage : UserControl
    {
        private DashBoardMITM character;
        //private DashBoardSocket dashBoardSocket;
        public delegate void onSessionBeginEventHandler(object sender, WareType wareType);
        public event onSessionBeginEventHandler OnSessionBegin;

        public StartPage()
        {
            InitializeComponent();
        }


        private void RaiseSessionBegin(WareType wareType)
        {
            if (OnSessionBegin != null)
            {
                OnSessionBegin(this,wareType);
            }
        }

        private void ButtonSocket_Click(object sender, RoutedEventArgs e)
        {
            RaiseSessionBegin(WareType.Socket);
        }

        private void ButtonMITM_Click(object sender, RoutedEventArgs e)
        {
            RaiseSessionBegin(WareType.MITM);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {


            //using (StreamReader file = File.OpenText(@"C:\Users\nenbe\AppData\Local\Ware\items.json"))
            //using (JsonTextReader reader = new JsonTextReader(file))
            //{
            //    JArray a2 = (JArray)JToken.ReadFrom(reader);
            //    using (var db = new ConfigContext())
            //    {

            //        db.Database.EnsureCreated();
            //        foreach (var item in a2)
            //        {
            //            Item i = item.ToObject<Item>();
            //            Debug.WriteLine(i.Name);
            //            db.Items.Add(i);
            //        }
            //        db.SaveChanges();
            //    }

            //}


            #region a
            // string s = @"[ {d: 'Téléporte à une distance de #1 cases maximum.',c: -1,o: ''},
            // { d: 'Fait reculer de #1 case(s)',c: -1,o: ''},
            // { d: 'Fait avancer de #1 case(s)',c: 0,o: ''},
            // { d: 'Divorcer le couple',c: 0,o: ''},
            // { d: 'Echange les places de 2 joueurs',c: 0,o: ''},
            // { d: 'Esquive #1% des coups en reculant de #2 case(s)',c: 0,o: ''},
            // { d: 'Permet d\'utiliser l\'émoticon #3',c: 0,o: ''},
            // { d: 'Change le temps de jeu d\'un joueur',c: 0,o: ''},
            // { d: 'Porter un joueur',c: 0,o: ''},
            // { d: 'Jeter un joueur',c: 0,o: ''},
            // { d: 'Vole #1{~1~2 à }#2 PM',c: 0,o: '',j: true},
            // { d: 'Ajoute +#1{~1~2 à }#2 PM',c: 0,o: '+',j: true},
            // { d: '#3% dommages subis x#1, sinon soigné de x#2',c: 0,o: ''},
            // { d: 'PDV rendus : #1{~1~2 à }#2',c: 0,o: '',j: true},
            // { d: 'Vole #1{~1~2 à }#2 PDV (fixe)',c: 0,o: '',j: true},
            // { d: 'Vole #1{~1~2 à }#2 PA',c: 0,o: '',j: true},
            // { d: 'Dommages : #1{~1~2 à }#2% de la vie de l\'attaquant (eau)',c: 0,o: '',j: true,e: 'W'},
            // { d: 'Dommages : #1{~1~2 à }#2% de la vie de l\'attaquant (terre)',c: 0,o: '',j: true,e: 'E'},
            // { d: 'Dommages : #1{~1~2 à }#2% de la vie de l\'attaquant (air)',c: 0,o: '',j: true,e: 'A'},
            // { d: 'Dommages : #1{~1~2 à }#2% de la vie de l\'attaquant (feu)',c: 0,o: '',j: true,e: 'F'},
            // { d: 'Dommages : #1{~1~2 à }#2% de la vie de l\'attaquant (neutre)',c: 0,o: '',j: true,e: 'N'},
            // { d: 'Donne #1{~1~2 à }#2 % de sa vie',c: 0,o: '',j: true},
            // { d: 'Vole #1{~1~2 à }#2 PDV (eau)',c: 0,o: '',t: true,j: true,e: 'W'},
            // { d: 'Vole #1{~1~2 à }#2 PDV (terre)',c: 0,o: '',t: true,j: true,e: 'E'},
            // { d: 'Vole #1{~1~2 à }#2 PDV (air)',c: 0,o: '',t: true,j: true,e: 'A'},
            // { d: 'Vole #1{~1~2 à }#2 PDV (feu)',c: 0,o: '',t: true,j: true,e: 'F'},
            // { d: 'Vole #1{~1~2 à }#2 PDV (neutre)',c: 0,o: '',t: true,j: true,e: 'N'},
            // { d: 'Dommages : #1{~1~2 à }#2 (eau)',c: 0,o: '',t: true,j: true,e: 'W'},
            // { d: 'Dommages : #1{~1~2 à }#2 (terre)',c: 0,o: '',t: true,j: true,e: 'E'},
            // { d: 'Dommages : #1{~1~2 à }#2 (air)',c: 0,o: '',t: true,j: true,e: 'A'},
            // { d: 'Dommages : #1{~1~2 à }#2 (feu)',c: 0,o: '',t: true,j: true,e: 'F'} ,
            //{ d: 'Dommages : #1{~1~2 à }#2 (neutre)',c: 0,o: '',t: true,j: true,e: 'N'} ,
            //{ d: 'PA perdus à la cible : #1{~1~2 à }#2',c: 1,o: '/',j: true} ,
            //{ d: 'Dommages réduits de  #1{~1~2 à }#2',c: 16,o: '+',j: true} ,
            //{ d: 'Renvoie un sort de niveau #2 maximum',c: 30,o: ''} ,
            //{ d: 'Dommages retournés : #1{~1~2 à }#2',c: 31,o: '',j: true} ,
            //{ d: 'PDV rendus : #1{~1~2 à }#2',c: 0,o: '',j: true} ,
            //{ d: 'Dommages pour le lanceur : #1{~1~2 à }#2',c: 0,o: '-',j: true} ,
            //{ d: '+#1{~1~2 à }#2 en vie',c: 0,o: '+',j: true} ,
            //{ d: '+#1{~1~2 à }#2 PA',c: 1,o: '+',j: true} ,
            //{ d: '+#1{~1~2 à }#2 de dommages',c: 16,o: '+',j: true} ,
            //{ d: 'Double les dommages ou rend  #1{~1~2 à }#2 PDV',c: 0,o: '',j: true} ,
            //{ d: 'Multiplie les dommages par #1',c: 17,o: '+'} ,
            //{ d: '+#1{~1~2 à }#2 aux coups critiques',c: 18,o: '+',j: true} ,
            //{ d: '-#1{~1~2 à -}#2 à la portée',c: 19,o: '-',j: true} ,
            //{ d: '+#1{~1~2 à }#2 à la portée',c: 19,o: '+',j: true} ,
            //{ d: '+#1{~1~2 à }#2 en force',c: 10,o: '+',j: true} ,
            //{ d: '+#1{~1~2 à }#2 en agilité',c: 14,o: '+',j: true} ,
            //{ d: 'Ajoute +#1{~1~2 à }#2 PA',c: 0,o: '+',j: true} ,
            //{ d: '+#1{~1~2 à }#2 de dommages',c: 16,o: '+',j: true} ,
            //{ d: '+#1{~1~2 à }#2 aux échecs critiques',c: 39,o: '-',j: true} ,
            //{ d: '+#1{~1~2 à }#2 à la chance',c: 13,o: '+',j: true} ,
            //{ d: '+#1{~1~2 à }#2 en sagesse',c: 12,o: '+',j: true} ,
            //{ d: '+#1{~1~2 à }#2 en vitalité',c: 11,o: '+',j: true} ,
            //{ d: '+#1{~1~2 à }#2 en intelligence',c: 15,o: '+',j: true} ,
            //{ d: 'PM perdus : #1{~1~2 à }#2',c: 23,o: '-',j: true} ,
            //{ d: '+#1{~1~2 à }#2 PM',c: 23,o: '+',j: true} ,
            //{ d: 'Vole  #1{~1~2 à }#2 d\'or',c: -1,o: '',j: true} ,
            //{ d: '#1 PA utilisés font perdre #2 PDV',c: 0,o: '-'} ,
            //{ d: 'Enlève les envoûtements',c: -1,o: ''} ,
            //{ d: 'PA perdus pour le lanceur : #1{~1~2 à }#2',c: 1,o: '-',j: true} ,
            //{ d: 'PM perdus pour le lanceur : #1{~1~2 à }#2',c: 23,o: '-',j: true} ,
            //{ d: 'Portée du lanceur réduite de : #1{~1~2 à }#2',c: 19,o: '-',j: true} ,
            //{ d: 'Portée du lanceur augmentée de : #1{~1~2 à }#2',c: 19,o: '+',j: true} ,
            //{ d: 'Dommages physiques du lanceur augmentés de : #1{~1~2 à }#2',c: 16,o: '+',j: true} ,
            //{ d: 'Augmente les dommages de #1{~1~2 à }#2%',c: 17,o: '+',j: true} ,
            //{ d: 'Rend #1{~1~2 à }#2 points d\'énergie',c: 29,o: '+',j: true} ,
            //{ d: 'Fait passer le tour suivant',c: -1,o: ''} ,
            //{ d: 'Tue la cible',c: 0,o: ''} ,
            //{ d: '+#1{~1~2 à }#2 aux dommages physiques',c: 16,o: '+',j: true} ,
            //{ d: 'PDV rendus : #1{~1~2 à }#2',c: 0,o: '',j: true} ,
            //{ d: 'Dommages : #1{~1~2 à }#2 (non boostés)',c: 0,o: '-',j: true} ,
            //{ d: '-#1{~1~2 à }#2 aux dommages',c: 16,o: '-',j: true} ,
            //{ d: 'Change les paroles',c: 38,o: ''} ,
            //{ d: 'Ressuscite un allié',c: 0,o: ''} ,
            //{ d: 'Quelqu\'un vous suit !',c: 0,o: ''} ,
            //{ d: 'Change l\'apparence',c: 38,o: ''} ,
            //{ d: 'Rend le personnage invisible',c: 24,o: '+'} ,
            //{ d: '-#1{~1~2 à -}#2 en chance',c: 13,o: '-',j: true} ,
            //{ d: '-#1{~1~2 à -}#2 en vitalité',c: 11,o: '-',j: true} ,
            //{ d: '-#1{~1~2 à -}#2 en agilité',c: 14,o: '-',j: true} ,
            //{ d: '-#1{~1~2 à -}#2 en intelligence',c: 15,o: '-',j: true} ,
            //{ d: '-#1{~1~2 à -}#2 en sagesse',c: 12,o: '-',j: true} ,
            //{ d: '-#1{~1~2 à -}#2 en force',c: 10,o: '-',j: true} ,
            //{ d: 'Augmente le poids portable de #1{~1~2 à }#2 pods',c: 0,o: '+',j: true} ,
            //{ d: 'Réduit le poids portable de #1{~1~2 à }#2 pods',c: 0,o: '-',j: true} ,
            //{ d: '+#1{~1~2 à }#2 % de chance d\'esquiver les pertes de PA',c: 27,o: '+',j: true} ,
            //{ d: '+#1{~1~2 à }#2 % de chance d\'esquiver les pertes de PM',c: 28,o: '+',j: true} ,
            //{ d: '-#1{~1~2 à }#2 % de chance d\'esquiver les pertes de PA',c: 27,o: '-',j: true} ,
            //{ d: '-#1{~1~2 à }#2 % de chance d\'esquiver les pertes de PM',c: 28,o: '-',j: true} ,
            //{ d: 'Dommages réduits de #1%',c: -1,o: '-'} ,
            //{ d: 'Augmente les dommages (#1) de #2%',c: 16,o: '+'} ,
            //{ d: 'PA retournés : #1{~1~2 à }#2',c: 0,o: '',j: true} ,
            //{ d: '-#1{~1~2 à -}#2 PA',c: 1,o: '-',j: true} ,
            //{ d: '-#1{~1~2 à -}#2 PM',c: 23,o: '-',j: true} ,
            //{ d: '-#1{~1~2 à }#2 aux coups critiques',c: 18,o: '-',j: true} ,
            //{ d: 'Réduction magique diminué de #1{~1~2 à }#2',c: 20,o: '-',j: true} ,
            //{ d: 'Réduction physique diminué de #1{~1~2 à }#2',c: 21,o: '-',j: true} ,
            //{ d: '+#1{~1~2 à }#2 en initiative',c: 44,o: '+',j: true} ,
            //{ d: '-#1{~1~2 à }#2 en initiative',c: 44,o: '-',j: true} ,
            //{ d: '+#1{~1~2 à }#2 en prospection',c: 48,o: '+',j: true} ,
            //{ d: '-#1{~1~2 à }#2 en prospection',c: 48,o: '-',j: true} ,
            //{ d: '+#1{~1~2 à }#2 de soins',c: 49,o: '+',j: true} ,
            //{ d: '-#1{~1~2 à }#2 de soins',c: 49,o: '-',j: true} ,
            //{ d: 'Crée un double du lanceur de sort',c: -1,o: ''} ,
            //{ d: 'Invoque une créature',c: -1,o: ''} ,
            //{ d: '+#1{~1~2 à }#2 créatures invocables',c: 26,o: '+',j: true} ,
            //{ d: 'Réduction magique de #1{~1~2 à }#2',c: 20,o: '/',j: true} ,
            //{ d: 'Réduction physique de #1{~1~2 à }#2',c: 21,o: '/',j: true} ,
            //{ d: 'Invoque une créature statique',c: 0,o: ''} ,
            //{ d: 'Diminue les dommages de #1{~1~2 à }#2%',c: 17,o: '-',j: true} ,
            //{ d: 'Changer l\'alignement',c: 0,o: '/'} ,
            //{ d: 'Gagner #1{~1~2 à }#2 kamas',c: 0,o: '+',j: true} ,
            //{ d: 'Transforme en #1',c: 0,o: ''} ,
            //{ d: 'Pose un objet au sol',c: -1,o: ''} ,
            //{ d: 'Dévoile tous les objets invisibles',c: -1,o: ''} ,
            //{ d: 'Ressuscite la cible',c: 0,o: ''} ,
            //{ d: '#1{~1~2 à }#2 % de résistance à la terre',c: 33,o: '+',j: true} ,
            //{ d: '#1{~1~2 à }#2 % de résistance à l\'eau',c: 35,o: '+',j: true} ,
            //{ d: '#1{~1~2 à }#2 % de résistance à l\'air',c: 36,o: '+',j: true} ,
            //{ d: '#1{~1~2 à }#2 % de résistance au feu',c: 34,o: '+',j: true} ,
            //{ d: '#1{~1~2 à }#2 % de résistance neutre',c: 37,o: '+',j: true} ,
            //{ d: '#1{~1~2 à }#2 % de faiblesse face à la terre',c: 33,o: '-',j: true} ,
            //{ d: '#1{~1~2 à }#2 % de faiblesse face à l\'eau ',c: 35,o: '-',j: true} ,
            //{ d: '#1{~1~2 à }#2 % de faiblesse face à l\'air',c: 36,o: '-',j: true} ,
            //{ d: '#1{~1~2 à }#2 % de faiblesse face au feu',c: 34,o: '-',j: true} ,
            //{ d: '#1{~1~2 à }#2 % de faiblesse neutre',c: 37,o: '-',j: true} ,
            //{ d: 'Renvoie #1 dommages',c: 50,o: '+'} ,
            //{ d: 'Qu\'y a-t-il là dedans ?',c: 0,o: ''} ,
            //{ d: 'Qu\'y a-t-il là dedans ?',c: 0,o: ''} ,
            //{ d: '+#1{~1~2 à }#2 de dommages aux pièges',c: 70,o: '+',j: true} ,
            //{ d: '+#1{~1~2 à }#2% de dommages aux pièges',c: 69,o: '+',j: true} ,
            //{ d: 'Récupère une monture !',c: 0,o: '+'} ,
            //{ d: '+#1 en énergie perdue',c: 51,o: '/'} ,
            //{ d: '+#1{~1~2 à }#2 de résistance à la terre',c: 54,o: '+',j: true} ,
            //{ d: '+#1{~1~2 à }#2 de résistance à l\'eau',c: 56,o: '+',j: true} ,
            //{ d: '+#1{~1~2 à }#2 de résistance à l\'air',c: 57,o: '+',j: true} ,
            //{ d: '+#1{~1~2 à }#2 de résistance au feu',c: 55,o: '+',j: true} ,
            //{ d: '+#1{~1~2 à }#2 de résistance neutre',c: 58,o: '+',j: true} ,
            //{ d: '-#1{~1~2 à }#2 de résistance à la terre',c: 54,o: '-',j: true} ,
            //{ d: '-#1{~1~2 à }#2 de résistance à l\'eau',c: 56,o: '-',j: true} ,
            //{ d: '-#1{~1~2 à }#2 de résistance à l\'air',c: 57,o: '-',j: true} ,
            //{ d: '-#1{~1~2 à }#2 de résistance au feu',c: 55,o: '-',j: true} ,
            //{ d: '-#1{~1~2 à }#2 de résistance neutre',c: 58,o: '-',j: true} ,
            //{ d: '#1{~1~2 à }#2 % de res. terre face aux combattants ',c: 59,o: '+',j: true} ,
            //{ d: '#1{~1~2 à }#2 % de res. eau face aux combattants',c: 61,o: '+',j: true} ,
            //{ d: '#1{~1~2 à }#2 % de res. air face aux combattants',c: 62,o: '+',j: true} ,
            //{ d: '#1{~1~2 à }#2 % de res. feu face aux combattants',c: 60,o: '+',j: true} ,
            //{ d: '#1{~1~2 à }#2 % de res. neutre face aux combattants',c: 63,o: '+',j: true} ,
            //{ d: '#1{~1~2 à }#2 % de faiblesse terre face aux combattants',c: 59,o: '-',j: true} ,
            //{ d: '#1{~1~2 à }#2 % de faiblesse eau face aux combattants',c: 61,o: '-',j: true} ,
            //{ d: '#1{~1~2 à }#2 % de faiblesse air face aux combattants',c: 62,o: '-',j: true} ,
            //{ d: '#1{~1~2 à }#2 % de faiblesse feu face aux combattants',c: 60,o: '-',j: true} ,
            //{ d: '#1{~1~2 à }#2 % de faiblesse neutre face aux combattants',c: 63,o: '-',j: true} ,
            //{ d: '+#1{~1~2 à }#2 de res. terre face aux combattants ',c: 64,o: '+',j: true} ,
            //{ d: '+#1{~1~2 à }#2 de res. eau face aux combattants ',c: 66,o: '+',j: true} ,
            //{ d: '+#1{~1~2 à }#2 de res. air face aux combattants',c: 67,o: '+',j: true} ,
            //{ d: '+#1{~1~2 à }#2 de res. feu aux combattants',c: 65,o: '+',j: true} ,
            //{ d: '+#1{~1~2 à }#2 de res. neutre aux combattants',c: 68,o: '+',j: true} ,
            //{ d: 'Dommages réduits de #1{~1~2 à }#2',c: 16,o: '+',j: true} ,
            //{ d: '#1{~1~2 à -}#2 vol de chance',c: 13,o: '',j: true} ,
            //{ d: '#1{~1~2 à -}#2 vol de vitalité',c: 11,o: '',j: true} ,
            //{ d: '#1{~1~2 à -}#2 vol d\'agilité',c: 14,o: '',j: true} ,
            //{ d: '#1{~1~2 à -}#2 vol d\'intelligence',c: 15,o: '',j: true} ,
            //{ d: '#1{~1~2 à -}#2 vol de sagesse',c: 12,o: '',j: true} ,
            //{ d: '#1{~1~2 à -}#2 vol de force',c: 12,o: '',j: true} ,
            //{ d: 'Dommages : #1{~1~2 à }#2% de la vie manquante de l\'attaquant (eau)',c: 0,o: '',j: true} ,
            //{ d: 'Dommages : #1{~1~2 à }#2% de la vie manquante de l\'attaquant (terre)',c: 0,o: '',j: true} ,
            //{ d: 'Dommages : #1{~1~2 à }#2% de la vie manquante de l\'attaquant (air)',c: 0,o: '',j: true} ,
            //{ d: 'Dommages : #1{~1~2 à }#2% de la vie manquante de l\'attaquant (feu)',c: 0,o: '',j: true} ,
            //{ d: 'Dommages : #1{~1~2 à }#2% de la vie manquante de l\'attaquant (neutre)',c: 0,o: '',j: true} ,
            //{ d: 'Augmente la portée du sort #1 de #3',c: 0,o: '+'} ,
            //{ d: 'Rend la portée du sort #1 modifiable',c: 0,o: '+'} ,
            //{ d: '+#3 de dommages sur le sort #1',c: 0,o: '+'} ,
            //{ d: '+#3 de soins sur le sort #1',c: 0,o: '+'} ,
            //{ d: 'Réduit de #3 le coût en PA du sort #1',c: 0,o: '+'} ,
            //{ d: 'Réduit de #3 le délai de relance du sort #1',c: 0,o: '+'} ,
            //{ d: '+#3 aux CC sur le sort #1',c: 0,o: '+'} ,
            //{ d: 'Désactive le lancer en ligne du sort #1',c: 0,o: '+'} ,
            //{ d: 'Désactive la ligne de vue du sort #1',c: 0,o: '+'} ,
            //{ d: 'Augmente de #3 le nombre de lancer maximal par tour du sort #1',c: 0,o: '+'} ,
            //{ d: 'Augmente de #3 le nombre de lancer maximal par cible du sort #1',c: 0,o: '+'} ,
            //{ d: 'Fixe à #3 le délai de relance du sort #1',c: 0,o: '+'} ,
            //{ d: 'Augmente les dégâts de base du sort #1 de #3',c: 0,o: '+'} ,
            //{ d: 'Diminue la portée du sort #1 de #3',c: 0,o: '-'} ,
            //{ d: 'null',c: 0,o: '/'} ,
            //{ d: 'Vole #1{~1~2 à }#2 PO',c: 0,o: '',j: true} ,
            //{ d: 'Change une couleur',c: 38,o: ''} ,
            //{ d: 'Change l\'apparence',c: 0,o: ''} ,
            //{ d: 'Pose un piège de rang #2',c: -1,o: ''} ,
            //{ d: 'Pose un glyphe de rang #2',c: -1,o: ''} ,
            //{ d: 'Pose un glyphe de rang #2',c: -1,o: ''} ,
            //{ d: 'Tue et remplace par une invocation',c: 0,o: ''} ,
            //{ d: 'Pose un prisme',c: 0,o: ''} ,
            //{ d: 'Téléporte au point de sauvegarde',c: 0,o: ''} ,
            //{ d: 'null',c: 0,o: '+'} ,
            //{ d: 'Enregistre sa position',c: 0,o: ''} ,
            //{ d: 'Apprend le métier #3',c: 0,o: ''} ,
            //{ d: 'Apprend le sort #3',c: 0,o: ''} ,
            //{ d: '+#1{~1~2 à }#2 points d\' XP',c: 0,o: '+',j: true} ,
            //{ d: '+#1{~1~2 à }#2 en sagesse',c: 12,o: '+',j: true} ,
            //{ d: '+#1{~1~2 à }#2 en force',c: 10,o: '+',j: true} ,
            //{ d: '+#1{~1~2 à }#2 en chance',c: 13,o: '+',j: true} ,
            //{ d: '+#1{~1~2 à }#2 en agilité',c: 14,o: '+',j: true} ,
            //{ d: '+#1{~1~2 à }#2 en vitalité',c: 11,o: '+',j: true} ,
            //{ d: '+#1{~1~2 à }#2 en intelligence',c: 15,o: '+',j: true} ,
            //{ d: '+#1{~1~2 à }#2 points de caractéristique',c: 0,o: '+',j: true} ,
            //{ d: '+#1{~1~2 à }#2 points de sort',c: 0,o: '+',j: true} ,
            //{ d: '+ #1 d\'XP dans le métier #2',c: 0,o: '+'} ,
            //{ d: 'Fait oublier le métier de #3',c: 0,o: ''} ,
            //{ d: 'Fait oublier un niveau du sort #3',c: 0,o: ''} ,
            //{ d: 'Consulter #3',c: 0,o: ''} ,
            //{ d: 'Invoque : #3 (grade #1)',c: 0,o: ''} ,
            //{ d: 'Téléporte chez soi',c: 0,o: ''} ,
            //{ d: 'Invoque : #3',c: -1,o: '/'} ,
            //{ d: 'Fait oublier un niveau du sort #3',c: 0,o: ''} ,
            //{ d: 'null',c: 0,o: '/'} ,
            //{ d: 'null',c: 0,o: '/'} ,
            //{ d: 'Reproduit la carte d\'origine',c: 0,o: '/'} ,
            //{ d: 'Invoque : #3',c: 0,o: '/'} ,
            //{ d: 'null',c: 0,o: ''} ,
            //{ d: 'Ajoute #3 points d\'honneur',c: 52,o: '+'} ,
            //{ d: 'Ajoute #3 points de déshonneur',c: 53,o: '-'} ,
            //{ d: 'Retire #3 points d\'honneur',c: 52,o: '-'} ,
            //{ d: 'Retire #3 points de déshonneur',c: 53,o: '+'} ,
            //{ d: 'Ressuscite les alliés présents sur la carte',c: 0,o: '+'} ,
            //{ d: 'PDV rendus : #1{~1~2 à }#2',c: 0,o: '+',j: true} ,
            //{ d: 'Libère les âmes des ennemis',c: 0,o: '+'} ,
            //{ d: 'Libère une âme ennemie',c: 0,o: '+'} ,
            //{ d: 'Faire semblant d\'être #3',c: 0,o: '/'}, 
            //{ d: 'null',c: 0,o: '+'} ,
            //{ d: 'Pas d\'effet supplémentaire',c: 0,o: '/'}, 
            //{ d: 'Incarnation de niveau #3',c: 0,o: '/'} ,
            //{ d: 'Dommages : #1{~1~2 à }#2% de la vie de l\'attaquant (neutre)',c: 0,o: '',t: true,j: true}, 
            //{ d: 'Dommages : #1{~1~2 à }#2% de la vie de l\'attaquant (neutre)',c: 0,o: '',t: true,j: true} ,
            //{ d: 'Dommages : #1{~1~2 à }#2% de la vie de l\'attaquant (neutre)',c: 0,o: '',j: true} ,
            //{ d: 'Lier son métier : #1',c: 0,o: '/'} ,
            //{ d: 'Change l\'élément de frappe',c: 0,o: ''}, 
            //{ d: 'Puissance : #1{~1~2 à }#2',c: 0,o: ''} ,
            //{ d: '+#1{~1~2 à }#2 point de durabilité',c: 0,o: '',j: true}, 
            //{ d: '#1% capture d\'âme de puissance #3',c: 0,o: '/'} ,
            //{ d: '#1% de proba de capturer une monture',c: 0,o: '/'}, 
            //{ d: 'Coût supplémentaire',c: 0,o: '/'} ,
            //{ d: '#1 : #3',c: 0,o: '/'} ,
            //{ d: '#1 : #3',c: 0,o: '/'} ,
            //{ d: '#1 : #3',c: 0,o: '/'} ,
            //{ d: 'Nombre de victimes : #2',c: 0,o: ''} ,
            //{ d: 'Débloque le titre #3',c: 0,o: ''} ,
            //{ d: 'Renommer la guilde : #4',c: 0,o: '/'} ,
            //{ d: 'Téléporte au prisme allié le plus proche',c: 0,o: ''} ,
            //{ d: 'Agresse les joueurs de l\'alignement ennemi automatiquement',c: 0,o: ''} ,
            //{ d: '',c: 0,o: ''} ,
            //{ d: 'null',c: 0,o: ''} ,
            //{ d: 'null',c: 0,o: ''} ,
            //{ d: 'Bonus aux chances de capture : #1{~1~2 à }#2%',c: 72,o: '/',j: true} ,
            //{ d: 'Bonus à l\'xp de la dragodinde : #1{~1~2 à }#2%',c: 73,o: '/',j: true} ,
            //{ d: 'Disparaît en se déplaçant',c: 0,o: ''} ,
            //{ d: 'Echange les places de 2 joueurs',c: 0,o: ''} ,
            //{ d: 'Confusion horaire : #1{~1~2 à }#2 degrés',c: 74,o: '-',j: true} ,
            //{ d: 'Confusion horaire : #1{~1~2 à }#2 Pi/2',c: 74,o: '-',j: true} ,
            //{ d: 'Confusion horaire : #1{~1~2 à }#2 Pi/4',c: 74,o: '-',j: true} ,
            //{ d: 'Confusion contre horaire : #1{~1~2 à }#2 degrés',c: 74,o: '-',j: true} ,
            //{ d: 'Confusion contre horaire : #1{~1~2 à }#2 Pi/2',c: 74,o: '-',j: true} ,
            //{ d: 'Confusion contre horaire : #1{~1~2 à }#2 Pi/4',c: 74,o: '-',j: true} ,
            //{ d: '+#1{~1~2 à }#2% de dégâts subis permanents',c: 75,o: '-',j: true} ,
            //{ d: 'Invoque le dernier allié mort avec #1{~1~2 à }#2 % de ses PDV',c: 0,o: '+',j: true} ,
            //{ d: 'Minimise les effets aléatoires',c: 0,o: '-'} ,
            //{ d: 'Maximise les effets aléatoires',c: 0,o: '/'} ,
            //{ d: 'Pousse jusqu\'à la case visée',c: 0,o: '/'} ,
            //{ d: 'Retour à la position de départ',c: 0,o: '/'} ,
            //{ d: 'null',c: 0,o: '/'} ,
            //{ d: 'Soigne sur attaque',c: 0,o: '/'} ,
            //{ d: '#1',c: 0,o: '/'} ,
            //{ d: 'Châtiment de #2 sur #3 tour(s)',c: 0,o: '+'} ,
            //{ d: 'null',c: 0,o: '/'} ,
            //{ d: 'null',c: 0,o: ''} ,
            //{ d: 'Prépare #1{~1~2 à }#2 parchemins pour mercenaire [wait]',c: 0,o: '/'} ,
            //{ d: 'Arme de chasse',c: 0,o: '/'} ,
            //{ d: 'Points de vie : #3',c: 0,o: ''} ,
            //{ d: 'Reçu le : #1',c: 0,o: ''} ,
            //{ d: 'Corpulence : #1',c: 0,o: ''} ,
            //{ d: 'Dernier repas : #1',c: 0,o: ''} ,
            //{ d: 'A mangé le : #1',c: 0,o: ''} ,
            //{ d: 'Taille : #3 poces',c: 0,o: ''} ,
            //{ d: 'Tour(s) restant(s) : #3',c: 0,o: ''} ,
            //{ d: 'Résistance : #2 / #3',c: 0,o: ''} ,
            //{ d: 'null',c: 0,o: '/'} ,
            //{ d: '#1',c: 0,o: '/'} ,
            //{ d: 'null',c: 0,o: ''} ,
            //{ d: 'null',c: 0,o: '/'} ,
            //{ d: 'Téléporte',c: 0,o: '+'} ,
            //{ d: 'Lance un combat contre #2',c: 0,o: '/'} ,
            //{ d: 'Augmente la sérénité, diminue l\'agressivité',c: 0,o: '/'} ,
            //{ d: 'Augmente l\'agressivité, diminue la sérénité',c: 0,o: '/'} ,
            //{ d: 'Augmente l\'endurance',c: 0,o: '+'} ,
            //{ d: 'Diminue l\'endurance',c: 0,o: '-'} ,
            //{ d: 'Augmente l\'amour',c: 0,o: '+'} ,
            //{ d: 'Diminue l\'amour',c: 0,o: '-'} ,
            //{ d: 'Accelère la maturité',c: 0,o: '+'} ,
            //{ d: 'Ralentit la maturité',c: 0,o: '-'} ,
            //{ d: 'Augmente les capacités d\'un familier #3',c: 0,o: '+'} ,
            //{ d: 'Capacités accrues',c: 0,o: '+'} ,
            //{ d: 'Retirer temporairement un objet d\'élevage',c: 0,o: '/'} ,
            //{ d: 'Récupérer un objet d\'enclos',c: 0,o: '/'} ,
            //{ d: 'Objet pour enclos',c: 0,o: '/'} ,
            //{ d: 'Monter/Descendre d\'une monture',c: 0,o: '/'} ,
            //{ d: 'Etat #3',c: 71,o: '/'} ,
            //{ d: 'Enlève l\'état \'#3\'',c: 71,o: '/'} ,
            //{ d: 'Alignement : #3',c: 0,o: '/'} ,
            //{ d: 'Grade : #3',c: 0,o: '/'} ,
            //{ d: 'Niveau : #3',c: 0,o: '/'} ,
            //{ d: 'Créé depuis : #3 jour(s)',c: 0,o: '/'} ,
            //{ d: 'Nom : #4',c: 0,o: '/'} ,
            //{ d: 'null',c: 0,o: ''} ,
            //{ d: 'null',c: 0,o: ''} ,
            //{ d: 'null',c: 0,o: ''} ,
            //{ d: 'null',c: 0,o: ''} ,
            //{ d: 'null',c: 0,o: ''} ,
            //{ d: 'Échangeable dès le : #1',c: 0,o: '/'} ,
            //{ d: 'null',c: 0,o: '/'} ,
            //{ d: 'Modifié par : #4',c: 0,o: '/'} ,
            //{ d: 'Prépare #1{~1~2 à }#2 parchemins',c: 0,o: '/'} ,
            //{ d: 'Appartient à : #4',c: 0,o: '/'} ,
            //{ d: 'Fabriqué par : #4',c: 0,o: '/'} ,
            //{ d: 'Recherche : #4',c: 0,o: '/'} ,
            //{ d: '#4',c: 0,o: '/'} ,
            //{ d: '!! Certificat invalide !!',c: 0,o: '-'} ,
            //{ d: 'Consulter la fiche de la monture',c: 0,o: '/'} ,
            //{ d: 'Appartient à : #4',c: 0,o: '/'} ,
            //{ d: 'Nom : #4',c: 0,o: '/'} ,
            //{ d: 'Validité : #1j #2h #3m',c: 0,o: '/'} ,
            //{ d: 'null',c: 0,o: '+'}, ]";

            #endregion
            MessageBox.Show("done");
            Task.Run(() =>
            {
                using (var db = new ConfigContext())
                {

                    db.Database.EnsureCreated();
                    db.SpellEffect.AsNoTracking();
                    db.SpellLvl.AsNoTracking();
                    db.Spell.AsNoTracking();
                    XmlDocument doc = new XmlDocument();
                    doc.Load(@"C:\Users\nenbe\AppData\Roaming\D-One\resources\spells.xml");
                    Spell spell = new Spell();
                    SpellLvl spellLvl = new SpellLvl();
                    SpellEffect spellEffect = new SpellEffect();
                    //db.Set<Spell>();
                    //db.Set<SpellLvl>();
                    //db.Set<SpellEffect>();
                    int i = 0;
                    int u = 0;
                    int o = 0;
                    foreach (XmlNode node in doc.DocumentElement.ChildNodes)
                    {
                        spell = new Spell();
                        //spell.Id = i;// int.Parse(node.Attributes["ID"].Value);
                        i++;
                        spell.Id = int.Parse(node.Attributes["ID"].Value);
                        spell.Name = node.ChildNodes.Item(0).InnerText;
                        db.Spell.Add(spell);
                        Debug.WriteLine(node.ChildNodes.Item(0).InnerText);



                        foreach (XmlNode node2 in node.ChildNodes)
                        {
                            if (node2.Name == "LEVEL")
                            {

                                spellLvl = new SpellLvl();
                                spellLvl.SpellId = int.Parse(node.Attributes["ID"].Value);
                                spellLvl.Lvl = int.Parse(node2.Attributes["LEVEL"].Value);
                                spellLvl.CostPa = int.Parse(node2.Attributes["COST_PA"].Value);
                                spellLvl.RangeMin = int.Parse(node2.Attributes["RANGE_MIN"].Value);
                                spellLvl.RangeMax = int.Parse(node2.Attributes["RANGE_MAX"].Value);
                                spellLvl.LaunchInline = node2.Attributes["LAUNCH_INLINE"].Value;
                                spellLvl.VisionLine = node2.Attributes["VISION_LINE"].Value;
                                spellLvl.EmptyCell = node2.Attributes["EMPTY_CELL"].Value;
                                spellLvl.ModifiableDistance = node2.Attributes["MODIFIABLE_DISTANCE"].Value;
                                spellLvl.LaunchPerTurn = int.Parse(node2.Attributes["LAUNCH_PER_TURN"].Value);
                                spellLvl.LaunchPerTarget = int.Parse(node2.Attributes["LAUNCH_PER_TARGET"].Value);
                                spellLvl.CoolDown = int.Parse(node2.Attributes["COOLDOWN"].Value);
                                db.SpellLvl.Add(spellLvl);
                                u++;
                            }
                            else
                            {
                            }

                            foreach (XmlNode node3 in node2.ChildNodes)
                            {

                                if (node3.Name == "EFFECT")
                                {

                                    spellEffect = new SpellEffect();
                                    // spellEffect.Id = o;
                                    spellEffect.SpellLvl = int.Parse(node2.Attributes["LEVEL"].Value);
                                    spellEffect.Type = int.Parse(node3.Attributes["TYPE"].Value);
                                    spellEffect.CoolDown = int.Parse(node3.Attributes["COOLDOWN"].Value);
                                    spellEffect.Target = int.Parse(node3.Attributes["TARGET"].Value);
                                    spellEffect.Range = node3.Attributes["RANGE"].Value;
                                    spellEffect.IsCritic = node3.Attributes["TYPE"].Value;
                                    db.SpellEffect.Add(spellEffect);
                                    o++;

                                }

                            }
                        }
                    }
                    db.SaveChanges();
                    MessageBox.Show("done"
                         );

                    //foreach (ParseObject m in results)
                    //{
                    //    int Id = m.Get<int>("Id");
                    //    string Name = m.Get<string>("Name");
                    //    int Area = m.Get<int>("Area");
                    //    AmountDataExtract++;
                    //    PercentExtract = (100 * AmountDataExtract) / MaxAmountDataExtract;
                    //    subArea = new SubArea() { Id = Id, Name = Name, Area = Area };
                    //    db.SubAreas.Add(subArea);
                    //}
                    //MaxAmountDataExtract = 0;
                    //AmountDataExtract = 0;
                    //PercentExtract = 0;
                    //db.SaveChanges();
                }
            });

        }

    }
}
