﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ware.Services
{
    public class LocalDb
    {
        class User
        {
            public string Name { get; set; }
            public bool IsActive { get; set; }

        }
        public LocalDb()
        {
            using (var db = new LiteDatabase(@"MyData.db"))
            {
                // Get customer collection
                var col = db.GetCollection<User>("users");

                // Create your new customer instance
                var customer = new User
                {
                    Name = "John Doe",
                    IsActive = true
                };

                // Create unique index in Name field
                col.EnsureIndex(x => x.Name, true);

                // Insert new customer document (Id will be auto-incremented)
                col.Insert(customer);

                // Update a document inside a collection
                customer.Name = "Joana Doe";

                col.Update(customer);

                // Use LINQ to query documents (with no index)
            }
        }
    }
}
