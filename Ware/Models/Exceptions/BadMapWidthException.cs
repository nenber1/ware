﻿using System;

namespace Ware.Models.Exceptions
{
    [Serializable]
    public class BadMapWidthException : Exception
    {
        public BadMapWidthException()
        {
        }

        public BadMapWidthException(string name) : base(String.Format("Invalid width"))
        {
        }
    }
}