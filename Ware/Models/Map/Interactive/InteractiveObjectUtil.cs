﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ware.Models.Map.Interactive
{
    public class InteractiveObjectUtil
    {
        public short[] gfxs { get; private set; }
        public bool caminable { get; private set; }
        public short[] habilidades { get; private set; }
        public string nombre { get; private set; }
        public bool recolectable { get; private set; }

        private static List<InteractiveObjectUtil> interactivos_modelo_cargados = new List<InteractiveObjectUtil>();

        public InteractiveObjectUtil(string _nombre, string _gfx, bool _caminable, string _habilidades, bool _recolectable)
        {
            nombre = _nombre;

            if (!_gfx.Equals("-1") && !string.IsNullOrEmpty(_gfx))
            {
                string[] separador = _gfx.Split(',');
                gfxs = new short[separador.Length];

                for (byte i = 0; i < gfxs.Length; i++)
                    gfxs[i] = short.Parse(separador[i]);
            }

            caminable = _caminable;

            if (!_habilidades.Equals("-1") && !string.IsNullOrEmpty(_habilidades))
            {
                string[] separador = _habilidades.Split(',');
                habilidades = new short[separador.Length];

                for (byte i = 0; i < habilidades.Length; ++i)
                    habilidades[i] = short.Parse(separador[i]);
            }

            recolectable = _recolectable;
            interactivos_modelo_cargados.Add(this);
        }

        public static InteractiveObjectUtil get_Modelo_Por_Gfx(short gfx_id)
        {
            foreach (InteractiveObjectUtil interactivo in interactivos_modelo_cargados)
            {
                if (interactivo.gfxs.Contains(gfx_id))
                    return interactivo;
            }
            return null;
        }

        public static InteractiveObjectUtil get_Modelo_Por_Habilidad(short habilidad_id)
        {
            IEnumerable<InteractiveObjectUtil> lista_interactivos = interactivos_modelo_cargados.Where(i => i.habilidades != null);

            foreach (InteractiveObjectUtil interactivo in lista_interactivos)
            {
                if (interactivo.habilidades.Contains(habilidad_id))
                    return interactivo;
            }
            return null;
        }

        public static List<InteractiveObjectUtil> get_Interactivos_Modelos_Cargados() => interactivos_modelo_cargados;
    }
}
