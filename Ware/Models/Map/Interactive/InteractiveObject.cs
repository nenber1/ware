﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ware.Models.Map.Interactive
{
    public class InteractiveObject
    {
        public short gfx { get; private set; }
        public Cell celda { get; private set; }
        public InteractiveObjectUtil modelo { get; private set; }
        public bool es_utilizable { get; set; } = false;

        public InteractiveObject(short _gfx, Cell _celda)
        {
            gfx = _gfx;
            celda = _celda;

            InteractiveObjectUtil _modelo = InteractiveObjectUtil.get_Modelo_Por_Gfx(gfx);

            if (_modelo != null)
            {
                modelo = _modelo;
                es_utilizable = true;
            }
        }
    }
    
}
