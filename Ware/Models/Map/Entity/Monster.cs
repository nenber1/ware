﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ware.Models.Map.Entity
{
    public class Monster : IEntity
    {
        public int id { get; set; } = 0;
        public int template_id { get; set; } = 0;
        public Cell celda { get; set; }
        public int nivel { get; set; }

        public List<Monster> moobs_dentro_grupo { get; set; }
        public Monster lider_grupo { get; set; }
        bool disposed;

        public int get_Total_Monstruos => moobs_dentro_grupo.Count + 1;
        public int get_Total_Nivel_Grupo => lider_grupo.nivel + moobs_dentro_grupo.Sum(f => f.nivel);

        public Monster(int _id, int _template, Cell _celda, int _nivel)
        {
            id = _id;
            template_id = _template;
            celda = _celda;
            moobs_dentro_grupo = new List<Monster>();
            nivel = _nivel;
        }

        public bool get_Contiene_Monstruo(int id)
        {
            if (lider_grupo.template_id == id)
                return true;

            for (int i = 0; i < moobs_dentro_grupo.Count; i++)
            {
                if (moobs_dentro_grupo[i].template_id == id)
                    return true;
            }
            return false;
        }

        public int get_number_monstre(int id)
        {
            int nombre = 0;
            if (lider_grupo.template_id == id)
                nombre++;

            for (int i = 0; i < moobs_dentro_grupo.Count; i++)
            {
                if (moobs_dentro_grupo[i].template_id == id)
                    nombre++;
            }

            return nombre;
        }

        public void Dispose() => Dispose(true);
        ~Monster() => Dispose(false);

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                moobs_dentro_grupo.Clear();
                moobs_dentro_grupo = null;
                disposed = true;
            }
        }
    }
}
