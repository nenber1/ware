﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ware.Models.Map.Entity
{
    public class Character : IEntity
    {
        public int id { get; set; } = 0;
        public string nombre { get; set; }
        public byte sexo { get; set; } = 0;
        public Cell celda { get; set; }
        private bool disposed;

        public Character(int _id, string _nombre_personaje, byte _sexo, Cell _celda)
        {
            id = _id;
            nombre = _nombre_personaje;
            sexo = _sexo;
            celda = _celda;
        }

        #region Zona Dispose
        ~Character() => Dispose(false);
        public void Dispose() => Dispose(true);

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                celda = null;
                disposed = true;
            }
        }
        #endregion
    }
}
