﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ware.Models.Map.Entity
{
    public interface IEntity : IDisposable
    {
        int id { get; set; }
        Cell celda { get; set; }
    }
}
