﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ware.Models.Map.Entity
{
    public class Npc : IEntity
    {
        public int id { get; set; }
        public int npc_modelo_id { get; private set; }
        public Cell celda { get; set; }

        public short pregunta { get; set; }
        public List<short> respuestas { get; set; }
        private bool disposed;

        public Npc(int _id, int _npc_modelo_id, Cell _celda)
        {
            id = _id;
            npc_modelo_id = _npc_modelo_id;
            celda = _celda;
        }

        #region Zona Dispose
        ~Npc() => Dispose(false);
        public void Dispose() => Dispose(true);

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                respuestas?.Clear();
                respuestas = null;
                celda = null;
                disposed = true;
            }
        }
        #endregion
    }
}
