﻿using System;
using System.Drawing;
using Ware.Enums;

namespace Ware.Models.Character
{
    public class BotMessage
    {
        private static Color Information = Color.Green;
        private static Color Client = Color.Orange;
        private static Color NULL = Color.Green;

        // Properties
        public BotType Sender { get; set; }

        public string Message { get; set; }
        public DateTime Time { get; set; }
        public System.Windows.Media.SolidColorBrush ForegroundColor { get; set; }

        // Constructor
        public BotMessage(BotType sender, string message)
        {
            Sender = sender;
            Time = DateTime.Now;
            string type;
            switch (Sender)
            {
                case BotType.Information:
                    ForegroundColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(Information.A, Information.R, Information.G, Information.B));
                    type = $"[{Time:HH:mm}] [Information] ";
                    break;

                case BotType.Harvest:
                    ForegroundColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(Client.A, Client.R, Client.G, Client.B));
                    type = $"[{Time:HH:mm}] [Recolte] ";
                    break;

                case BotType.Group:
                    ForegroundColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(NULL.A, NULL.R, NULL.G, NULL.B));
                    type = $"[{Time:HH:mm}] [Groupe] ";
                    break;

                default:
                    type = "";
                    break;
            }
            Message = type + message;
            ForegroundColor.Freeze();
        }
    }
}