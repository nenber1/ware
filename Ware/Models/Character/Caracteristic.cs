﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Ware.Models.Character
{
    public class Caracteristic
    {
        public Caracteristic(Bitmap image,string name, string value)
        {
            Name = name;
            Value = value;
            using (var memory = new MemoryStream())
            {
                image.Save(memory, ImageFormat.Png);
                memory.Position = 0;

                var bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();
                bitmapImage.Freeze();

                Image =  bitmapImage;
            }
        }
        public Caracteristic(string name, string value)
        {
            Name = name;
            Value = value;
            Image = null;
        }
        public BitmapImage Image { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
