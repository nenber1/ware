﻿using System.Collections.ObjectModel;
using System.ComponentModel;

namespace Ware.Models.Character
{
    public sealed class InstanceGroup : BaseInstance, INotifyPropertyChanged
    {
        private string name;
        public string Name { get => name; set { name = value; RaisePropertyChanged("Name"); } }
        private ObservableCollection<Instance> instancesCollection;

        public ObservableCollection<Instance> InstancesCollection
        {
            get { return instancesCollection; }
            set
            {
                RaisePropertyChanged("InstancesCollection");
                instancesCollection = value;
            }
        }

        //public InstanceGroup(string name, params Instance[] instances) : base(name)
        //{
        //    Name = name;

        //}
        public InstanceGroup(string name) : base(name)
        {
            Name = name;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            // take a copy to prevent thread issues
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}