﻿using System;
using System.Drawing;
using Ware.Enums;

namespace Ware.Models.Character
{
    public class DebugMessage
    {
        private static Color Server = Color.Red;
        private static Color Client = Color.Orange;
        private static Color NULL = Color.Green;

        // Properties
        public DebugType Sender { get; set; }

        public string Message { get; set; }
        public DateTime Time { get; set; }
        public System.Windows.Media.SolidColorBrush ForegroundColor { get; set; }

        // Constructor
        public DebugMessage(DebugType sender, string message)
        {
            Sender = sender;
            Time = DateTime.Now;
            string type;
            switch (Sender)
            {
                case DebugType.Server:
                    ForegroundColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(Server.A, Server.R, Server.G, Server.B));
                    type = $"[{Time:HH:mm}] [SERVEUR] ";
                    break;

                case DebugType.Client:
                    ForegroundColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(Client.A, Client.R, Client.G, Client.B));
                    type = $"[{Time:HH:mm}] [CLIENT] ";
                    break;

                case DebugType.NULL:
                    ForegroundColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(NULL.A, NULL.R, NULL.G, NULL.B));
                    type = $"[{Time:HH:mm}] [NULL] ";
                    break;

                default:
                    type = "";
                    break;
            }
            Message = type + message;
            ForegroundColor.Freeze();
        }
    }
}