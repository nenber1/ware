﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using Ware.ViewModels;

namespace Ware.Models.Character
{
    public abstract class BaseInstance : INotifyPropertyChanged
    {
        //private object _content;
        private string _name;
        public BaseInstance(string name)
        {
            Name = name;
        }
        public string Name
        {
            get { return _name; }
            set { this.RaisePropertyChanged("Name"); }
        }

        //public object Content
        //{
        //    get { return _content; }
        //    set { this.MutateVerbose(ref _content, value, RaisePropertyChanged("Content")); }
        //}


        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Methods


        private void RaisePropertyChanged(string propertyName)
        {
            // take a copy to prevent thread issues
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}
