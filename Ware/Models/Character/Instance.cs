﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Ware.Core.MDI;
using Ware.Engine.Managers;
using Ware.Engine.Providers;

//using Ware.Engine.Managers;
using Ware.Enums;
using Ware.Network.MITM;
using Ware.Network.Protocol;
using Ware.View.MITM;
using Ware.ViewModels;

namespace Ware.Models.Character
{
    public class Instance : BaseInstance, IDisposable, INotifyPropertyChanged
    {
        private Models.LocalDatabase.Map map;
        public Models.LocalDatabase.Map Map { get => map; set { map = value; RaisePropertyChanged("Map"); } }
        private Models.LocalDatabase.SubArea subArea;
        public Models.LocalDatabase.SubArea SubArea { get => subArea; set { subArea = value; RaisePropertyChanged("SubArea"); } }
        private Models.LocalDatabase.Area area;
        public Models.LocalDatabase.Area Area { get => area; set { area = value; RaisePropertyChanged("Area"); } }
        private InstanceGroup group;
        private WareType WareType;
        private Processus Processus;
        private DashboardViewModel dashboardViewModel;
        private LogMessage log;
        private DebugMessage debug;
        private BotMessage botMessage;
        public DashBoardMITM DashBoard { get; set; }
        private InstanceState instanceState;
        public InstanceState InstanceState { get => instanceState; set { instanceState = value; RaisePropertyChanged("InstanceState"); } }
        private string subscription;
        public string Subscription { get => subscription; set { subscription = value; RaisePropertyChanged("Subscription"); } }
        public DashboardViewModel DashboardViewModel { get => dashboardViewModel; set { dashboardViewModel = value; RaisePropertyChanged("DashBoardViewModel"); } }
        private string name;
        public string Name { get => name; set { name = value; RaisePropertyChanged("Name"); } }
        private InstanceGroup instanceGroup;
        public InstanceGroup InstanceGroup { get => instanceGroup; set { instanceGroup = value; RaisePropertyChanged("InstanceGroup"); } }
        private string idCharacter;
        public string IdCharacter { get => idCharacter; set { idCharacter = value; RaisePropertyChanged("IdCharacter"); } }

        #region Managers

        public MapManager MapManager { get; }
        public CommandManager CommandManager { get; }
        public CharacterManager CharacterManager { get; }
        public SocketManager SocketManager { get; private set; }
        private ProtocolManager ProtocolManager;
        #endregion Managers

        /// <summary>
        /// Frames
        /// </summary>
        //public ServerAuthentificationFrame ServerAuthentificationFrame { get; private set; }

        public Instance(string name, WareType wareType) : base(name)
        {
            WareType = wareType;
            Name = name;

            SocketManager = new SocketManager("172.65.206.193", "443");
            SocketManager.OnDisconnection += SocketManager_OnDisconnection;
            if (WareType == WareType.MITM)
            {
                InstanceState = InstanceState.Disconnected;
                dashboardViewModel = new DashboardViewModel();
                DashBoard = new DashBoardMITM(dashboardViewModel);
                ProtocolManager = new ProtocolManager(this);
                DashBoard.onProcessReady += DashBoard_onProcessReady;
                MapManager = new MapManager(this);
                CommandManager = new CommandManager(this);
                CharacterManager = new CharacterManager(this);
                LogProvider logProvider = new LogProvider(Debug, Log);
            }
        }
        public void Disconnect()
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                SocketManager.Disconnect();
                SocketManager = new SocketManager("172.65.206.193", "443");
                SocketManager.OnDisconnection += SocketManager_OnDisconnection;
                if (WareType == WareType.MITM)
                {
                    InstanceState = InstanceState.Disconnected;
                    dashboardViewModel = new DashboardViewModel();
                    DashBoard = new DashBoardMITM(dashboardViewModel);
                    ProtocolManager = new ProtocolManager(this);
                    DashBoard.onProcessReady += DashBoard_onProcessReady;
                }
            }));
            
        }

        private void SocketManager_OnDisconnection(object sender, SocketDisconnectionEventArgs socketDisconnectionEventArgs)
        {
            InstanceState = InstanceState.Disconnected;
        }

        private void DashBoard_onProcessReady(object sender, Processus e)
        {
            Processus = e;
            //Task t = Task.Run(() => Thread.Sleep(5000));
            SocketManager.Start(ProtocolManager, Processus.Proc.Id);
            Listener.Subscribe(Processus.Proc.Id);
        }

        public void Log(LogMessage logMessage)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                log = logMessage;
                dashboardViewModel.LogMessages.Add(log);
            }));
        }

        public void Debug(DebugMessage debugMessage)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                debug = debugMessage;
                dashboardViewModel.DebugMessages.Add(debug);
            }));
        }

        public void LogBot(BotMessage botMes)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                botMessage = botMes;
                dashboardViewModel.BotMessages.Add(botMessage);
            }));
        }

        public InstanceGroup Group { get; set; }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion INotifyPropertyChanged Members

        #region Methods

        private void RaisePropertyChanged(string propertyName)
        {
            // take a copy to prevent thread issues
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Methods

        public void Dispose()
        {
            try
            {
                //SocketManager.Disconnect();
            }
            catch (Exception)
            {
            }
            try
            {
                DashBoard.Process.Proc.Kill();
                DashBoard.Dispose();
            }
            catch (Exception)
            {
            }
        }

        ~Instance()  // finalizer
        {
        }
    }
}