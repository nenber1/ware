﻿using System;
using System.Drawing;
using Ware.Enums;

namespace Ware.Models.Character
{
    public class LogMessage
    {
        private static Color General = Color.White;
        private static Color Prive = Color.DodgerBlue;
        private static Color Guilde = Color.DarkViolet;
        private static Color Alignement = Color.DarkOrange;
        private static Color Recrutement = Color.DarkGray;
        private static Color Commerce = Color.SaddleBrown;
        private static Color Alert = Color.Red;
        private static Color Info = Color.LimeGreen;
        private static Color Bot = Color.SteelBlue;
        private static Color Equipe = Color.CornflowerBlue;
        private static Color Groupe = Color.DarkCyan;

        // Properties
        public LogType Sender { get; set; }

        public string Head { get; set; }
        public string Name { get; set; }
        public string Message { get; set; }
        public DateTime Time { get; set; }
        public System.Windows.Media.SolidColorBrush ForegroundColor { get; set; }

        // Constructor
        public LogMessage(LogType sender, string message)
        {
            Name = "";
            Sender = sender;
            Time = DateTime.Now;
            Message = message;
            switch (Sender)
            {
                case LogType.General:
                    ForegroundColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(General.A, General.R, General.G, General.B));
                    Head = $"[{Time:HH:mm}] [General]";
                    break;

                case LogType.Prive:
                    ForegroundColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(Prive.A, Prive.R, Prive.G, Prive.B));
                    Head = $"[{Time:HH:mm}] [Privé]";
                    break;

                case LogType.Guilde:
                    ForegroundColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(Guilde.A, Guilde.R, Guilde.G, Guilde.B));
                    Head = $"[{Time:HH:mm}] [Guilde]";
                    break;

                case LogType.Alignement:
                    ForegroundColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(Alignement.A, Alignement.R, Alignement.G, Alignement.B));
                    Head = $"[{Time:HH:mm}] [Alignement]";
                    break;

                case LogType.Recrutement:
                    ForegroundColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(Recrutement.A, Recrutement.R, Recrutement.G, Recrutement.B));
                    Head = $"[{Time:HH:mm}] [Recrutement]";
                    break;

                case LogType.Commerce:
                    ForegroundColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(Commerce.A, Commerce.R, Commerce.G, Commerce.B));
                    Head = $"[{Time:HH:mm}] [Commerce]";
                    break;

                case LogType.Alert:
                    ForegroundColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(Alert.A, Alert.R, Alert.G, Alert.B));
                    Head = $"[{Time:HH:mm}] [Dofus]";
                    break;

                case LogType.Info:
                    ForegroundColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(Info.A, Info.R, Info.G, Info.B));
                    Head = $"[{Time:HH:mm}] [Dofus]";
                    break;

                case LogType.Ware:
                    ForegroundColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(Bot.A, Bot.R, Bot.G, Bot.B));
                    Head = $"[{Time:HH:mm}] [Ware]";
                    break;

                case LogType.Equipe:
                    ForegroundColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(Equipe.A, Equipe.R, Equipe.G, Equipe.B));
                    Head = $"[{Time:HH:mm}] [Equipe]";
                    break;

                case LogType.Groupe:
                    ForegroundColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(Groupe.A, Groupe.R, Groupe.G, Groupe.B));
                    Head = $"[{Time:HH:mm}] [Groupe]";
                    break;

                default:
                    Head = "";
                    break;
            }
            ForegroundColor.Freeze();
        }

        // Constructor
        public LogMessage(LogType sender, string message, string name)
        {
            Name = "" + name + ": ";
            Sender = sender;
            Time = DateTime.Now;
            Message = message;
            switch (Sender)
            {
                case LogType.General:
                    ForegroundColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(General.A, General.R, General.G, General.B));
                    Head = $"[{Time:HH:mm}] [General]";
                    break;

                case LogType.Prive:
                    ForegroundColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(Prive.A, Prive.R, Prive.G, Prive.B));
                    Head = $"[{Time:HH:mm}] [Privé]";
                    break;

                case LogType.Guilde:
                    ForegroundColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(Guilde.A, Guilde.R, Guilde.G, Guilde.B));
                    Head = $"[{Time:HH:mm}] [Guilde]";
                    break;

                case LogType.Alignement:
                    ForegroundColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(Alignement.A, Alignement.R, Alignement.G, Alignement.B));
                    Head = $"[{Time:HH:mm}] [Alignement]";
                    break;

                case LogType.Recrutement:
                    ForegroundColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(Recrutement.A, Recrutement.R, Recrutement.G, Recrutement.B));
                    Head = $"[{Time:HH:mm}] [Recrutement]";
                    break;

                case LogType.Commerce:
                    ForegroundColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(Commerce.A, Commerce.R, Commerce.G, Commerce.B));
                    Head = $"[{Time:HH:mm}] [Commerce]";
                    break;

                case LogType.Alert:
                    ForegroundColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(Alert.A, Alert.R, Alert.G, Alert.B));
                    Head = $"[{Time:HH:mm}] [Dofus]";
                    break;

                case LogType.Info:
                    ForegroundColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(Info.A, Info.R, Info.G, Info.B));
                    Head = $"[{Time:HH:mm}] [Dofus]";
                    break;

                case LogType.Ware:
                    ForegroundColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(Bot.A, Bot.R, Bot.G, Bot.B));
                    Head = $"[{Time:HH:mm}] [Ware]";
                    break;

                case LogType.Equipe:
                    ForegroundColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(Equipe.A, Equipe.R, Equipe.G, Equipe.B));
                    Head = $"[{Time:HH:mm}] [Equipe]";
                    break;

                case LogType.Groupe:
                    ForegroundColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(Groupe.A, Groupe.R, Groupe.G, Groupe.B));
                    Head = $"[{Time:HH:mm}] [Groupe]";
                    break;

                default:
                    Head = "";
                    break;
            }
            ForegroundColor.Freeze();
        }
    }
}