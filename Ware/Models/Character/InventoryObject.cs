﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Ware.Models.LocalDatabase;

namespace Ware.Models.Character
{
    public class InventoryObject
    {
        public Item ObjectModel { get; }
        public uint Uid { get; }
        public int Amount { get; } = 0;
        public int Position { get; } = -1;
        public List<Caracteristic> Caracteristics {get;}

        public InventoryObject(Item model, string data)
        {
            ObjectModel = model;
            string[] props = data.Split('~');
            if (!string.IsNullOrEmpty(props[0]))
                Uid = Convert.ToUInt32((props[0]),16);
            //Amount = Convert.ToInt32((props[2]),16);
            if(!string.IsNullOrEmpty(props[3]))
                Position = Convert.ToInt32((props[2]),16);
        }
    }
}
