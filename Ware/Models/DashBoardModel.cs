﻿using Ware.Core.MDI;

namespace Ware.Models
{

    public class DashBoardModel
    {
        #region Members
        string _life;
        string _power;
        string _pods;
        string _kamas;
        #endregion


        #region Properties
        /// <summary>
        /// The artist name.
        /// </summary>
        public string Life
        {
            get { return _life; }
            set { _life = value; }
        }
        public string Power
        {
            get { return _power; }
            set { _power = value; }
        }
        public string Pods
        {
            get { return _pods; }
            set { _pods = value; }
        }
        public string Kamas
        {
            get { return _kamas; }
            set { _kamas = value; }
        }


        public string Abonnement { get; set; }
        public string WareState { get; set; }
        public int UserState { get; set; }
        public string Chat { get; set; }
        public Processus Processus { get; set; }
        #endregion
    }
}
