﻿using System.Collections.ObjectModel;
using System.Windows.Controls;
using Ware.ViewModels;

namespace Ware.Models
{
    public class Main
    {
        public string SnackMessage { get; set; }
        public UserControl Content { get; set; }
        public string Utilisateur { get; set; }
        public object DialogHostContent { get; set; }
        public bool DialogHostIsOpen { get; set; }
        public ObservableCollection<MainMenuItem> MainMenuItems { get; }
    }
}
