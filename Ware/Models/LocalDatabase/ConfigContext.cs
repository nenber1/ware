﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Data.Entity.Migrations;
using System.IO;

namespace Ware.Models.LocalDatabase
{
    public class ConfigContext : Microsoft.EntityFrameworkCore.DbContext
    {
        private string _dbPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Ware\config.db3";
        public DbSet<Config> Configs { get; set; }
        public DbSet<Map> Maps { get; set; }
        public DbSet<Area> Areas { get; set; }
        public DbSet<SubArea> SubAreas { get; set; }
        public DbSet<Spell> Spell { get; set; }
        public DbSet<SpellLvl> SpellLvl { get; set; }
        public DbSet<SpellEffect> SpellEffect { get; set; }
        public DbSet<Item> Items { get; set; }

        public ConfigContext()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Ware"))
            {
                DirectoryInfo di = Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData));
            }
            if (!File.Exists(_dbPath))
            {
                FileStream fi = File.Create(_dbPath);
            }
            optionsBuilder.UseSqlite(@"Data Source=" + _dbPath + ";");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
            modelBuilder.Entity<Map>()
                .Property(b => b.Teleports)
                .IsRequired(false);
            modelBuilder.Entity<Spell>().HasKey(entity=> entity.Id);
            modelBuilder.Entity<SpellEffect>().HasKey(entity => entity.Id);
           
            modelBuilder.Entity<SpellLvl>().HasKey(entity => entity.Id);
            modelBuilder.Entity<Item>().HasKey(entity => entity.Id);

        }
    }
}