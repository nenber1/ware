﻿namespace Ware.Models.LocalDatabase
{
    public class SubArea
    {
        public int? Id { get; set; } = 0;
        public string Name { get; set; } = "";
        public int? Area { get; set; } = -1;
    }
}