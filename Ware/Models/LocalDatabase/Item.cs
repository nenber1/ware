﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ware.Models.LocalDatabase
{
    public class Item
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int Type { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }
        public int Pods { get; set; }
        public int IsEtheral { get; set; }
        public string Conditions { get; set; }
        public string WeaponStats { get; set; }
    }
}
