﻿namespace Ware.Models.LocalDatabase
{
    public class Config
    {
        public int ConfigId { get; set; }
        public string GamePath { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }
    }
}