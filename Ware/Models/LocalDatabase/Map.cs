﻿namespace Ware.Models.LocalDatabase
{
    public class Map
    {
        public int Id { get; set; } = 0;
        public int Width { get; set; } = 0;
        public int Height { get; set; } = 0;
        public int X { get; set; } = 0;
        public int Y { get; set; } = 0;
        public string Teleports { get; set; } = "";
        public string Data { get; set; } = "";
        public int? SubArea { get; set; } = -1;
    }
}