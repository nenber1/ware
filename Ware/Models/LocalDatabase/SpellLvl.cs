﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Ware.Models.LocalDatabase
{
    public class SpellLvl
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int SpellId { get; set; }
        public int Lvl { get; set; }
        public int CostPa { get; set; }
        public int RangeMin { get; set; }
        public int RangeMax { get; set; }
        public string LaunchInline { get; set; }
        public string VisionLine { get; set; }
        public string EmptyCell { get; set; }
        public string ModifiableDistance { get; set; }
        public int LaunchPerTurn { get; set; }
        public int LaunchPerTarget { get; set; }
        public int CoolDown { get; set; }


    }
}

 