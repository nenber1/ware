﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using System.Threading.Tasks;

namespace Ware.Models.LocalDatabase
{
    public class SpellEffect
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        public int SpellLvl { get; set; }
        public int? Type { get; set; } = 0;
        public int? CoolDown { get; set; } = 0;
        public int? Target { get; set; } = 0;
        public string Range { get; set; } = "";
        public string IsCritic { get; set; }
    }
}
