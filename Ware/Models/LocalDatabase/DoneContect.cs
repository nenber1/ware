﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.IO;
using System.Reflection;

namespace Ware.Models.LocalDatabase
{
    public class DoneContect : DbContext
    {
        private string _dbPath = @"C:\Users\nenbe\AppData\Roaming\D-One\resources\db.db3";
        public DbSet<Map> Map { get; set; }

        public DoneContect()
        {
            if (!Directory.Exists(@"C: \Users\nenbe\AppData\Roaming\D-One\resources"))
            {
                Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData));
            }
            if (!File.Exists(_dbPath))
            {
                File.Create(_dbPath);
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data Source=" + _dbPath + ";");
            optionsBuilder.EnableSensitiveDataLogging(true);
        }
       

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Map>()
                .Property(b => b.Teleports)
                .IsRequired(false);
        }
    }
}