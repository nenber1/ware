﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ware.Enums;

namespace Ware.Models
{
    public static class Extensions
    {
        

        public static string FullMessageException(this Exception ex)
        {
            Exception e = ex;
            StringBuilder s = new StringBuilder();
            while (e != null)
            {
                s.AppendLine("Exception type: " + e.GetType().FullName);
                s.AppendLine("Message       : " + e.Message);
                s.AppendLine("Stacktrace:");
                s.AppendLine(e.StackTrace);
                s.AppendLine();
                e = e.InnerException;
            }
            return s.ToString();
        }

        //public static T get_Or<T>(this Table table, string key, DataType type, T orValue)
        //{
        //    DynValue flag = table.Get(key);

        //    if (flag.IsNil() || flag.Type != type)
        //        return orValue;

        //    try
        //    {
        //        return (T)flag.ToObject(typeof(T));
        //    }
        //    catch
        //    {
        //        return orValue;
        //    }
        //}

        public static Dictionary<MapDirection, List<short>> Add(this Dictionary<MapDirection, List<short>> cells, short cellId)
        {
            short[] topCells = new short[] { 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 36 };
            short[] rightCells = new short[] { 28, 57, 86, 115, 144, 173, 231, 202, 260, 289, 318, 347, 376, 405, 434 };
            short[] bottomCells = new short[] { 451, 452, 453, 454, 455, 456, 457, 458, 459, 460, 461, 462, 463 };
            short[] leftCells = new short[] { 15, 44, 73, 102, 131, 160, 189, 218, 247, 276, 305, 334, 363, 392, 421, 450 };

            if (topCells.Contains(cellId))
            {
                if (cells.ContainsKey(MapDirection.TOP))
                    cells[MapDirection.TOP].Add(cellId);
                else
                {
                    cells.Add(MapDirection.TOP, new List<short>());
                    cells[MapDirection.TOP].Add(cellId);
                }
            }

            if (rightCells.Contains(cellId))
            {
                if (cells.ContainsKey(MapDirection.RIGHT))
                    cells[MapDirection.RIGHT].Add(cellId);
                else
                {
                    cells.Add(MapDirection.RIGHT, new List<short>());
                    cells[MapDirection.RIGHT].Add(cellId);
                }
            }

            if (bottomCells.Contains(cellId))
            {
                if (cells.ContainsKey(MapDirection.BOTTOM))
                    cells[MapDirection.BOTTOM].Add(cellId);
                else
                {
                    cells.Add(MapDirection.BOTTOM, new List<short>());
                    cells[MapDirection.BOTTOM].Add(cellId);
                }
            }

            if (leftCells.Contains(cellId))
            {
                if (cells.ContainsKey(MapDirection.LEFT))
                    cells[MapDirection.LEFT].Add(cellId);
                else
                {
                    cells.Add(MapDirection.LEFT, new List<short>());
                    cells[MapDirection.LEFT].Add(cellId);
                }
            }

            return cells;
        }
    }
}
