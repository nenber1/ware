﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Ware.Common.Utils;
using Ware.Engine.Providers;
using Ware.Engine.Providers.Utils;
using Ware.Enums;
using Ware.Models;
using Ware.Models.Character;
using Ware.Models.LocalDatabase;
using Ware.Models.Map;
using Ware.Models.Map.Entity;
using Ware.Models.Map.Interactive;

namespace Ware.Engine.Managers
{
    public class MapManager : INotifyPropertyChanged
    {
        private Instance instance;
        public Cell[] Cells { get; set; }
        public Models.LocalDatabase.Map Map { get; set; }
        public PathProvider pathProvider;
        public Dictionary<MapDirection, List<short>> CellsTeleport;
        private List<Cell> actual_path;
        public Cell CurrentCell { get; set; }

        /** Concurrent para forzar thread-safety **/
        public ConcurrentDictionary<int, IEntity> entities;
        public ConcurrentDictionary<int, InteractiveObject> interactives;

        public MapManager(Instance inst)
        {
            instance = inst;
            pathProvider = new PathProvider(this);
            entities = new ConcurrentDictionary<int, IEntity>();
            interactives = new ConcurrentDictionary<int, InteractiveObject>();
            CellsTeleport = new Dictionary<MapDirection, List<short>>();
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion INotifyPropertyChanged Members

        //#region Methods
        public Cell GetCellFromId(int CellId) => Cells[CellId];



        public void LoadMap(string data)
        {
            entities.Clear();
            interactives.Clear();
            CellsTeleport.Clear();

            Models.LocalDatabase.Map map;
            Area area;
            SubArea subArea;
            using (var db = new ConfigContext())
            {
                db.Database.EnsureCreated();
                int idMap = int.Parse(data.Split('|')[1]);
                map = db.Maps.Find(idMap);
                if (map != null)
                {
                    subArea = db.SubAreas.Find(map.SubArea);
                    if (subArea != null)
                    {
                        area = db.Areas.Find(subArea.Area);
                    }
                    else
                    {
                        area = null;
                    }
                }
                else
                {
                    area = null;
                    subArea = null;
                }
            }
            instance.Map = map;
            this.Map = map;
            instance.SubArea = subArea;
            instance.Area = area;

            DecompressMap(Map.Data);
            
            //Character c = lista_personajes().Where(ch => ch.id == int.Parse(instance.IdCharacter)).First();
           // currentCell = c.celda;

            Task.Run(() => getTeleportCell(Cells)).Wait();
            //Task.Factory.StartNew(() =>
            //{
            //    foreach (Cell item in Cells)
            //    {
            //        if (item != null)
            //        {
            //            instance.LogBot(new BotMessage(Enums.BotType.Information, item.cellId.ToString() + " : " + item.isActive));

            //        }
            //    }
            //});
        }

        public void getTeleportCell(Cell[] cells)
        {
            List<Cell> cellsToManipulate = cells.ToList().Where(c => c.IsTeleportCell()).ToList();
            cellsToManipulate.ForEach(cell =>
            {
                CellsTeleport.Add(cell.cellId);
            });
        }

        public string GetCoordinates => $"[{Map.X},{Map.Y}]";
        public Cell GetCellFromId(short prmCellId) => Cells[prmCellId];
        public bool esta_En_Mapa(string _coordenadas) => _coordenadas == Map.Id.ToString() || _coordenadas == GetCoordinates;
        public Cell GetCellByCoordinates(int prmX, int prmY) => Cells.FirstOrDefault(cell => cell.x == prmX && cell.y == prmY);
        public bool get_Puede_Luchar_Contra_Grupo_Monstruos(int monstruos_minimos, int monstruos_maximos, int nivel_minimo, int nivel_maximo, List<int> monstruos_prohibidos, List<int> monstruos_obligatorios) => get_Grupo_Monstruos(monstruos_minimos, monstruos_maximos, nivel_minimo, nivel_maximo, monstruos_prohibidos, monstruos_obligatorios).Count > 0;

        // si el destino es una celda teleport, aunque haya un monstruo encima de la celda no causara agresion
        public List<Cell> celdas_ocupadas() => entities.Values.Where(c => c is Monster).Select(c => c.celda).ToList();
        public List<Npc> lista_npcs() => entities.Values.Where(n => n is Npc).Select(n => n as Npc).ToList();
        public List<Monster> lista_monstruos() => entities.Values.Where(n => n is Monster).Select(n => n as Monster).ToList();
        public List<Character> lista_personajes() => entities.Values.Where(n => n is Character).Select(n => n as Character).ToList();

        public List<Monster> get_Grupo_Monstruos(int monstruos_minimos, int monstruos_maximos, int nivel_minimo, int nivel_maximo, List<int> monstruos_prohibidos, List<int> monstruos_obligatorios)
        {
            List<Monster> grupos_monstruos_disponibles = new List<Monster>();

            foreach (Monster grupo_monstruo in lista_monstruos())
            {
                if (grupo_monstruo.get_Total_Monstruos < monstruos_minimos || grupo_monstruo.get_Total_Monstruos > monstruos_maximos)
                    continue;

                if (grupo_monstruo.get_Total_Nivel_Grupo < nivel_minimo || grupo_monstruo.get_Total_Nivel_Grupo > nivel_maximo)
                    continue;

                if (grupo_monstruo.celda.cellType == CellTypes.TELEPORT_CELL)
                    continue;

                bool es_valido = true;

                if (monstruos_prohibidos != null)
                {
                    for (int i = 0; i < monstruos_prohibidos.Count; i++)
                    {
                        if (grupo_monstruo.get_Contiene_Monstruo(monstruos_prohibidos[i]))
                        {
                            es_valido = false;
                            break;
                        }
                    }
                }

                if (monstruos_obligatorios != null && es_valido)
                {
                    for (int i = 0; i < monstruos_obligatorios.Count; i++)
                    {
                        if (!grupo_monstruo.get_Contiene_Monstruo(monstruos_obligatorios[i]))
                        {
                            es_valido = false;
                            break;
                        }
                    }
                }

                if (es_valido)
                    grupos_monstruos_disponibles.Add(grupo_monstruo);
            }
            return grupos_monstruos_disponibles;
        }
        public bool isGroupCapturable(List<KeyValuePair<int, int>> list_monstre_nombre, Monster groupe_actual)
        {
            bool capturable = false;

            foreach (var item in list_monstre_nombre)
            {
                if (item.Value <= groupe_actual.get_number_monstre(item.Key))
                {
                    capturable = true;
                    break;
                }
            }
            return capturable;
        }

        //public void GetMapRefreshEvent() => mapRefreshEvent?.Invoke();
        //public void GetEntitiesRefreshEvent() => entitiesRefreshEvent?.Invoke();

        #region Metodos de descompresion
        private void DecompressMap(string prmMapData)
        {

            Cells = new Cell[prmMapData.Length / 10];
            string cells_value;

            for (int i = 0; i < prmMapData.Length; i += 10)
            {
                cells_value = prmMapData.Substring(i, 10);
                Cells[i / 10] = DecompressCell(cells_value, Convert.ToInt16(i / 10));
            }
            for (int i = 0; i < Map.Teleports.Split('|').Length; i++)
            {
                var myCells = from val in Cells
                              let parseVal = val
                              where val.cellId == short.Parse(Map.Teleports.Split('|')[i].Split(',')[0])
                              select parseVal;
                foreach(Cell ce in myCells)
                {
                    GetCellFromId(ce.cellId).cellType = CellTypes.TELEPORT_CELL;
                }
            }
        }

        private Cell DecompressCell(string prmCellData, short prmCellId)
        {
            byte[] cellInformations = new byte[prmCellData.Length];

            for (int i = 0; i < prmCellData.Length; i++)
                cellInformations[i] = Convert.ToByte(Functions.get_Hash(prmCellData[i]));

            CellTypes tipo = (CellTypes)((cellInformations[2] & 56) >> 3);
            bool activa = (cellInformations[0] & 32) >> 5 != 0;
            bool es_linea_vision = (cellInformations[0] & 1) != 1;
            bool tiene_objeto_interactivo = ((cellInformations[7] & 2) >> 1) != 0;
            short layer_objeto_2_num = Convert.ToInt16(((cellInformations[0] & 2) << 12) + ((cellInformations[7] & 1) << 12) + (cellInformations[8] << 6) + cellInformations[9]);
            short layer_objeto_1_num = Convert.ToInt16(((cellInformations[0] & 4) << 11) + ((cellInformations[4] & 1) << 12) + (cellInformations[5] << 6) + cellInformations[6]);
            byte nivel = Convert.ToByte(cellInformations[1] & 15);
            byte slope = Convert.ToByte((cellInformations[4] & 60) >> 2);


            return new Cell(prmCellId, activa, tipo, es_linea_vision, nivel, slope, tiene_objeto_interactivo ? layer_objeto_2_num : Convert.ToInt16(-1), layer_objeto_1_num, layer_objeto_2_num, this);
        }

        public bool haveTeleport()
        {
            return CellsTeleport.Count > 0;
        }

        public string TransformToCellId(string[] cellsDirection)
        {
            StringBuilder st = new StringBuilder();
            for (int i = 0; i < cellsDirection.Length; i++)
            {
                switch (cellsDirection[i])
                {
                    case "RIGHT":
                        st.Append(CellsTeleport[MapDirection.RIGHT].First());
                        break;
                    case "LEFT":
                        st.Append(CellsTeleport[MapDirection.LEFT].First());
                        break;
                    case "TOP":
                        st.Append(CellsTeleport[MapDirection.TOP].First());
                        break;
                    case "BOTTOM":
                        st.Append(CellsTeleport[MapDirection.BOTTOM].First());
                        break;
                    default:
                        break;
                }
                if (i < cellsDirection.Length - 1)
                    st.Append('|');
            }
            return st.ToString();
        }
        public string TransformToCellId(string cellDirection)
        {
            StringBuilder st = new StringBuilder();
            switch (cellDirection)
            {
                case "RIGHT":
                    st.Append(CellsTeleport[MapDirection.RIGHT].First());
                    break;
                case "LEFT":
                    st.Append(CellsTeleport[MapDirection.LEFT].First());
                    break;
                case "TOP":
                    st.Append(CellsTeleport[MapDirection.TOP].First());
                    break;
                case "BOTTOM":
                    st.Append(CellsTeleport[MapDirection.BOTTOM].First());
                    break;
                default:
                    break;
            }
            return st.ToString();
        }


        public MovementResult MoveToCell(Cell initalCell,Cell celda_destino, List<Cell> celdas_no_permitidas, bool detener_delante = false, byte distancia_detener = 0)
        {
            if (celda_destino.cellId < 0 || celda_destino.cellId > Cells.Length)
                return MovementResult.CellRangeError;

            //if (cuenta.Is_Busy() || personaje.inventario.porcentaje_pods >= 100)
            //    return MovementResult.CharacterBusyOrFull;

            if (celda_destino.cellId == initalCell.cellId)
                return MovementResult.SameCell;

            if (celda_destino.cellType == CellTypes.NOT_WALKABLE && celda_destino.interactiveObject == null)
                return MovementResult.CellNotWalkable;

            if (celda_destino.cellType == CellTypes.INTERACTIVE_OBJECT && celda_destino.interactiveObject == null)
                return MovementResult.CellIsTypeOfInteractiveObject;

            if (celdas_no_permitidas.Contains(celda_destino))
                return MovementResult.MonsterOnSun;

            List<Cell> path_temporal = pathProvider.get_Path(initalCell, celda_destino, celdas_no_permitidas, detener_delante, distancia_detener);

            if (path_temporal == null || path_temporal.Count == 0)
                return MovementResult.PathfindingError;

            if (!detener_delante && path_temporal.Last().cellId != celda_destino.cellId)
                return MovementResult.PathfindingError;

            if (detener_delante && path_temporal.Count == 1 && path_temporal[0].cellId == initalCell.cellId)
                return MovementResult.SameCell;

            if (detener_delante && path_temporal.Count == 2 && path_temporal[0].cellId == initalCell.cellId && path_temporal[1].cellId == celda_destino.cellId)
                return MovementResult.SameCell;

            actual_path = path_temporal;
            Move();
            return MovementResult.Success;
        }

        private void Move()
        {
            //if (cuenta.AccountState == AccountStates.REGENERATION)
            //    cuenta.connexion.SendPacket("eU1", true);

            string path_string = PathFinderUtil.get_Pathfinding_Limpio(actual_path);
            instance.SocketManager.ServerGameSend("GA001" + path_string);
           // personaje.evento_Personaje_Pathfinding_Minimapa(actual_path);
        }

        public bool get_Cambiar_Mapa(MapDirection direccion, Cell celda, bool ignoreGroupOnSun = false)
        {
            //if (cuenta.Is_Busy() || personaje.inventario.porcentaje_pods >= 100)
            //    return false;

            if (!get_Puede_Cambiar_Mapa(direccion, celda))
                return false;

            return get_Mover_Para_Cambiar_mapa(celda, ignoreGroupOnSun);
        }

        public bool ChangeMap(MapDirection direction)
        {
            //if (cuenta.Is_Busy())
            //    return false;

            List<Cell> celdas_teleport = Cells.Where(celda => celda.cellType == CellTypes.TELEPORT_CELL).Select(celda => celda).ToList();
            Cell cell;
            while (celdas_teleport.Count > 0)
            {
                cell = celdas_teleport[PathFinderUtil.get_Random(0, celdas_teleport.Count)];

                if (get_Cambiar_Mapa(direction, cell))
                    return true;

                celdas_teleport.Remove(cell);
            }

            //if(celdas_teleport.Count == 0)
            //{
            //    celdas_teleport = Cells.Where()

            //    switch (direccion)
            //    {
            //        case MapDirection.NONE:
                        
            //            break;
            //        case MapDirection.TOP:
            //            cell = GetCellFromId(int.Parse(Map.Teleports.Split('|')[3].Split(',')[0]));
            //            return true;
            //            break;
            //        case MapDirection.BOTTOM:
            //            cell = GetCellFromId(int.Parse(Map.Teleports.Split('|')[1].Split(',')[0]));
            //            return true;
            //            break;
            //        case MapDirection.LEFT:
            //            cell = GetCellFromId(int.Parse(Map.Teleports.Split('|')[0].Split(',')[0]));
            //            return true;
            //            break;
            //        case MapDirection.RIGHT:
            //            cell = GetCellFromId(int.Parse(Map.Teleports.Split('|')[2].Split(',')[0]));
            //            return true;
            //            break;

            //    }
            //}

            LogProvider.Log(new LogMessage(LogType.Alert, "Impossible de changer de map"));
            return false;
        }

        private bool get_Mover_Para_Cambiar_mapa(Cell celda, bool ignoreGroupOnSun = false)
        {
            var cellsNotPermitted = celdas_ocupadas().Where(c => c.cellType != CellTypes.TELEPORT_CELL).ToList();
            if (ignoreGroupOnSun)
                cellsNotPermitted = new List<Cell>();
            var t = new Random().Next(650, 1500);
            Task.Delay(t);

            MovementResult resultado = MoveToCell( CurrentCell, celda, cellsNotPermitted);
            switch (resultado)
            {
                case MovementResult.Success:
                    LogProvider.Log(new LogMessage(LogType.Ware,"MOUVEMENT", $"{this.GetCoordinates} changement de map via la cellule {celda.cellId} "));
                    return true;

                default:
                    LogProvider.Log(new LogMessage(LogType.Ware, "MOUVEMENT", $"Chemin vers {celda.cellId} résultat échoué ou bloqué : {resultado}"));
                    Task.Delay(4600);
                    this.get_Mover_Para_Cambiar_mapa(celda, ignoreGroupOnSun);
                    return false;
            }
        }

        public bool get_Puede_Cambiar_Mapa(MapDirection direccion, Cell celda)
        {
            switch (direccion)
            {
                case MapDirection.LEFT:
                    return (celda.x - 1) == celda.y;

                case MapDirection.RIGHT:
                    return (celda.x - 27) == celda.y;

                case MapDirection.BOTTOM:
                    return (celda.x + celda.y) == 31;

                case MapDirection.TOP:
                    return celda.y < 0 && (celda.x - Math.Abs(celda.y)) == 1;
            }

            return true; // direccion NINGUNA
        }

        private void RaisePropertyChanged(string propertyName)
        {
            // take a copy to prevent thread issues
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}