﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Ware.Engine.Providers;
using Ware.Models.Character;
using Ware.Models.LocalDatabase;
using Ware.Properties;

namespace Ware.Engine.Managers
{
    public class CharacterManager
    {
        private Instance instance;
        private InventoryItemProvider inventoryItemProvider;
        public CharacterManager(Instance inst)
        {
            instance = inst;
            inventoryItemProvider = new InventoryItemProvider();
        }


        public void TreatSpells(string data)
        {
            data = data.Substring(2);
            string[] spells = data.Split(';');
            Debug.WriteLine(data);
            using (var db = new ConfigContext())
            {
                for (int i = 0; i < spells.Length; i++)
                {
                    Debug.WriteLine(spells[i].Split('~')[0]);
                    if(spells[i].Split('~')[0] != "" && spells[i].Split('~')[0] != " " && spells[i].Split('~')[0] != "_")
                    {
                        Spell spell = db.Spell.Find(int.Parse(spells[i].Split('~')[0]));
                        spell.Lvl = spells[i].Split('~')[1];
                        //Spell spell = new Spell
                        //    (
                        //        int.Parse(spells[i].Split('~')[0]),
                        //        int.Parse(spells[i].Split('~')[1]),
                        //        int.Parse(spells[i].Split('~')[2]),
                        //        ""
                        //    );
                        Application.Current.Dispatcher.Invoke(new Action(() =>
                        {
                            instance.DashboardViewModel.SpellsViewModel.Spells.Add(spell);
                        }));



                    }
                }
            }
        }
        public void TreatCaracteristics(string data)
        {
            data = data.Substring(2);
            string[] cara = data.Split('|');
            string k = string.Join("",data.Split('|')[1].Reverse());
            k = System.Text.RegularExpressions.Regex.Replace(k, ".{3}", "$0 ");
            instance.DashboardViewModel.Kamas = string.Join("",k.Reverse());
            instance.DashboardViewModel.Life = data.Split('|')[5].Split(',')[0];
            instance.DashboardViewModel.MaxLife = data.Split('|')[5].Split(',')[1];
            instance.DashboardViewModel.PercentLife = ((((int.Parse(data.Split('|')[5].Split(',')[0])) * 100)) / (int.Parse(data.Split('|')[5].Split(',')[1]))).ToString();
            instance.DashboardViewModel.Power = data.Split('|')[6].Split(',')[0];
            instance.DashboardViewModel.MaxPower = data.Split('|')[6].Split(',')[1];
            instance.DashboardViewModel.PercentPower = ((((int.Parse(data.Split('|')[6].Split(',')[0])) * 100)) / (int.Parse(data.Split('|')[6].Split(',')[1]))).ToString();
            instance.DashboardViewModel.SpellsViewModel.SpellPointsAmount = int.Parse(data.Split('|')[3]);
            instance.DashboardViewModel.CaracteristicsViewModel.CaracteristicsPointsAmount = int.Parse(data.Split('|')[2]);
            instance.DashboardViewModel.CaracteristicsViewModel.Caracteristics.Add(new Caracteristic(Resources._1516, "Initiative", data.Split('|')[7]));
            instance.DashboardViewModel.CaracteristicsViewModel.Caracteristics.Add(new Caracteristic(Resources._1527, "Prospection", data.Split('|')[8]));
            instance.DashboardViewModel.CaracteristicsViewModel.Caracteristics.Add(new Caracteristic(Resources.range, "Portée", SumCarac(data.Split('|')[17].Split(',')[0], data.Split('|')[17].Split(',')[3], data.Split('|')[17].Split(',')[2]) + " (+" + data.Split('|')[17].Split(',')[1] + ")"));
            instance.DashboardViewModel.CaracteristicsViewModel.Caracteristics.Add(new Caracteristic(Resources.summonable_creature, "Créatures invocables", SumCarac(data.Split('|')[18].Split(',')[0], data.Split('|')[18].Split(',')[3], data.Split('|')[18].Split(',')[2]) + " (+" + data.Split('|')[18].Split(',')[1] + ")"));
            instance.DashboardViewModel.CaracteristicsViewModel.Caracteristics.Add(new Caracteristic(Resources._100,"Pa", data.Split('|')[9].Split(',')[4]));
            instance.DashboardViewModel.CaracteristicsViewModel.Caracteristics.Add(new Caracteristic(Resources._893,"Pm", data.Split('|')[10].Split(',')[4]));
            instance.DashboardViewModel.CaracteristicsViewModel.Caracteristics.Add(new Caracteristic(Resources._1522, "Vitalité", SumCarac(data.Split('|')[12].Split(',')[0], data.Split('|')[12].Split(',')[3], data.Split('|')[12].Split(',')[2]) + " (+" + data.Split('|')[12].Split(',')[1] + ")"));
            instance.DashboardViewModel.CaracteristicsViewModel.Caracteristics.Add(new Caracteristic(Resources._1516, "Sagesse", SumCarac(data.Split('|')[13].Split(',')[0], data.Split('|')[13].Split(',')[3], data.Split('|')[13].Split(',')[2]) + " (+" + data.Split('|')[13].Split(',')[1] + ")"));
            instance.DashboardViewModel.CaracteristicsViewModel.Caracteristics.Add(new Caracteristic(Resources._1541, "Force", SumCarac(data.Split('|')[11].Split(',')[0], data.Split('|')[11].Split(',')[3], data.Split('|')[11].Split(',')[2]) + " (+" + data.Split('|')[11].Split(',')[1] + ")"));
            instance.DashboardViewModel.CaracteristicsViewModel.Caracteristics.Add(new Caracteristic(Resources._1537, "Intelligence", SumCarac(data.Split('|')[16].Split(',')[0], data.Split('|')[16].Split(',')[3], data.Split('|')[16].Split(',')[2]) + " (+" + data.Split('|')[16].Split(',')[1] + ")"));
            instance.DashboardViewModel.CaracteristicsViewModel.Caracteristics.Add(new Caracteristic(Resources._1520, "Chance", SumCarac(data.Split('|')[14].Split(',')[0], data.Split('|')[14].Split(',')[3], data.Split('|')[14].Split(',')[2]) + " (+" + data.Split('|')[14].Split(',')[1] + ")"));
            instance.DashboardViewModel.CaracteristicsViewModel.Caracteristics.Add(new Caracteristic(Resources._1545, "Agilité", SumCarac(data.Split('|')[15].Split(',')[0], data.Split('|')[15].Split(',')[3], data.Split('|')[15].Split(',')[2]) + " (+" + data.Split('|')[15].Split(',')[1] + ")"));

        }
        public void TreatInventory(string data)
        {

            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                foreach (var item in inventoryItemProvider.GetItems(data))
                {
                    instance.DashboardViewModel.InventoryViewModel.Items.Add(item);

                }
            }));
            

        }
        private string SumCarac(string a, string b, string c)
        {
            return (int.Parse(a) + int.Parse(b) + int.Parse(c)).ToString();
        }
    }
}
