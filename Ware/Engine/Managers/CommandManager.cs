﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ware.Engine.Providers;
using Ware.Enums;
using Ware.Models.Character;

namespace Ware.Engine.Managers
{
    public class CommandManager
    {
        private static Instance Instance;
        public CommandManager(Instance instance)
        {
            Instance = instance;
        }

        public static void ParseCommand(LogType logType,string m)
        {
            if(m != null && m.Contains("/") && m.Contains(" "))
            {
                if (m.Substring(0, 1) == "/" && m.Substring(2,1) != " ")
                {
                    TreatCommand(m);
                }
                else
                {
                    // TreatMessage(logType,msg)
                }
            }
            
        }

        private static void TreatCommand(string msg)
        {
            msg = msg.Substring(1);
            string[] msgs = msg.Split(' ');
            switch (msgs[0])
            {
                case "move":
                    switch (msgs[1])
                    {
                        case "top":
                            Instance.MapManager.ChangeMap(MapDirection.TOP);
                            break;
                        case "bot":
                            Instance.MapManager.ChangeMap(MapDirection.BOTTOM);
                            break;
                        case "right":
                            Instance.MapManager.ChangeMap(MapDirection.RIGHT);
                            break;
                        case "left":
                            Instance.MapManager.ChangeMap(MapDirection.LEFT);
                            break;
                        default:
                            LogProvider.Log(new LogMessage(LogType.Ware, "Commande inconnue. Tapez \"/help\" pour obtenir plus d'informations sur els commandes disponibles."));
                            break;
                    }
                    break;
                case "help":
                    LogProvider.Log(new LogMessage(LogType.Ware, "Information sur les commandes disponibles :\n\n- move [top, bot, right, left]\nPermet de se déplacer d'une map."));

                    break;
                default:
                    LogProvider.Log(new LogMessage(LogType.Ware, "Commande inconnue. Tapez \"/help\" pour obtenir plus d'informations sur els commandes disponibles."));
                    break;
            }
        }
    }
}
