﻿using System;
using System.Threading.Tasks;
using Ware.Models.Character;

namespace Ware.Engine.Providers
{
    public class LogProvider
    {
        public Instance Instance { get; }
        private static Action<DebugMessage> DebugMessage;
        private static Action<LogMessage> LogMessage;
        public LogProvider(Action<DebugMessage> debugMessage, Action<LogMessage> logMessage)
        {
            DebugMessage = debugMessage;
            LogMessage = logMessage;
        }

        public async static void Debug(DebugMessage Message)
        {
            await Task.Factory.StartNew(() =>
             {
                 if (DebugMessage != null)
                 {
                     DebugMessage(Message);
                 }
             });
        }
        public async static void Log(LogMessage Message)
        {
            await Task.Factory.StartNew(() =>
            {
                if (LogMessage != null)
                {
                    LogMessage(Message);
                }
            });
        }
    }
}