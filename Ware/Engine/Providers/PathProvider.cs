﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Ware.Engine.Managers;
using Ware.Models.Exceptions;
using Ware.Models.LocalDatabase;
using Ware.Models.Map;

namespace Ware.Engine.Providers
{
    public class PathProvider
    {
        private Map map { get; set; }
        private bool disposed;
        private MapManager MapManager;

        public PathProvider(MapManager mapManager)
        {
            MapManager = mapManager;
        }


        public List<Cell> get_Path(Cell celda_inicio, Cell celda_final, List<Cell> celdas_no_permitidas, bool stopAhead = false, byte stopDistance = 0)
        {
            if (celda_inicio == null || celda_final == null)
                return null;

            List<Cell> celdas_permitidas = new List<Cell>() { celda_inicio };

            if (celdas_no_permitidas.Contains(celda_final))
                celdas_no_permitidas.Remove(celda_final);

            while (celdas_permitidas.Count > 0)
            {
                int index = 0;
                for (int i = 1; i < celdas_permitidas.Count; i++)
                {
                    if (celdas_permitidas[i].coste_f < celdas_permitidas[index].coste_f)
                        index = i;

                    if (celdas_permitidas[i].coste_f != celdas_permitidas[index].coste_f) continue;
                    if (celdas_permitidas[i].coste_g > celdas_permitidas[index].coste_g)
                        index = i;

                    if (celdas_permitidas[i].coste_g == celdas_permitidas[index].coste_g)
                        index = i;

                    if (celdas_permitidas[i].coste_g == celdas_permitidas[index].coste_g)
                        index = i;
                }

                Cell actual = celdas_permitidas[index];

                if (stopAhead && get_Distancia_Nodos(actual, celda_final) <= stopDistance && !celda_final.IsWalkable())
                    return get_Camino_Retroceso(celda_inicio, actual);

                if (actual == celda_final)
                    return get_Camino_Retroceso(celda_inicio, celda_final);

                celdas_permitidas.Remove(actual);
                celdas_no_permitidas.Add(actual);

                foreach (Cell celda_siguiente in get_Celdas_Adyecentes(actual))
                {
                    if (celdas_no_permitidas.Contains(celda_siguiente) || !celda_siguiente.IsWalkable())
                        continue;

                    if (celda_siguiente.IsTeleportCell() && celda_siguiente != celda_final)
                        continue;

                    int temporal_g = actual.coste_g + get_Distancia_Nodos(celda_siguiente, actual);

                    if (!celdas_permitidas.Contains(celda_siguiente))
                        celdas_permitidas.Add(celda_siguiente);
                    else if (temporal_g >= celda_siguiente.coste_g)
                        continue;

                    celda_siguiente.coste_g = temporal_g;
                    celda_siguiente.coste_h = get_Distancia_Nodos(celda_siguiente, celda_final);
                    celda_siguiente.coste_f = celda_siguiente.coste_g + celda_siguiente.coste_h;
                    celda_siguiente.parentNode = actual;
                }
            }

            return null;
        }

        private List<Cell> get_Camino_Retroceso(Cell nodo_inicial, Cell nodo_final)
        {
            Cell nodo_actual = nodo_final;
            List<Cell> celdas_camino = new List<Cell>();

            while (nodo_actual != nodo_inicial)
            {
                celdas_camino.Add(nodo_actual);
                nodo_actual = nodo_actual.parentNode;
            }

            celdas_camino.Add(nodo_inicial);
            celdas_camino.Reverse();

            return celdas_camino;
        }

        public List<Cell> get_Celdas_Adyecentes(Cell nodo)
        {
            List<Cell> celdas_adyecentes = new List<Cell>();

            Cell celda_derecha = MapManager.Cells.FirstOrDefault(nodec => nodec.x == nodo.x + 1 && nodec.y == nodo.y);
            Cell celda_izquierda = MapManager.Cells.FirstOrDefault(nodec => nodec.x == nodo.x - 1 && nodec.y == nodo.y);
            Cell celda_inferior = MapManager.Cells.FirstOrDefault(nodec => nodec.x == nodo.x && nodec.y == nodo.y + 1);
            Cell celda_superior = MapManager.Cells.FirstOrDefault(nodec => nodec.x == nodo.x && nodec.y == nodo.y - 1);

            if (celda_derecha != null)
                celdas_adyecentes.Add(celda_derecha);
            if (celda_izquierda != null)
                celdas_adyecentes.Add(celda_izquierda);
            if (celda_inferior != null)
                celdas_adyecentes.Add(celda_inferior);
            if (celda_superior != null)
                celdas_adyecentes.Add(celda_superior);

            Cell superior_izquierda = MapManager.Cells.FirstOrDefault(nodec => nodec.x == nodo.x - 1 && nodec.y == nodo.y - 1);
            Cell inferior_derecha = MapManager.Cells.FirstOrDefault(nodec => nodec.x == nodo.x + 1 && nodec.y == nodo.y + 1);
            Cell inferior_izquierda = MapManager.Cells.FirstOrDefault(nodec => nodec.x == nodo.x - 1 && nodec.y == nodo.y + 1);
            Cell superior_derecha = MapManager.Cells.FirstOrDefault(nodec => nodec.x == nodo.x + 1 && nodec.y == nodo.y - 1);

            if (superior_izquierda != null)
                celdas_adyecentes.Add(superior_izquierda);
            if (superior_derecha != null)
                celdas_adyecentes.Add(superior_derecha);
            if (inferior_derecha != null)
                celdas_adyecentes.Add(inferior_derecha);
            if (inferior_izquierda != null)
                celdas_adyecentes.Add(inferior_izquierda);

            return celdas_adyecentes;
        }

        private int get_Distancia_Nodos(Cell a, Cell b) => ((a.x - b.x) * (a.x - b.x)) + ((a.y - b.y) * (a.y - b.y));

        #region Zona Dispose
        public void Dispose() => Dispose(true);
        ~PathProvider() => Dispose(false);

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {

                disposed = true;
            }
        }
        #endregion



        // Mettre Evite soleil
        // Anti-agro
        // Esquive Tacle

        //private void LoadSprites(int cellEnd)
        //{
        //    //for (int i = 1; i <= 1000; i++)
        //    //{
        //    //    if (MapHandler[i].active && MapHandler[i].movement > 1)
        //    //    {
        //    //        if (MapHandler[i].lineOfSight == false)
        //    //            closelist.Add(i);
        //    //        else if (fight == false)
        //    //        {
        //    //            // S'il s'agit d'une case avec des soleils pour changer de map
        //    //            if ((MapHandler[i].layerObject1Num == 1030) || (MapHandler[i].layerObject2Num == 1030))
        //    //            {
        //    //                // Je vérifie qu'il s'agit d'une case autre de celle que je souhaite.
        //    //                if (i != cellEnd && EviteChangeur)
        //    //                    closelist.Add(i);
        //    //            }
        //    //        }
        //    //    }
        //    //    else
        //    //        closelist.Add(i);
        //    //}
        //}

        // refaire entity

        //private void LoadEntity(ListView dgv, int cellEnd, int caseActuel)
        //{
        //    foreach (ListViewItem Pair in dgv.Items)
        //    {
        //        if (System.Convert.ToInt32(Pair.SubItems(0).Text) != caseActuel)
        //        {
        //            if (System.Convert.ToInt32(Pair.SubItems(0).Text) != cellEnd)
        //                closelist.Add(System.Convert.ToInt32(Pair.SubItems(0).Text));
        //        }
        //    }
        //}

        //private void loadCell()
        //{
        //    for (var i = 0; i <= 1024; i++)
        //    {
        //        Plist[i] = 0;
        //        Flist[i] = 0;
        //        Glist[i] = 0;
        //        Hlist[i] = 0;
        //    }
        //}

        //public string pathing(int cellbegin, int nCellEnd, int nbrPM = 9999, bool eviteSoleil = true, bool enCombat = false, bool antiAgro = false, bool antiTacle = false)
        //{
        //    // var withBlock = Comptes(index);
        //    nombreDePM = nbrPM;
        //    mapLargeur = MapManager.Map.Width == 0 ? 15 : MapManager.Map.Width;
        //    EviteChangeur = !enCombat;
        //    MapHandler = MapManager.Cells;
        //    loadCell();
        //    fight = false;
        //    if (fight)
        //        nombreDePM = 3;
        //    LoadSprites(nCellEnd);

        //    if (fight)
        //    {
        //    }
        //    else
        //    {
        //    }

        //    closelist.Remove(nCellEnd);
        //    string returnPath = Findpath(cellbegin, nCellEnd);

        //    // withBlock.PathTotal = returnPath;

        //    return cleanPath(returnPath);
        //}

        //private string Findpath(int cell1, int cell2)
        //{
        //    int current;
        //    int i = 0;

        //    openlist.Add(cell1);

        //    while (!openlist.Contains(cell2))
        //    {
        //        i += 1;
        //        if (i > 1000)
        //            return "";

        //        current = getFpoint();
        //        if (current == cell2)
        //            break;

        //        closelist.Add(current);
        //        openlist.Remove(current);

        //        foreach (int cell in getChild(current))
        //        {
        //            if (closelist.Contains(cell) == false)
        //            {
        //                if (openlist.Contains(cell))
        //                {
        //                    if (Glist[current] + 5 < Glist[cell])
        //                    {
        //                        Plist[cell] = current;
        //                        Glist[cell] = Glist[current] + 5;
        //                        Hlist[cell] = goalDistance(cell, cell2);
        //                        Flist[cell] = Glist[cell] + Hlist[cell];
        //                    }
        //                }
        //                else
        //                {
        //                    openlist.Add(cell);
        //                    openlist[openlist.Count - 1] = cell;
        //                    Glist[cell] = Glist[current] + 5;
        //                    Hlist[cell] = goalDistance(cell, cell2);
        //                    Flist[cell] = Glist[cell] + Hlist[cell];
        //                    Plist[cell] = current;
        //                }
        //            }
        //        }
        //    }

        //    return (GetParent(cell1, cell2));
        //}

        //private string GetParent(int cell1, int cell2)
        //{
        //    int current = cell2;
        //    ArrayList pathCell = new ArrayList();
        //    pathCell.Add(current);
        //    InitializeCells();
        //    do
        //    {
        //        pathCell.Add(Plist[current]);
        //        current = Plist[current];
        //        if (current == 0)
        //            break;
        //    } while (current != cell1);
        //    return getPath(pathCell);
        //}

        //private void InitializeCells()
        //{
        //    int Number = 0;
        //    string[] hash = new[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "-", "_" };
        //    string[] hash2 = new[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
        //    int i = 0;
        //    for (i = 0; i <= hash2.Length - 1; i++)
        //    {
        //        for (var j = 0; j <= hash.Length - 1; j++)
        //        {
        //            cases[Number] = hash2[i] + hash[j];
        //            Number = Number + 1;
        //        }
        //    }
        //}

        //private string getPath(ArrayList pathCell)
        //{
        //    pathCell.Reverse();
        //    string pathing = "";
        //    int current;
        //    int child;
        //    int PMUsed = 0;

        //    for (var i = 0; i <= pathCell.Count - 2; i++)
        //    {
        //        PMUsed += 1;
        //        if ((PMUsed > nombreDePM))
        //            return pathing;
        //        current = (int)pathCell[i];
        //        child = (int)pathCell[i + 1];
        //        pathing += getOrientation(current, child, mapLargeur) + cases[child];
        //    }
        //    return pathing;
        //}

        //private ArrayList getChild(int cell)
        //{
        //    var x = getCaseCoordonneeX(cell);
        //    var y = getCaseCoordonneeY(cell);
        //    ArrayList children = new ArrayList();
        //    int temp;
        //    int locx, locy;

        //    if (fight == false)
        //    {
        //        temp = cell - (mapLargeur * 2 - 1);
        //        locx = getCaseCoordonneeX(temp);
        //        locy = getCaseCoordonneeY(temp);
        //        if (temp > 1 & temp < 1024 & locx == x - 1 & locy == y - 1 & closelist.Contains(temp) == false)
        //            children.Add(temp);

        //        temp = cell + (mapLargeur * 2 - 1);
        //        locx = getCaseCoordonneeX(temp);
        //        locy = getCaseCoordonneeY(temp);
        //        if (temp > 1 & temp < 1024 & locx == x + 1 & locy == y + 1 & closelist.Contains(temp) == false)
        //            children.Add(temp);
        //    }

        //    temp = cell - mapLargeur;
        //    locx = getCaseCoordonneeX(temp);
        //    locy = getCaseCoordonneeY(temp);
        //    if (temp > 1 & temp < 1024 & locx == x - 1 & locy == y & closelist.Contains(temp) == false)
        //        children.Add(temp);

        //    temp = cell + mapLargeur;
        //    locx = getCaseCoordonneeX(temp);
        //    locy = getCaseCoordonneeY(temp);
        //    if (temp > 1 & temp < 1024 & locx == x + 1 & locy == y & closelist.Contains(temp) == false)
        //        children.Add(temp);

        //    temp = cell - (mapLargeur - 1);
        //    locx = getCaseCoordonneeX(temp);
        //    locy = getCaseCoordonneeY(temp);
        //    if (temp > 1 & temp < 1024 & locx == x & locy == y - 1 & closelist.Contains(temp) == false)
        //        children.Add(temp);

        //    temp = cell + (mapLargeur - 1);
        //    locx = getCaseCoordonneeX(temp);
        //    locy = getCaseCoordonneeY(temp);
        //    if (temp > 1 & temp < 1024 & locx == x & locy == y + 1 & closelist.Contains(temp) == false)
        //        children.Add(temp);

        //    if (fight == false)
        //    {
        //        temp = cell - 1;
        //        locx = getCaseCoordonneeX(temp);
        //        locy = getCaseCoordonneeY(temp);
        //        if (temp > 1 & temp < 1024 & locx == x - 1 & locy == y + 1 & closelist.Contains(temp) == false)
        //            children.Add(temp);

        //        temp = cell + 1;
        //        locx = getCaseCoordonneeX(temp);
        //        locy = getCaseCoordonneeY(temp);
        //        if (temp > 1 & temp < 1024 & locx == x + 1 & locy == y - 1 & closelist.Contains(temp) == false)
        //            children.Add(temp);
        //    }

        //    return children;
        //}

        //private int getFpoint()
        //{
        //    int x = 9999;
        //    int cell = 0;

        //    foreach (int item in openlist)
        //    {
        //        if (closelist.Contains(item) == false)
        //        {
        //            if (Flist[item] < x)
        //            {
        //                x = Flist[item];
        //                cell = item;
        //            }
        //        }
        //    }

        //    return cell;
        //}

        //public class loc8
        //{
        //    public int y = 0;
        //    public int x = 0;
        //}

        //private int getCaseCoordonneeX(int nNum)
        //{
        //    var _loc4 = mapLargeur;
        //    var _loc5 = Math.Floor(nNum / (double)(_loc4 * 2 - 1));
        //    var _loc6 = nNum - _loc5 * (_loc4 * 2 - 1);
        //    var _loc7 = _loc6 % _loc4;
        //    loc8 _loc8 = new loc8();

        //    int y = (int)(_loc5 - _loc7);
        //    int x = (int)((nNum - (_loc4 - 1) * y) / (double)_loc4);
        //    return x;
        //}

        //private int getCaseCoordonneeY(int nNum)
        //{
        //    int _loc4 = mapLargeur;
        //    int _loc5 = (int)Math.Floor(nNum / (double)(_loc4 * 2 - 1));
        //    int _loc6 = nNum - _loc5 * (_loc4 * 2 - 1);
        //    int _loc7 = _loc6 % _loc4;
        //    loc8 _loc8 = new loc8();

        //    int y = _loc5 - _loc7;
        //    int x = (int)((nNum - ((_loc4 - 1) * y)) / (double)_loc4);
        //    return y;
        //}

        //private int goalDistance(int nCell1, int nCell2)
        //{
        //    int _loc5x = getCaseCoordonneeX(nCell1);
        //    int _loc5y = getCaseCoordonneeY(nCell1);
        //    int _loc6x = getCaseCoordonneeX(nCell2);
        //    int _loc6y = getCaseCoordonneeY(nCell2);
        //    int _loc7 = Math.Abs(_loc5x - _loc6x);
        //    int _loc8 = Math.Abs(_loc5y - _loc6y);
        //    return (_loc7 + _loc8);
        //}

        //private char getOrientation(int cell1, int cell2, int Map_Largeur)
        //{
        //    int mapWidth = 1026;
        //    char obj;

        //    int num = cell2 - cell1;

        //    if (Map_Largeur == null)
        //    {
        //        throw new BadMapWidthException();
        //    }
        //    else
        //    {
        //        mapWidth = Map_Largeur;
        //    }

        //    //switch (num)
        //    //{
        //    //    case int a when a > 0 - (mapWidth * 2 - 1) && a < -29:
        //    //        obj = 'g';
        //    //        break;

        //    //    case int a when a > mapWidth * 2 - 1 && a < 29:
        //    //        obj = 'c';
        //    //        break;

        //    //    case -1:
        //    //        {
        //    //            obj = 'e';
        //    //            break;
        //    //        }

        //    //    case 1:
        //    //        {
        //    //            obj = 'a';
        //    //            break;
        //    //        }
        //    //    case int a when a == Convert.ToInt32(-mapWidth):
        //    //        {
        //    //            obj = 'f';
        //    //            break;
        //    //        }

        //    //    case int a when a == mapWidth:
        //    //        {
        //    //            obj = 'b';
        //    //            break;
        //    //        }

        //    //    case int a when a != 0 - (mapWidth - 1):
        //    //        {
        //    //            obj = num != mapWidth - 1 ? 'a' : 'd';
        //    //            break;
        //    //        }
        //    //    default:
        //    //        {
        //    //            obj = 'h';
        //    //            break;
        //    //        }
        //    //}
        //    switch (cell1 - cell2)
        //    {
        //        case -29:
        //            return 'g';
        //            break;
        //        case 29:
        //            return 'c';
        //            break;
        //        case -1:
        //            return 'e';
        //            break;
        //        case 1:
        //            return 'a';
        //            break;
        //        case -15:
        //            return 'f';
        //            break;
        //        case 15:
        //            return 'b';
        //            break;
        //        case -14:
        //            return 'h';
        //            break;
        //        case 14:
        //            return 'd';
        //            break;
        //    }

        //    return 'a';
        //}

        //private string cleanPath(string path)
        //{
        //    string cleanedPath = "";

        //    if ((path.Length > 3))
        //    {
        //        for (int i = 1; i <= path.Length; i += 3)
        //        {
        //            if ((path.Substring(i, 1) != path.Substring(i + 3, 1)))
        //                cleanedPath += path.Substring(i, 3);
        //        }
        //    }
        //    else
        //        cleanedPath = path;
        //    return cleanedPath;
        //}
    }
}