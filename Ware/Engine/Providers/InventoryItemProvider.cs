﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Ware.Models.Character;
using Ware.Models.LocalDatabase;
using Ware.Properties;

namespace Ware.Engine.Providers
{
    public class InventoryItemProvider
    {
        public InventoryItemProvider()
        {

        }

        public List<InventoryObject> GetItems(string brutData)
        {
            List<InventoryObject> objects = new List<InventoryObject>();
            string[] items = brutData.Split('|')[10].Split(';');
            using (var db = new ConfigContext())
            {

                foreach (string val in items)
                {
                    try
                    {
                        Convert.ToInt32(val.Split('~')[1], 16);
                        // effects = new List<Caracteristic>();
                        Item sample = db.Items.Find(Convert.ToInt32(val.Split('~')[1], 16));
                        //GetEffects(val.Split('~')[4]);
                        InventoryObject inventoryObject = new InventoryObject(sample, val);
                        objects.Add(inventoryObject);
                    }
                    catch (Exception)
                    {

                    }
                   

                }
            }
            return objects;
        }

        private void GetEffects(string data)
        {
            List<Caracteristic> effects = new List<Caracteristic>();
            // data = 1 item
            if(data != "")
            {
                string[] linesEffect = data.Split(',');
                for (int i = 0; i < linesEffect.Length; i++)
                {
                    if (linesEffect[i] != "")
                    {
                        string[] LineEffect = linesEffect[i].Split('#');

                        int carac = (int)(LineEffect[0] != "-1" ? Convert.ToInt64(LineEffect[0], 16) : -1);

                        string valeur1 = Convert.ToInt64(LineEffect[1], 16).ToString();
                        int valeur2;
                        string valeur3 = Convert.ToInt64(LineEffect[3], 16).ToString();
                        switch (carac)
                        {
                            case 91:
                                effects.Add(new Caracteristic(Resources._1518, "", "Vole : " + valeur1 + " à " + Convert.ToInt64(LineEffect[2], 16).ToString() + " (eau)"));
                                break;
                            case 92:
                                effects.Add(new Caracteristic(Resources._1539, "", "Vole : " + valeur1 + " à " + Convert.ToInt64(LineEffect[2], 16).ToString() + " (terre)"));
                                break;
                            case 93:
                                effects.Add(new Caracteristic(Resources._1543, "", "Vole : " + valeur1 + " à " + Convert.ToInt64(LineEffect[2], 16).ToString() + " (air)"));
                                break;
                            case 94:
                                effects.Add(new Caracteristic(Resources._1535,"", "Vole : " + valeur1 + " à " + Convert.ToInt64(LineEffect[2], 16).ToString() + " (feu)"));
                            break;
                            case 95:
                                effects.Add(new Caracteristic(Resources._1529, "", "Vole : " + valeur1 + " à " + Convert.ToInt64(LineEffect[2], 16).ToString() + " (neutre)"));
                            break;
                        }
                    }
                }
            }
        }
    }
}
