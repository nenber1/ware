﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Ware.Common.Utils;
using Ware.Enums;
using Ware.Models.Map;

namespace Ware.Engine.Providers.Utils
{
    internal class PathFinderUtil
    {
        private static readonly Dictionary<AnimationType, AnimationDuration> tiempo_tipo_animacion = new Dictionary<AnimationType, AnimationDuration>()
        {
            { AnimationType.Mount, new AnimationDuration(135, 200, 120) },
            { AnimationType.Running, new AnimationDuration(170, 255, 150) },
            { AnimationType.Walking, new AnimationDuration(480, 510, 425) },
            { AnimationType.Ghost, new AnimationDuration(57, 85, 50) }
        };

        public static int get_Tiempo_Desplazamiento_Mapa(Cell celda_actual, List<Cell> celdas_camino, bool con_montura = false)
        {
            int tiempo_desplazamiento = 20;
            AnimationDuration tipo_animacion;

            if (con_montura)
                tipo_animacion = tiempo_tipo_animacion[AnimationType.Mount];
            else
                tipo_animacion = celdas_camino.Count > 6 ? tiempo_tipo_animacion[AnimationType.Running] : tiempo_tipo_animacion[AnimationType.Walking];

            Cell siguiente_celda;

            for (int i = 1; i < celdas_camino.Count; i++)
            {
                siguiente_celda = celdas_camino[i];

                if (celda_actual.y == siguiente_celda.y)
                    tiempo_desplazamiento += tipo_animacion.horizontal;
                else if (celda_actual.x == siguiente_celda.y)
                    tiempo_desplazamiento += tipo_animacion.vertical;
                else
                    tiempo_desplazamiento += tipo_animacion.lineal;

                if (celda_actual.layer_ground_nivel < siguiente_celda.layer_ground_nivel)
                    tiempo_desplazamiento += 100;
                else if (siguiente_celda.layer_ground_nivel > celda_actual.layer_ground_nivel)
                    tiempo_desplazamiento -= 100;
                else if (celda_actual.layer_ground_slope != siguiente_celda.layer_ground_slope)
                {
                    if (celda_actual.layer_ground_slope == 1)
                        tiempo_desplazamiento += 100;
                    else if (siguiente_celda.layer_ground_slope == 1)
                        tiempo_desplazamiento -= 100;
                }
                celda_actual = siguiente_celda;
            }

            return tiempo_desplazamiento;
        }

        public static string get_Pathfinding_Limpio(List<Cell> camino)
        {
            Cell celda_destino = camino.Last();

            if (camino.Count <= 2)
                return celda_destino.GetCharDirection(camino.First()) + Functions.Get_Cell_Char(celda_destino.cellId);

            StringBuilder pathfinder = new StringBuilder();
            char direccion_anterior = camino[1].GetCharDirection(camino.First()), direccion_actual;

            for (int i = 2; i < camino.Count; i++)
            {
                Cell celda_actual = camino[i];
                Cell celda_anterior = camino[i - 1];
                direccion_actual = celda_actual.GetCharDirection(celda_anterior);

                if (direccion_anterior != direccion_actual)
                {
                    pathfinder.Append(direccion_anterior);
                    pathfinder.Append(Functions.Get_Cell_Char(celda_anterior.cellId));

                    direccion_anterior = direccion_actual;
                }
            }

            pathfinder.Append(direccion_anterior);
            pathfinder.Append(Functions.Get_Cell_Char(celda_destino.cellId));
            return pathfinder.ToString();
        }

        private static int seed = Environment.TickCount;
        private static readonly ThreadLocal<Random> random = new ThreadLocal<Random>(() => new Random(Interlocked.Increment(ref seed)));
        private static readonly object sema = new object();

        public static int get_Random(int min, int max)
        {
            lock (sema)
            {
                return min <= max ? random.Value.Next(min, max) : random.Value.Next(max, min);
            }
        }

        public static double get_Random_Numero()
        {
            lock (sema)
            {
                return random.Value.NextDouble();
            }
        }
    }
}
