﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ware.Engine.Providers.Utils
{
    internal class AnimationDuration
    {
        public int lineal { get; private set; }
        public int horizontal { get; private set; }
        public int vertical { get; private set; }

        public AnimationDuration(int _lineal, int _horizontal, int _vertical)
        {
            lineal = _lineal;
            horizontal = _horizontal;
            vertical = _vertical;
        }
    }
}
