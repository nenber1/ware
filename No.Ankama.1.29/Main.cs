﻿using EasyHook;
using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading;

namespace No.Ankama._1._29
{
    public class RemoteInterface : MarshalByRefObject
    {
        public void IsInstalled(int clientPID)
        {
            //MessageBox.Show("Injection réussie");
        }

        public void Message(object o, params object[] args)
        {
            //MessageBox.Show(o.ToString() + args);
        }

        public void ErrorHandler(Exception ex)
        {
        }
    }

    public class Main : IEntryPoint
    {
        private static string ChannelName;

        private RemoteInterface Interface;
        private LocalHook CreateConnectHook;
        private List<IPAddress> Whitelist;

        public Main(RemoteHooking.IContext InContext, string InChannelName)
        {
            try
            {
                Interface = RemoteHooking.IpcConnectClient<RemoteInterface>(InChannelName);
                ChannelName = InChannelName;
                Interface.IsInstalled(RemoteHooking.GetCurrentProcessId());
            }
            catch (Exception ex)
            {
                Interface.ErrorHandler(ex);
            }
        }

        public void Run(RemoteHooking.IContext InContext, string InChannelName)
        {
            try
            {
                CreateConnectHook = LocalHook.Create(
                                    LocalHook.GetProcAddress("Ws2_32.dll", "connect"),
                                    new NativeSocketMethods.WinsockConnectDelegate(HookCallback),
                                    this);

                CreateConnectHook.ThreadACL.SetExclusiveACL(new[] { 0 });

                Whitelist = new List<IPAddress>()
                {
                    IPAddress.Parse("172.65.206.193")
                    //IPAddress.Parse("34.252.21.81"),
                    //IPAddress.Parse("34.251.172.139"),
                };
            }
            catch (Exception ex)
            {
                Interface.ErrorHandler(ex);
            }

            RemoteHooking.WakeUpProcess();

            for (; ; )
                Thread.Sleep(1000);
        }

        private int HookCallback(IntPtr s, IntPtr addr, int addrsize)
        {
            NativeSocketMethods.sockaddr_in structure = Marshal.PtrToStructure<NativeSocketMethods.sockaddr_in>(addr);

            if (structure.sin_addr.S_addr > 0)
                Interface.Message(
                    $"> Connection attempt at {new IPAddress(structure.sin_addr.S_addr)}:{structure.sin_port}");

            if (Whitelist.Contains(new IPAddress(structure.sin_addr.S_addr)))
            {
                var buffer = Marshal.AllocHGlobal(addrsize);
                var str = new NativeSocketMethods.sockaddr_in();
                str.sin_addr.S_addr = NativeSocketMethods.inet_addr("127.0.0.1");
                str.sin_port = NativeSocketMethods.htons(443);
                str.sin_family = (short)NativeSocketMethods.AddressFamily.InterNetworkv4;
                Marshal.StructureToPtr(str, buffer, true);
                return NativeSocketMethods.connect(s, buffer, addrsize);
            }

            return NativeSocketMethods.connect(s, addr, addrsize);
        }
    }
}